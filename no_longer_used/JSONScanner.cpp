/*
#include "../../../Headers/Engine/Json/JSONScanner.h"
#include <iostream>
using namespace std;
using namespace Engine;

bool JSONScanner::IsDigit(char inputChar)
{
    int num = inputChar - '0';

    if(num >= 0 && num <= 9)
    {
        return true;
    }

    return false;
}

void JSONScanner::NewString(uint& pos, string input)
{
    string newString = "";

    while(input[pos] != '"')
    {
        newString.push_back(input[pos]);
        pos++;
    }

    _queue.EnQueue(new JSONString(newString));
}

void JSONScanner::NewNumber(uint& pos, string input)
{
    int num = 0;

    while(IsDigit(input[pos]))
    {
        num = num * 10 + (input[pos] - '0');
        pos++;
    }

    _queue.EnQueue(new JSONNumber(num));
}

void JSONScanner::NewTrue(uint& pos, string input)
{
    assert(input[pos] == 't' &&
           input[pos + 1] == 'r' &&
           input[pos + 2] == 'u' &&
           input[pos + 3] == 'e');

    _queue.EnQueue(new JSONBoolean(true));
    pos = pos + 3;
}

void JSONScanner::NewFalse(uint& pos, string input)
{
    assert(input[pos] == 'f' &&
           input[pos + 1] == 'a' &&
           input[pos + 2] == 'l' &&
           input[pos + 3] == 's' &&
           input[pos + 4] == 'e');

    _queue.EnQueue(new JSONBoolean(false));
    pos = pos + 4;
}

Queue<IJSONElement*>& JSONScanner::Scan(string input)
{
	_queue.Clear();

    for(uint i = 0; i < input.length(); i++)
    {
        if(IsDigit(input[i]))
        {
            NewNumber(i, input);
        }

        switch(input[i])
        {
            case '"':
                NewString(++i, input);
                break;

            case '{':
                _queue.EnQueue(new JSONToken('{', OPEN_BRACE));
                break;

            case '}':
                _queue.EnQueue(new JSONToken('}', CLOSE_BRACE));
                break;

            case '[':
                _queue.EnQueue(new JSONToken('[', OPEN_BRACKET));
                break;

            case ']':
                _queue.EnQueue(new JSONToken(']', CLOSE_BRACKET));
                break;

            case ',':
                _queue.EnQueue(new JSONToken(',', COMMA));
                break;

            case ':':
                _queue.EnQueue(new JSONToken(':', COLON));
                break;

            case 't':
                NewTrue(i, input);
                break;

            case 'f':
                NewFalse(i, input);
                break;

            default:break;
        }
    }

    return _queue;
}
*/
