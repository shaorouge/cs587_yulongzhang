/*
#ifndef _IJSONELEMENT_H_
#define _IJSONELEMENT_H_
#include "../Iterator/ArrayIterator.h"
#include "../DataStructure/HashMap.h"

namespace Engine
{
	enum Token
	{
		//terminal tokens
		OPEN_BRACE,
		CLOSE_BRACE,
		OPEN_BRACKET,
		CLOSE_BRACKET,
		COMMA,
		COLON,

		STRING,
		NUMBER,
		BOOLEAN,
		NUL,

		//nonterminal tokens
		OBJECT,
		ARRAY,
		VALUE,
		OBJECT_EXTENSION,
		ARRAY_EXTENSION,

		END
	};

	class IJSONElement
	{
	protected:
		Token _token;

	public:
		virtual Token GetToken()
		{
			return _token;
		}

		virtual ~IJSONElement(){}
	};

	class JSONObject : public IJSONElement
	{
	private:
		HashMap<IJSONElement*> _map;

	public:
		JSONObject()
		{
			_token = OBJECT;
		}

		void Put(string name, IJSONElement* element)
		{
			_map.Set(name, element);
		}

		IJSONElement* Get(string name)
		{
			return _map.Get(name);
		}
	};

	class JSONArray : public DynamicArray<IJSONElement*>, public IJSONElement
	{
	private:
		string _name;

	public:
		JSONArray()
		{
			_token = ARRAY;
		}

		string GetName()
		{
			return _name;
		}
	};

	class JSONValue : public IJSONElement
	{
	public:
		JSONValue()
		{
			_token = VALUE;
		}
	};

	class JSONString : public IJSONElement
	{
	private:
		string _value;

	public:
		JSONString(string value) : _value(value)
		{
			_token = STRING;
		}

		string GetValue()
		{
			return _value;
		}
	};

	class JSONNumber : public IJSONElement
	{
	private:
		int _value;

	public:
		JSONNumber(int value) : _value(value)
		{
			_token = NUMBER;
		}

		int GetValue()
		{
			return _value;
		}
	};

	class JSONBoolean : public IJSONElement
	{
	private:
		bool _value;

	public:
		JSONBoolean(bool value) : _value(value)
		{
			_token = BOOLEAN;
		}

		bool GetValue()
		{
			return _value;
		}
	};

	class JSONToken : public IJSONElement
	{
	private:
		char _value;

	public:
		JSONToken(char value, Token token) : _value(value)
		{
			_token = token;
		}

		char GetValue()
		{
			return _value;
		}
	};
}
#endif
*/
