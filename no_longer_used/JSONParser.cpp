/*
#include "../../../Headers/Engine/Json/JSONParser.h"
using namespace Engine;

JSONObject* JSONParser::Object(Queue<IJSONElement*>& queue)
{
    JSONObject* jsonObj = new JSONObject();

    while(queue.Front()->GetToken() != CLOSE_BRACE)
    {
        assert(queue.Front()->GetToken() == OPEN_BRACE ||
               queue.Front()->GetToken() == COMMA);

        queue.DeQueue();
        string key = ((JSONString*)queue.DeQueue())->GetValue();

        assert(queue.DeQueue()->GetToken() == COLON);

        jsonObj->Put(key, Value(queue));
    }

    assert(queue.DeQueue()->GetToken() == CLOSE_BRACE);

    return jsonObj;
}

IJSONElement* JSONParser::Value(Queue<IJSONElement*>& queue)
{
    Token token = queue.Front()->GetToken();

    if(token == OPEN_BRACE)
    {
        return Object(queue);
    }
    else if(token == OPEN_BRACKET)
    {
        return Array(queue);
    }
    else if(token == STRING || token == NUMBER ||token == BOOLEAN)
    {
        return queue.DeQueue();
    }

    assert(false);
}

JSONArray* JSONParser::Array(Queue<IJSONElement*>& queue)
{
    JSONArray* jsonArray = new JSONArray();

    while(queue.Front()->GetToken() != CLOSE_BRACKET)
    {
        assert(queue.Front()->GetToken() == OPEN_BRACKET ||
               queue.Front()->GetToken() == COMMA);

        queue.DeQueue();
        jsonArray->Add(Value(queue));
    }

    assert(queue.DeQueue()->GetToken() == CLOSE_BRACKET);

    return jsonArray;
}

JSONParser::JSONParser()
{

}

JSONObject* JSONParser::Parse(Queue<IJSONElement*>& queue)
{
    return Object(queue);
}

*/
