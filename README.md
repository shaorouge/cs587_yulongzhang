Coding Style:
1. Class name and function name always start with Capital letter.
   example:
	class MyClass{};
	void Function(){}

2. Private class variables always start with an underline.
   example:
	int _variable;

3. Space should be put between variables and operators.
   example:
	int i = 0;

4. Setters and Getters are inline functions.
   example:
	inline string GetName()
	{
	    return _name;
	}

	inline void SetName(const string& name)
	{
	    _name = name
	}
