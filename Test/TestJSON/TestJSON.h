
#ifndef _TESTJSON_H_
#define _TESTJSON_H_

#include "../../Headers/Engine/Json/FileReader.h"
#include "../../Headers/Engine/Json/JSONValue.h"
#include "../../Headers/Engine/Json/JSONReader.h"
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>

namespace Test
{

	class TestJSON : public CppUnit::TestFixture
	{
		CPPUNIT_TEST_SUITE(TestJSON);
		CPPUNIT_TEST(TestParser);
		CPPUNIT_TEST_SUITE_END();

	private:
		Engine::JSONReader _reader;
		Engine::JSONValue _jsonValue;

	public:
		void setUp();

		void tearDown();

		void TestParser();
	};
}

#endif
