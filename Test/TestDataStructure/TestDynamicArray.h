
#ifndef _TESTDYNAMICARRAY_H_
#define _TESTDYNAMICARRAY_H_
#include "../../Headers/Engine/Iterator/ArrayIterator.h"
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>

namespace Test
{
	typedef unsigned int uint;

	class TestDynamicArray : public CppUnit::TestFixture
	{
		CPPUNIT_TEST_SUITE(TestDynamicArray);
		//CPPUNIT_TEST(TestAdd);
		CPPUNIT_TEST(TestIterator);
		CPPUNIT_TEST_SUITE_END();

	private:
		Engine::DynamicArray<int> _dynArray;
		uint _size;

	public:
		void setUp();

		void tearDown();

		void TestAdd();

		void TestIterator();
	};
}

#endif /* TESTDYNAMICARRAY_H_ */
