
#ifndef _TESTLINKLIST_H_
#define _TESTLINKLIST_H_
#include "../../Headers/Engine/Iterator/LinkListIterator.h"
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>

namespace Test
{
	typedef unsigned int uint;

	class TestLinkList : public CppUnit::TestFixture
	{
		CPPUNIT_TEST_SUITE(TestLinkList);
		//CPPUNIT_TEST(TestAdd);
		//CPPUNIT_TEST(TestClear);
		//CPPUNIT_TEST(TestInsert);
		//CPPUNIT_TEST(TestMerge);
		//CPPUNIT_TEST(TestRemoveAt);
		//CPPUNIT_TEST(TestRemove);
		//CPPUNIT_TEST(TestExists);
		//CPPUNIT_TEST(TestIterator);
		CPPUNIT_TEST(TestRemoveCurrent);
		CPPUNIT_TEST_SUITE_END();

	private:
		Engine::LinkList<int> _linkList;
		Engine::LinkList<int> _linkList2;
		uint _size;

		void Return(uint i);

	public:
		void setUp();

		void tearDown();

		void TestAdd();

		void TestClear();

		void TestInsert();

		void TestMerge();

		void TestRemoveAt();

		void TestRemove();

		void TestExists();

		void TestIterator();

		void TestRemoveCurrent();
	};

} /* namespace Test */

#endif /* TESTLINKLIST_H_ */
