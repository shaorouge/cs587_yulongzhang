
#include "TestHashMap.h"
#include <iostream>
using namespace std;
using namespace Test;
using namespace Engine;

//CPPUNIT_TEST_SUITE_REGISTRATION(TestHashMap);

string TestHashMap::IntToStr(int i)
{
	stringstream ss;
	ss << i;
	return ss.str();
}

void TestHashMap::setUp()
{

}

void TestHashMap::tearDown()
{

}

void TestHashMap::TestAdd()
{
	int size = 100;

	for(int i = 0; i < size; i++)
	{
		_hashMap.Set(IntToStr(i), i);
	}

	for(int i = 0; i < size; i++)
	{
		cout << _hashMap.Get(IntToStr(i)) << endl;
	}
}

void TestHashMap::TestIterator()
{
	int size = 100;
	int count = 0;

	for(int i = 0; i < size; i++)
	{
		_hashMap.Set(IntToStr(i), i);
	}

	for(Iterator<int>* iterator = _hashMap.CreateIterator(); !iterator->IsDone(); iterator->Next())
	{
		cout << iterator->Current() << endl;
		count++;
	}

	cout << "Count: " << count << endl;
}
