
#include "TestLinkList.h"
#include <iostream>
using namespace std;
using namespace Test;
using namespace Engine;

//CPPUNIT_TEST_SUITE_REGISTRATION(TestLinkList);

void TestLinkList::Return(uint i)
{
	if(i % 30 ==0)
	{
		cout << endl;
	}
}

void TestLinkList::setUp()
{
	_size = 100;

	for(uint i = 0; i < _size; i++)
	{
		_linkList.Add(i);
	}

	for(uint i = 0; i < _size; i++)
	{
		_linkList2.Add(0 - i);
	}
}

void TestLinkList::tearDown()
{

}

void TestLinkList::TestAdd()
{
	for(uint i = 0; i < _size; i++)
	{
		uint temp = _linkList.Get(i);

		CPPUNIT_ASSERT(temp == i);

		cout << temp << " ";

		Return(i);
	}
}

void TestLinkList::TestClear()
{
	_linkList.Clear();

	CPPUNIT_ASSERT(_linkList.IsEmpty());
	CPPUNIT_ASSERT(_linkList.Size() == 0);
}

void TestLinkList::TestInsert()
{
	for(uint i = 0; i < _size; i++)
	{
		_linkList.Insert(10, i);

		int temp = _linkList.Get(10);

		CPPUNIT_ASSERT(temp == i);

		cout << temp << " ";

		Return(i);
	}
}

void TestLinkList::TestMerge()
{
	_linkList.Concact(&_linkList2);

	for(uint i = 0; i < _size * 2; i++)
	{
		int temp = _linkList.Get(i);

		cout << temp << " ";

		Return(i);
	}
}

void TestLinkList::TestRemoveAt()
{
	for(uint i = 0; i < 10; i++)
	{
		_linkList.RemoveAt(90);
	}

	for(uint i = 0; i < _size - 10; i++)
	{
		uint temp = _linkList.Get(i);

		cout << temp << " ";

		Return(i);
	}
}

void TestLinkList::TestRemove()
{
	for(int i = 80; i < 90; i++)
	{
		_linkList.Remove(i);
	}

	for(uint i = 0; i < _size - 10; i++)
	{
		uint temp = _linkList.Get(i);

		cout << temp << " ";

		Return(i);
	}
}

void TestLinkList::TestExists()
{
	for(uint i = 0; i < 100; i++)
	{
		CPPUNIT_ASSERT(_linkList.Exists(i));
	}

	for(uint i = 100; i < 200; i++)
	{
		CPPUNIT_ASSERT(_linkList.Exists(i) == false);
	}
}

void TestLinkList::TestIterator()
{
	int i = 0;

	for(Iterator<int>* iterator = _linkList.CreateIterator(); !iterator->IsDone(); iterator->Next(), i++)
	{
		CPPUNIT_ASSERT(iterator->Current() == i);

		cout << iterator->Current() << " ";

		Return(i);
	}
}

void TestLinkList::TestRemoveCurrent()
{
	int i = 0;

	Iterator<int>* iterator = _linkList.CreateIterator();

	for(iterator->First(); i < 50; i++)
	{
		if(iterator->Current() % 3 == 0)
		{
			_linkList.RemoveCurrent();
		}
		else
		{
			iterator->Next();
		}
	}

	i = 0;

	for(iterator->First(); !iterator->IsDone(); iterator->Next(), i++)
	{
		cout << iterator->Current() << " ";

		Return(i);
	}
}








