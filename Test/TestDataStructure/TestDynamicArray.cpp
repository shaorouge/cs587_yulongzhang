
#include "TestDynamicArray.h"
#include <iostream>
using namespace std;
using namespace Test;
using namespace Engine;

//CPPUNIT_TEST_SUITE_REGISTRATION(TestDynamicArray);

void TestDynamicArray::setUp()
{
	_size = 10000;

	for(uint i = 0; i < _size; i++)
	{
		_dynArray.Add(i);
	}
}

void TestDynamicArray::tearDown()
{

}

void TestDynamicArray::TestAdd()
{
	for(uint i = 0; i < _size; i++)
	{
		CPPUNIT_ASSERT(_dynArray[i] == i);

		cout << _dynArray[i] <<" ";

		if(i % 50 == 0)
		{
			cout << endl;
		}
	}
}

void TestDynamicArray::TestIterator()
{
	uint i = 0;

	for(Iterator<int>* iterator = _dynArray.CreateIterator(); !iterator->IsDone(); iterator->Next(), i++)
	{
		CPPUNIT_ASSERT(iterator->Current() == i);

		cout << iterator->Current() << " " << i << " ";

		if(i % 50 == 0)
		{
			cout << endl;
		}
	}
}
