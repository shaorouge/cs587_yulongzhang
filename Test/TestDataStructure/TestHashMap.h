
#ifndef TESTHASHMAP_H_
#define TESTHASHMAP_H_
#include "../../Headers/Engine/Iterator/HashMapIterator.h"
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>

namespace Test
{

	class TestHashMap : public CppUnit::TestFixture
	{
		CPPUNIT_TEST_SUITE(TestHashMap);
		//CPPUNIT_TEST(TestAdd);
		CPPUNIT_TEST(TestIterator);
		CPPUNIT_TEST_SUITE_END();

	private:
		Engine::HashMap<int> _hashMap;

	public:
		string IntToStr(int i);

		void setUp();

		void tearDown();

		void TestAdd();

		void TestIterator();
	};

} /* namespace Test */

#endif /* TESTHASHMAP_H_ */
