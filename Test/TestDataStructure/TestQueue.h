
#ifndef TESTQUEUE_H_
#define TESTQUEUE_H_
#include "../../Headers/Engine/DataStructure/Queue.h"
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>

namespace Test
{

	class TestQueue : public CppUnit::TestFixture
	{
		CPPUNIT_TEST_SUITE(TestQueue);
		CPPUNIT_TEST(TestEnDeQueue);
		CPPUNIT_TEST_SUITE_END();

	private:
		Engine::Queue<int> _queue;

	public:
		void setUp();

		void tearDown();

		void TestEnDeQueue();
	};

} /* namespace Test */

#endif /* TESTQUEUE_H_ */
