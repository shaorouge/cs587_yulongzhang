
#ifndef _JSONVALUE_H_
#define _JSONVALUE_H_
#include "../Iterator/ArrayIterator.h"
#include "../Iterator/HashMapIterator.h"

namespace Engine
{
	class JSONValue
	{
	private:
		enum Type
		{
			Number,
			String,
			Boolean,
			Array,
			Object,
			Null
		};

		union ValueHolder
		{
			char* str;
			int num;
			bool boolean;
			DynamicArray<JSONValue*>* array;
			HashMap<JSONValue*>* map;
		} _valueHolder;

		Type _type;

	public:
		JSONValue();
		~JSONValue();

		int AsNumber();
		string AsString();
		bool AsBool();

		JSONValue* operator[](int index);
		JSONValue* operator[](const string& key);
		void operator=(JSONValue& value);

		void SetValue(const string& value);
		void SetValue(const int value);
		void SetValue(const bool value);

		/**
		 * Add new value to hash map
		 */
		JSONValue* NewValue(const string& value);

		/**
		 * Add new value to array
		 */
		JSONValue* AddValue();

		Engine::Iterator<JSONValue*>* ArrayIterator();
		void Destory();
	};
}

#endif
