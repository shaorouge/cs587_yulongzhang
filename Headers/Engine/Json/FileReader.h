
#ifndef _FILEREADER_H_
#define _FILEREADER_H_

#include <cstdlib>
#include <iostream>
#include <fstream>
using namespace std;

namespace Engine
{
	class FileReader
	{
	private:
		string _infileName;
		string _outfileName;
		FileReader();

	public:
		static FileReader* Instance();
		string ReadInfile();
		void WriteOutfile(string str);
		void ClearOutfile();

		void SetInfileName(string name)
		{
			_infileName = name;
		}

		void SetOutfileName(string name)
		{
			_outfileName = name;
		}

	};
}

#endif
