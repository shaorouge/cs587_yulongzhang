
#ifndef _JSONREADER_H_
#define _JSONREADER_H_
#include "JSONValue.h"
#include "../DataStructure/Stack.h"

namespace Engine
{
	enum Token
	{
		ObjectBegin,			//{
		ObjectEnd,				//}

		ArrayBegin,				//[
		ArrayEnd,				//]

		String,					//"
		Number,
		True,					//true
		False,					//false
		Null,

		ArraySeperator,			//,
		MemberSeperator			//:
	};

	class JSONReader
	{
	private:
		Token _currentToken;
		int _currentNum;
		string _currentStr;
		Stack<JSONValue*> _valueStack;

		char* _begin;			//document position record
		char* _end;
		char* _current;

		void ReadValue();
		void ReadToken();
		int ReadNumber();
		const string ReadString();
		void ReadObject();
		void ReadArray();
		char GetNextChar();
		bool Match(const string& matchStr, uint length);

	public:
		JSONReader();
		~JSONReader();

		JSONValue* Parse(const string& document);
	};
}

#endif
