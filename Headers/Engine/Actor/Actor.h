
#ifndef _ACTOR_H_
#define _ACTOR_H_
#include <iostream>
#include "../Render/IDrawable.h"
#include "../Physics/Rigidbody.h"
#include "../Physics/ICollidable.h"
#include "../Scene/SceneNode.h"

namespace Engine
{
	class Actor
	{
	protected:
		string _id;
		SceneNode* _sceneNode;
		IDrawable* _drawable;
		Rigidbody* _rigidbody;
		ICollidable* _collidable;
		int _layer;					//Indicates which layer the Actor is at

	public:
		Actor();
		Actor(const string& id, SceneNode* sceneNode, IDrawable* drawable, Rigidbody* rigidbody, ICollidable* collidable, int layer);
		virtual ~Actor();

		inline SceneNode* GetSceneNode()
		{
			return _sceneNode;
		}

		inline void SetSceneNode(SceneNode* sceneNode)
		{
			_sceneNode = sceneNode;
		}

		inline IDrawable* GetDrawable()
		{
			return _drawable;
		}

		inline void SetDrawable(IDrawable* drawable)
		{
			_drawable = drawable;
		}

		inline Rigidbody* GetRigidbody()
		{
			return _rigidbody;
		}

		inline void SetRigidbody(Rigidbody* rigidbody)
		{
			_rigidbody = rigidbody;
		}

		inline ICollidable* GetColliable()
		{
			return _collidable;
		}

		inline void SetCollidable(ICollidable* collidable)
		{
			_collidable = collidable;
		}

		inline string GetId() const
		{
			return _id;
		}

		inline void SetId(const string& id)
		{
			_id = id;
		}

		inline int GetLayer()
		{
			return _layer;
		}

		inline void SetLayer(int layer)
		{
			_layer = layer;
		}
	};
}

#endif
