
#ifndef _RENDERMANAGER_H_
#define _RENDERMANAGER_H_
#include "../DataStructure/HashMap.h"
#include "../DataStructure/DynamicArray.h"
#include "../Resource/IResourceLoader.h"
#include "../Scene/SceneManager.h"
#include "../Resource/RenderResource.h"
#include "../Camera/Camera.h"
#include "IDrawable.h"

namespace Engine
{
	class RenderManager : public IResourceLoader
	{
	private:
		SDL_Window* _window;
		SDL_Renderer* _renderer;
		SceneManager* _sceneManager;

	public:
		RenderManager();
		~RenderManager();
		void Render();
		void Init(JSONValue* data);
		IResource* LoadFromJSON(JSONValue* data);

		inline void SetSceneManager(SceneManager* sceneManager)
		{
			_sceneManager = sceneManager;
		}
	};
}

#endif
