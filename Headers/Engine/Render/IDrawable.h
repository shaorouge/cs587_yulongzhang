
#ifndef _IDRAWABLE_H_
#define _IDRAWABLE_H_
#include <iostream>
#include <assert.h>
#include "../Resource/RenderResource.h"
#include "../Math/Vector2.h"
#include "../Math/Matrix3.h"
using namespace std;

namespace Engine
{
	class IDrawable
	{
	protected:
		RenderResource* _renderResource;

	public:
		virtual ~IDrawable(){}
		virtual void Draw(SDL_Renderer* renderer) = 0;
		virtual void ModifyPosition(float x, float y) = 0;
		virtual IDrawable* Clone() = 0;

		inline RenderResource* GetResource()
		{
			return _renderResource;
		}

		inline void SetResource(RenderResource* renderResource)
		{
			_renderResource = renderResource;
		}

		inline void Destory()
		{
			delete this;
		}
	};
}

#endif
