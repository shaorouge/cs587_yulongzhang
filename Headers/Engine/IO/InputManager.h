
#ifndef _INPUTMANAGER_H_
#define _INPUTMANAGER_H_
#include "../Tick/ITickable.h"
#include "../Iterator/ArrayIterator.h"
#include "IInputListener.h"
#include "Keyboard.h"

namespace Engine
{
	class InputManager : public IKeyListener, public ITickable
	{
	private:
		static InputManager* _instance;
		Keyboard* _keyboard;
		DynamicArray<IInputListener*> _listeners;

	public:
		InputManager();
		~InputManager();

		bool keyPressed(const KeyEvent& keyEvent);
		bool keyReleased(const KeyEvent& keyEvent);

		void Tick(const float deltaTime);
		void AddListener(IInputListener* listener);

		static InputManager* Instance();
		void Destory();

		inline  Keyboard* GetKeyboard()
		{
			return _keyboard;
		}

		inline void SetKeyboard(Keyboard* keyboard)
		{
			delete _keyboard;

			_keyboard = keyboard;
		}
	};
}

#endif
