
#ifndef _KEYBOARD_H_
#define _KEYBOARD_H_
#include <iostream>
#include "IKeyListener.h"
#include "../DataStructure/HashMap.h"
using namespace std;

namespace Engine
{
	class Keyboard
	{
	private:
		HashMap<KeyCode> _keyMap;
		Uint8 _keysState[256];			//Record the state of each key in keyboard, either "down" or "up"
		IKeyListener* _keyListener;
		bool _buffered;
		int _numEvents;					//The maximum number of events to retrieve from event queue

	public:
		Keyboard();
		Keyboard(bool buffered);
		Keyboard(bool buffered, int numEvents);
		~Keyboard();

		/**
		 * Copies the state of the keys into the sent buffer
		 * 1 is down and 0 is up
		 */
		void CopyKeyStates(unsigned char keys[256]) const;

		void Capture();

		/**
		 * Translates KeyCode to string
		 */
		string GetAsString(KeyCode kc);

		void Init();

		/**
		 * Returns true if key is down
		 */
		inline bool IsKeyDown(const KeyCode kc) const
		{
			return _keysState[kc] == 1;
		}

		/**
		 * Register a Keyboard Listener, only one allowed for simplicity
		 */
		inline void SetEventCallBack(IKeyListener* keyListener)
		{
			_keyListener = keyListener;
		}

		/**
		 * Returns currently set callback or NULL
		 */
		inline IKeyListener* GetEventCallBack() const
		{
			return _keyListener;
		}

		/**
		 * If is set buffered, the keyboard can receive input
		 */
		inline void SetBuffered(const bool buffered)
		{
			_buffered = buffered;
		}

		inline void SetNumEvents(const int num)
		{
			_numEvents = num;
		}

		inline KeyCode GetKeyCode(const string& keyStr)
		{
			if(_keyMap.Exists(keyStr))
			{
				return _keyMap.Get(keyStr);
			}

			return KC_UNASSIGNED;
		}
	};
}

#endif
