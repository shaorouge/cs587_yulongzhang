
#ifndef IINPUTLISTENER_H_
#define IINPUTLISTENER_H_
#include "KeyEvent.h"

namespace Engine
{
class IInputListener
{
	public:
		virtual ~IInputListener(){}
		virtual bool KeyPressed(const KeyEvent& keyEvent) = 0;
		virtual bool KeyReleased(const KeyEvent& keyEvent) = 0;
	};
}

#endif
