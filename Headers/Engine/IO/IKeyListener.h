
#ifndef _IKEYLISTENER_H_
#define _IKEYLISTENER_H_
#include "KeyEvent.h"

namespace Engine
{
	class IKeyListener
	{
	public:
		virtual ~IKeyListener(){}
		virtual bool keyPressed(const KeyEvent& keyEvent) = 0;
		virtual bool keyReleased(const KeyEvent& keyEvent) = 0;
	};
}

#endif
