
#ifndef _HASHMAPITERATOR_H_
#define _HASHMAPITERATOR_H_
#include "../DataStructure/HashMap.h"

namespace Engine
{
	template <typename T>
	class HashMapIterator : public Iterator<T>
	{
	private:
		HashMap<T>* _hashMap;
		uint _currentIndex;
		HashEntry<T>* _currentEntry;

	public:
		HashMapIterator(HashMap<T>* hashMap);
		~HashMapIterator();
		void First();
		void Next();
		bool IsDone();
		const T Current();
	};

	template <typename T>
	HashMapIterator<T>::HashMapIterator(HashMap<T>* hashMap) : _hashMap(hashMap)
	{
		assert(!_hashMap->IsEmpty());

		First();
	}

	template <typename T>
	HashMapIterator<T>::~HashMapIterator()
	{

	}

	template <typename T>
	void HashMapIterator<T>::First()
	{
		_currentIndex = 0;
		_currentEntry = _hashMap->_entries[0];

		while(_currentEntry == NULL)
		{
			_currentIndex++;
			_currentEntry = _hashMap->_entries[_currentIndex];
		}
	}

	template <typename T>
	void HashMapIterator<T>::Next()
	{
		if(_currentEntry != NULL && _currentEntry->next != NULL)
		{
			_currentEntry = _currentEntry->next;
		}
		else
		{
			_currentEntry = NULL;
			_currentIndex++;

			for(; _currentIndex < _hashMap->_tableSize && _currentEntry == NULL; _currentIndex++)
			{
				_currentEntry = _hashMap->_entries[_currentIndex];
			}
		}
	}

	template <typename T>
	bool HashMapIterator<T>::IsDone()
	{
		if(_currentIndex >= _hashMap->_tableSize && _currentEntry == NULL)
		{
			return true;
		}

		return false;
	}

	template <typename T>
	const T HashMapIterator<T>::Current()
	{
		assert(_currentEntry != NULL);

		return _currentEntry->element;
	}

	template <typename T>
	Iterator<T>* HashMap<T>::CreateIterator()
	{
		return new HashMapIterator<T>(this);
	}
}

#endif
