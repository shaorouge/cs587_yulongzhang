
#ifndef _HEAPITERATOR_H_
#define _HEAPITERATOR_H_
#include "../DataStructure/Heap.h"

namespace Engine
{
	template <typename T>
	class HeapIterator : public Iterator<T>
	{
	private:
		Heap<T>* _heap;
		uint _currentIndex;

	public:
		HeapIterator(Heap<T>* Heap);
		~HeapIterator();
		void First();
		void Next();
		bool IsDone();
		const T Current();
	};

	template <typename T>
	HeapIterator<T>::HeapIterator(Heap<T>* heap) : _heap(heap), _currentIndex(0)
	{

	}

	template <typename T>
	HeapIterator<T>::~HeapIterator()
	{

	}

	template <typename T>
	void HeapIterator<T>::First()
	{
		_currentIndex = 0;
	}

	template <typename T>
	void HeapIterator<T>::Next()
	{
		_currentIndex++;
	}

	template <typename T>
	bool HeapIterator<T>::IsDone()
	{
		return _currentIndex >= _heap->Size();
	}

	template <typename T>
	const T HeapIterator<T>::Current()
	{
		assert(!IsDone());

		return _heap->_dynArray[_currentIndex].element;
	}

	template <typename T>
	Iterator<T>* Heap<T>::CreateIterator()
	{
		return new HeapIterator<T>(this);
	}
}

#endif
