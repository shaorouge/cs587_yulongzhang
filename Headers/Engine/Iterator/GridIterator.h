
#ifndef _GRIDITERATOR_H_
#define _GRIDITERATOR_H_
#include "../DataStructure/Grid2.h"

namespace Engine
{
	template <typename T>
	class GridIterator : public Iterator<T>
	{
	private:
		Grid2<T>* _grid;
		uint _currentIndex;

	public:
		GridIterator(Grid2<T>* grid);
		~GridIterator();
		void First();
		void Next();
		bool IsDone();
		const T Current();
	};

	template <typename T>
	GridIterator<T>::GridIterator(Grid2<T>* grid) : _grid(grid), _currentIndex(0)
	{

	}

	template <typename T>
	GridIterator<T>::~GridIterator()
	{

	}

	template <typename T>
	void GridIterator<T>::First()
	{
		while(_grid->_array[_currentIndex] == _grid->_defaultData)
		{
			_currentIndex++;
		}
	}

	template <typename T>
	void GridIterator<T>:: Next()
	{
		_currentIndex++;

		First();
	}

	template <typename T>
	bool GridIterator<T>::IsDone()
	{
		return _currentIndex >= _grid->Size();
	}

	template <typename T>
	const T GridIterator<T>::Current()
	{
		return _grid->_array[_currentIndex];
	}

	template <typename T>
	Iterator<T>* Grid2<T>::CreateIterator()
	{
		return new GridIterator<T>(this);
	}
}

#endif
