
#ifndef _ARRAYITERATOR_H_
#define _ARRAYITERATOR_H_
#include "../DataStructure/DynamicArray.h"

namespace Engine
{
	template <typename T>
	class ArrayIterator : public Iterator<T>
	{
	private:
		DynamicArray<T>* _dynArray;
		uint _currentIndex;

	public:
		ArrayIterator(DynamicArray<T>* dynArray);
		~ArrayIterator();
		void First();
		void Next();
		bool IsDone();
		const T Current();
	};

	template <typename T>
	ArrayIterator<T>::ArrayIterator(DynamicArray<T>* dynArray) : _dynArray(dynArray), _currentIndex(0)
	{

	}

	template <typename T>
	ArrayIterator<T>::~ArrayIterator()
	{

	}

	template <typename T>
	void ArrayIterator<T>::First()
	{
		_currentIndex = 0;
	}

	template <typename T>
	void ArrayIterator<T>::Next()
	{
		_currentIndex++;
	}

	template <typename T>
	bool ArrayIterator<T>::IsDone()
	{
		return _currentIndex >= _dynArray->Size();
	}

	template <typename T>
	const T ArrayIterator<T>::Current()
	{
		assert(!IsDone());

		return _dynArray->_array[_currentIndex];
	}

	template <typename T>
	Iterator<T>* DynamicArray<T>::CreateIterator()
	{
		return new ArrayIterator<T>(this);
	}
}

#endif
