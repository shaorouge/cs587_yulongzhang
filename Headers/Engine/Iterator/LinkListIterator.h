
#ifndef _LINKLISTITERATOR_H_
#define _LINKLISTITERATOR_H_
#include <iostream>
#include "../DataStructure/LinkList.h"

namespace Engine
{
	template <typename T>
	class LinkListIterator : public Iterator<T>
	{
	private:
		LinkList<T>* _linkList;

	public:
		LinkListIterator(LinkList<T>* linkList);
		~LinkListIterator();
		void First();
		void Next();
		bool IsDone();
		const T Current();
	};

	template <typename T>
	LinkListIterator<T>::LinkListIterator(LinkList<T>* linkList) : _linkList(linkList)
	{
		_linkList->_preCurrent = _linkList->_head;
	}

	template <typename T>
	LinkListIterator<T>::~LinkListIterator()
	{

	}

	template <typename T>
	void LinkListIterator<T>::First()
	{
		_linkList->_preCurrent = _linkList->_head;
	}

	template <typename T>
	void LinkListIterator<T>::Next()
	{
		assert(_linkList->_preCurrent);

		_linkList->_preCurrent = _linkList->_preCurrent->next;
	}

	template <typename T>
	bool LinkListIterator<T>::IsDone()
	{
		return _linkList->_preCurrent->next == 0;
	}

	template <typename T>
	const T LinkListIterator<T>::Current()
	{
		assert(!IsDone());

		return _linkList->_preCurrent->next->element;
	}

	template <typename T>
	Iterator<T>* LinkList<T>::CreateIterator()
	{
		return new LinkListIterator<T>(this);
	}
}

#endif
