
#ifndef _ITERATOR_H_
#define _ITERATOR_H_

namespace Engine
{
	template <typename T>
	class Iterator
	{
	public:
		virtual ~Iterator(){};
		virtual void First() = 0;
		virtual void Next() = 0;
		virtual bool IsDone() = 0;
		virtual const T Current() = 0;

		void Destory()
		{
			delete this;
		}
	};
}

#endif
