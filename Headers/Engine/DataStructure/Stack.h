
#ifndef _STACK_H_
#define _STACK_H_
#include "DynamicArray.h"

namespace Engine
{
	template <typename T>
	class Stack
	{
	private:
		DynamicArray<T> _array;
		uint _top;

	public:
		Stack();
		~Stack();

		void Push(T object);
		T Pop();
		T Peek();

		bool IsEmpty();
		uint Size();
	};

	template <typename T>
	Stack<T>::Stack() : _top(0)
	{

	}

	template <typename T>
	Stack<T>::~Stack()
	{

	}

	template <typename T>
	void Stack<T>::Push(T object)
	{
		_array.Add(object);
		_top++;
	}

	template <typename T>
	T Stack<T>::Pop()
	{
		assert(_top > 0);

		T object = _array[--_top];
		_array.RemoveAt(_top);

		return object;
	}

	template <typename T>
	T Stack<T>::Peek()
	{
		assert(_top > 0);

		return _array[_top - 1];
	}

	template <typename T>
	bool Stack<T>::IsEmpty()
	{
		return _top == 0;
	}

	template <typename T>
	uint Stack<T>::Size()
	{
		return _array.Size();
	}
}

#endif
