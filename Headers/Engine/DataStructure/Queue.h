
#ifndef _QUEUE_H_
#define _QUEUE_H_
#include "LinkList.h"

namespace Engine
{
	template <typename T>
	class Queue
	{
	private:
		LinkList<T> _list;

	public:

		// -------Public Methods ---//

		Queue();
		~Queue();
		bool IsEmpty();                 //Check if the size of the queue is 0
		uint Size();                	//Return the size of the queue
		T Front();                   	//Return the first element of the queue
		T Back();                    	//Return the last element of the queue
		void Clear();
		void EnQueue(T element);       	//Add a new element at the back of the queue
		T DeQueue();                	//Remove the first element in the queue
	};

	template<typename T>
	 Queue<T>::Queue()
	{

	}

	template<typename T>
	 Queue<T>::~Queue()
	{

	}

	template<typename T>
	 bool Queue<T>::IsEmpty()
	{
		return _list.IsEmpty();
	}

	template<typename T>
	 uint Queue<T>::Size()
	{
		return _list.Size();
	}

	template<typename T>
	 T Queue<T>::Front()
	{
		return _list.Get(0);
	}

	template<typename T>
	 T Queue<T>::Back()
	{
		return _list.Get(Size() - 1);
	}

	template<typename T>
	void Queue<T>::Clear()
	{
		_list.Clear();
	}

	template<typename T>
	 void Queue<T>::EnQueue(T element)
	{
		_list.Add(element);
	}

	template<typename T>
	T Queue<T>::DeQueue()
	{
		T returnValue = Front();
		_list.RemoveAt(0);

		return returnValue;
	}
}

#endif
