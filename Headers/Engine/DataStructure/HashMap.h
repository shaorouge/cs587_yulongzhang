
#ifndef _HASHMAP_H_
#define _HASHMAP_H_

#include <string>
#include <stdio.h>
#include <iostream>
#include "DynamicArray.h"
using namespace std;

namespace Engine
{
	template <typename T>
	class HashMapIterator;

	template <typename T>
	struct HashEntry
	{
		string key;
		T element;
		HashEntry* next;
		HashEntry() : next(NULL){}
		HashEntry(string k) : key(k), next(NULL){};
		HashEntry(string k, T element):key(k), element(element), next(NULL){}
	};

	template <typename T>
	class HashMap
	{
	private:
		const uint _tableSize;
		HashEntry<T>** _entries;
		DynamicArray<string> _keys;

		friend class HashMapIterator<T>;

		// --- Private Methods ---//

		uint HashFunc(const char* key);

	public:

		// --- Constructors and Destructor ---//

		HashMap();
		HashMap(uint size);
		~HashMap();

		// --- Element access --- //

		bool Exists(const string key);
		DynamicArray<string>& GetAllKeys();
		T& operator[](const string& key);
		T Get(const string& key);

		// --- Modifiers --- //

		void Set(const string& key, T element);
		bool Remove(const string& key);
		void Clear();
		bool IsEmpty();

		Iterator<T>* CreateIterator();
	};

	/**
	 * FNV-hash (http://www.isthe.com/chongo/tech/comp/fnv/)
	 *
	 * @param: the key of the element
	 *
	 * @return: an index of one backet of the hash table
	 */
	template <typename T>
	uint HashMap<T>::HashFunc(const char* key)
	{
		uint hash = 5381;
		int c;

		 while((c = *key++))
		{
			hash = ((hash << 5) + hash) + c;
		}

		return hash % _tableSize;
	}


	/**
	 * Default Constructor
	 */
	template <typename T>
	HashMap<T>::HashMap() : _tableSize(100)
	{
		_entries = new HashEntry<T>* [_tableSize];

		for(uint i = 0; i < _tableSize; i++)
		{
			_entries[i] = NULL;
		}
	}

	/**
	 * Constructor
	 *
	 * @param: hash table size from user
	 */
	template <typename T>
	HashMap<T>::HashMap(uint size) : _tableSize(size)
	{
		_entries = new HashEntry<T>* [_tableSize];

		for(uint i = 0; i < _tableSize; i++)
		{
			_entries[i] = NULL;
		}
	}

	/**
	 * Destructor
	 */
	template <typename T>
	HashMap<T>::~HashMap()
	{

		for(uint i = 0; i < _tableSize; i++)
		{
			HashEntry<T>* entry = _entries[i];
			HashEntry<T>* removed = entry;

			while(entry != NULL)
			{
				removed = entry;
				entry = entry->next;
				delete removed;
			}
		}
	}

	/**
	 * Return true if an element with the given key is in this hash map
	 *
	 * @param: given key from user
	 *
	 * @return: boolean
	 */
	template <typename T>
	bool HashMap<T>::Exists(const string key)
	{
		uint index = HashFunc(key.c_str());

		HashEntry<T>* entry = _entries[index];

		while(entry != NULL)
		{
			if(entry->key == key)
			{
				return true;
			}
			entry = entry->next;
		}

		return false;
	}

	/**
	 * Return an array with all the keys in this hash map
	 *
	 * @return: a dynamic array
	 */
	template <typename T>
	DynamicArray<string>& HashMap<T>::GetAllKeys()
	{
		return _keys;
	}

	/**
	 * Return an element with a given key from user and allow assignment
	 *
	 * @param: given key from user
	 *
	 * @return: an element with given key
	 */
	template <typename T>
	T& HashMap<T>::operator[](const string& key)
	{
		uint index = HashFunc(key.c_str());
		HashEntry<T>* entry = _entries[index];

		if(entry == NULL)
		{
			_keys.Add(key);
			entry = new HashEntry<T>(key);
			return entry->element;
		}

		if(entry->key == key)
			return entry->element;

		while(entry->next != NULL)
		{
			entry = entry->next;

			if(entry->key == key)
				return entry->element;
		}

		_keys.Add(key);
		HashEntry<T>* newEntry = new HashEntry<T>(key);
		entry->next = newEntry;
		return newEntry->element;
	}

	/**
	 * Return an element with a given key from user
	 *
	 * @param: given key from user
	 *
	 * @return: an element with given key
	 */
	template <typename T>
	T HashMap<T>::Get(const string& key)
	{
		uint index = HashFunc(key.c_str());
		HashEntry<T>* entry = _entries[index];

		while(entry != NULL)
		{
			if(entry->key == key)
			{
				return entry->element;
			}

			entry = entry->next;
		}

		assert(false);
	}

	/**
	 * Add a new element and its key into the hash table
	 *
	 * @param: the key of the element
	 * @param: element in type T
	 */
	template <typename T>
	void HashMap<T>::Set(const string& key, T element)
	{
		uint index = HashFunc(key.c_str());

		HashEntry<T>* entry = _entries[index];
		HashEntry<T>* n = new HashEntry<T>(key, element);

		if(entry == NULL)
		{
			_keys.Add(key);
			_entries[index] = n;
			return;
		}

		if(entry->key == key)
		{
			entry->element = element;
		}

		while(entry->next != NULL)
		{
			entry = entry->next;

			if(entry->key == key)
			{
				entry->element = element;
				return;
			}
		}

		entry->next = n;
		_keys.Add(key);
	}

	/**
	 * Remove a hash entry from the hash table with the given key
	 *
	 * @param: the key of the element
	 *
	 * @return: a boolean depends on if an element is successfully removed
	 */
	template <typename T>
	bool HashMap<T>::Remove(const string& key)
	{
		uint index = HashFunc(key.c_str());

		if(_entries[index] != NULL && _entries[index]->key == key)
		{
			_keys.Remove(key);
			HashEntry<T>* removed = _entries[index];
			_entries[index] = _entries[index]->next;
			delete removed;
			return true;
		}

		HashEntry<T>* entry = _entries[index]->next;
		HashEntry<T>* preEntry = entry;

		while(entry != NULL)
		{
			if(entry->key == key)
			{
				_keys.Remove(key);
				preEntry->next = entry->next;
				delete entry;
				return true;
			}

			preEntry = entry;
			entry = entry->next;
		}

		return false;
	}

	template <typename T>
	void HashMap<T>::Clear()
	{
		for(uint i = 0; i < _tableSize; i++)
		{
			HashEntry<T>* currentEntry = _entries[i];

			while(currentEntry != NULL)
			{
				HashEntry<T>* removed = currentEntry;
				currentEntry = currentEntry->next;

				delete removed;
			}
		}
	}

	template <typename T>
	bool HashMap<T>::IsEmpty()
	{
		return _keys.IsEmpty();
	}
}

#endif






















