
#ifndef _HEAP_H_
#define _HEAP_H_
#include "../Iterator/ArrayIterator.h"

namespace Engine
{
	template <typename T>
	class HeapIterator;

	template <typename T>
	class Heap
	{
	private:

		// -------Private Properties ---//

		struct Item
		{
			int priority;
			T element;
			Item() : priority(0){}
			Item(int k, T e):priority(k),element(e){}
		};

		DynamicArray<Item> _dynArray;

		// -------Private Methods ---//

		int Parent(int childIndex);                  //Return the index of the parent
		int LeftChild(int parentIndex);              //Return the index of the left child
		int RightChild(int parentIndex);             //Return the index of the right child
		void UpHeap(int i);                          //Maintain the heap-order property by moving up the element with index i
		void DownHeap(int i);                        //Maintain the heap-order property by moving down element with index i
		void Swap(int index1, int index2);           //Swap the element at index1 with the element at index2

		friend class HeapIterator<T>;

	public:

		// -------Public Methods ---//

		Heap();
		~Heap();
		int Size();                                  //Return the size of the heap
		int Capacity();                              //Return the capacity of the heap
		bool IsEmpty();                                 //Check if the size of the heap is 0
		T Peek();                                     //Return the smallest element of the heap
		int MinPriority();
		void Push(int priority, T element);    //Add a new element into the heap
		T Pop();                                   //Remove the element with the smallest priority in the heap
		bool HasElement(T element);
		Iterator<T>* CreateIterator();
	};

	/**
	 Parent(int childIndex): Private function
	 Description: The parent's index is (childIndex - 1) * 2
	 Parameter:
	 childIndex:int
	 The index of the element
	 Return:
	 -1 if childIndex is 0, it has no parent
	 else
	 the parent's index of the childIndex
	*/
	template <typename T>
	inline int Heap<T>::Parent(int childIndex)
	{
		if (childIndex != 0)
		{
			int i = (childIndex - 1) >> 1;
			return i;
		}
		return -1;
	}

	/**
	 LeftChild(int parentIndex): Private function
	 Description: The left child's index is (parentIndex * 2) + 1
	 Parameter:
	 parentIndex:int
	 The index of the element
	 Return:
	 -1 if left child's index exceeds the accessiable length of the dynamic array
	 else
	 the left child's index of the parentIndex
	*/
	template <typename T>
	inline int Heap<T>::LeftChild(int parentIndex)
	{
		int i = (parentIndex << 1) + 1;
		return (i < Size()) ? i : -1;
	}

	/**
	 RightChild(int parentIndex): Private function
	 Description: The right child's index is (parentIndex * 2) + 2
	 Parameter:
	 parentIndex:int
	 The index of the element
	 Return:
	 -1 if right child's index exceeds the accessiable length of the dynamic array
	 else
	 the right child's index of the parentIndex
	 */
	template <typename T>
	inline int Heap<T>::RightChild(int parentIndex)
	{
		int i = (parentIndex << 1) + 2;
		return (i < Size()) ? i : -1;
	}

	/**
	 UpHeap(int index): Private function
	 Description: Maintain the property of the heap
	 Bubble up the element at index
	 Parameter:
	 index:int
	 The index of an element
	*/
	template <typename T>
	void Heap<T>::UpHeap(int index)
	{
		int pIndex = Parent(index);

		while( pIndex >= 0 &&
			  _dynArray[pIndex].priority > _dynArray[index].priority )
		{
			Swap(pIndex, index);

			index = pIndex;
			pIndex = Parent(index);
		}

		return;
	}

	/**
	 DownHeap(int index): Private function
	 Description: Maintain the property of the heap
	 Bubble down the element at index
	 Parameter:
	 index:int
	 The index of an element
	*/
	template <typename T>
	void Heap<T>::DownHeap(int index)
	{
		int lIndex;     //index of left child
		int rIndex;     //index of right child
		int smallest;   //stores the smallest priority among the priorities of
		//left child, right child and parent

		lIndex = LeftChild(index);
		rIndex = RightChild(index);

		if( lIndex >= 0 &&
		   _dynArray[lIndex].priority <
		   _dynArray[index].priority )
			smallest = lIndex;
		else
			smallest = index;

		if( rIndex >= 0 &&
		   _dynArray[rIndex].priority <
		   _dynArray[smallest].priority)
			smallest = rIndex;

		if( smallest != index )
		{
			Swap(index, smallest);
			DownHeap(smallest);
		}
	}

	/**
	 Swap(int index1, int index2): Private function
	 Description: Swap two elements at index1 and index2\
	 Parameter:
	 index1:int, index2:int
	 The indices of elements
	*/
	template <typename T>
	void Heap<T>::Swap(int index1, int index2)
	{
		Item temp = _dynArray[index1];
		_dynArray[index1] = _dynArray[index2];
		_dynArray[index2] = temp;
	}

	/**
	 Heap(): Constructor
	 Description: Initialize a dynamic array
	*/
	template <typename T>
	inline Heap<T>::Heap():_dynArray()
	{

	}

	/**
	 Heap(): Destructor
	 Description: Delete the Dynamic Array
	 */
	template <typename T>
	inline Heap<T>::~Heap()
	{
	}

	/**
	 GetSize(): Public function
	 Description: Return the size of the heap
	 Return:
	 The accessiable length of the dynamic array
	 */
	template <typename T>
	inline int Heap<T>::Size()
	{
		return _dynArray.Size();
	}

	/**
	 IsEmpty(): Public function
	 Description: Check if the size of the heap is 0
	 Return:
	 true if the accessable size of the dynamic array is 0
	 false if the accessable size of the dynamic array is larger than 0
	*/
	template <typename T>
	inline bool Heap<T>::IsEmpty()
	{
		return _dynArray.Size() == 0;
	}

	/**
	 GetMin(): Public function
	 Description: Return the first element of the heap
	 Return:
	 The first element of the dynamic array
	*/
	template <typename T>
	inline T Heap<T>::Peek()
	{
		return _dynArray[0].element;
	}

	template <typename T>
	inline int Heap<T>::MinPriority()
	{
		return _dynArray[0].priority;
	}
	/**
	 InsertElement(): Public function
	 Description: Add a new element into the heap
	 Parameter:
	 priority:int
	 The priority of the element
	 element:type<T>
	 The element that the user want to put into the heap
	 */
	template <typename T>
	inline void Heap<T>::Push(int priority, T element)
	{
		_dynArray.Add(Item(priority, element));
		UpHeap(Size() - 1);

		return;
	}

	/**
	 RemoveMin(): Public function
	 Description: Remove the first element of the heap
	 */
	template <typename T>
	inline T Heap<T>::Pop()
	{
		assert(Size() > 0 );

		T min = _dynArray[0].element;

		Swap(0, Size() - 1);

		_dynArray.RemoveAt(_dynArray.Size() - 1);

		if( Size() > 1 )
		{
			DownHeap(0);
		}

		return min;
	}

	template <typename T>
	bool Heap<T>::HasElement(T element)
	{
		for(int i = 0; i < _dynArray.Size(); i++)
		{
			if(_dynArray[i].element == element)
				return true;
		}
		return false;
	}
}

#endif
