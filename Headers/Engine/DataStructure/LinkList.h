
#ifndef _LINKLIST_H_
#define _LINKLIST_H_
#include <assert.h>
#include <iostream>
#include "../Iterator/Iterator.h"

namespace Engine
{
	typedef unsigned int uint;

	template <typename T>
	class LinkListIterator;

	template <typename T>
	class LinkList
	{
	private:

		struct Node
		{
			Node* next;
			T element;

			Node() : next(NULL) {};
			Node(T newElement) : next(NULL), element(newElement) {};
			~Node(){};
		};

		friend class LinkListIterator<T>;

		uint _size;
		Node* _head;					//The first node of link list, contains no element
		Node* _tail;					//The last node of link list
		Node* _preCurrent;				//THe node that is used for iteration

	public:
		LinkList();
		~LinkList();

		/**
		 * Insert element at desired position
		 */
		void Insert(uint pos, T element);

		/**
		 * Concact a link list to this link list
		 */
		void Concact(const LinkList* linkList);

		/**
		 * Concact another link list to this link list and clear that link list
		 */
		void ClearConcact(LinkList* linkList);

		/**
		 * Add element at the end;
		 */
		void Add(T element);

		/**
		 * Remove element at desired position
		 */
		void RemoveAt(int pos);

		/**
		 * Remove element, if the element doesn't exist, does nothing
		 */
		void Remove(T element);

		/**
		 * Remove the node where preCurrent points to
		 */
		void RemoveCurrent();

		/**
		 * Check if element exists
		 */
		bool Exists(T element);

		/**
		 * Test whether container is empty
		 */
		bool IsEmpty();

		void Clear();

	    const uint Size();

	    const T Get(uint pos);

	    Iterator<T>* CreateIterator();
	};

	template <typename T>
	LinkList<T>::LinkList() : _size(0)
	{
		_head = new Node();
		_tail = _head;
		_preCurrent = _head;
	}

	template <typename T>
	LinkList<T>::~LinkList()
	{
		Clear();

		if(_head != NULL)
		{
			delete _head;
		}
	}

	template <typename T>
	void LinkList<T>::Insert(uint pos, T element)
	{
		assert(pos < _size);

		uint i = 0;
		Node* temp = _head;

		while(i < pos)
		{
			temp = temp->next;
			i++;
		}

		Node* node = new Node(element);
		node->next = temp->next;
		temp->next = node;

		_size++;
	}

	template <typename T>
	void LinkList<T>::Concact(const LinkList* linkList)
	{
		if(linkList->_size == 0)
		{
			return;
		}

		_tail->next = linkList->_head->next;
		_tail = linkList->_tail;

		_size += linkList->_size;
	}

	template <typename T>
	void LinkList<T>::ClearConcact(LinkList* linkList)
	{
		Concact(linkList);

		linkList->_tail = NULL;
		linkList->_head->next = linkList->_tail;

		delete linkList;
	}

	template <typename T>
	void LinkList<T>::Add(T element)
	{
		Node* node = new Node(element);

		_tail->next = node;
		_tail = node;

		_size++;
	}

	template <typename T>
	void LinkList<T>::RemoveAt(int pos)
	{
		assert(pos < _size);

		Node* temp = _head;
		uint i = 0;

		while(i < pos)
		{
			temp = temp->next;
			i++;
		}

		Node* removeNode = temp->next;
		temp->next = removeNode->next;

		delete removeNode;
		_size--;

		if(_size == 0)
		{
			_tail = _head;
		}
	}

	template <typename T>
	void LinkList<T>::Remove(T element)
	{
		Node* temp = _head;

		while(temp->next)
		{
			if(temp->next->element == element)
			{
				Node* removeNode = temp->next;
				temp->next = removeNode->next;

				delete removeNode;
				_size--;

				return;
			}

			temp = temp->next;
		}
	}

	template <typename T>
	void LinkList<T>::RemoveCurrent()
	{
		assert(_preCurrent && _preCurrent->next);

		Node* temp = _preCurrent->next;
		_preCurrent->next = temp->next;

		delete temp;
		_size--;
	}

	template <typename T>
	bool LinkList<T>::Exists(T element)
	{
		if(_size == 0)
		{
			return false;
		}

		Node* temp = _head->next;

		while(temp)
		{
			if(temp->element == element)
			{
				return true;
			}

			temp = temp->next;
		}

		return false;
	}

	template <typename T>
	void LinkList<T>::Clear()
	{
		while(_head->next)
		{
			Node* temp = _head;
			_head = _head->next;

			delete temp;
		}

		delete _head;
		_head = new Node();

		_size = 0;
	}

	template <typename T>
	bool LinkList<T>::IsEmpty()
	{
		return _size == 0;
	}

	template <typename T>
	const uint LinkList<T>::Size()
	{
		return _size;
	}

	template <typename T>
	const T LinkList<T>::Get(uint pos)
	{
		assert(pos < _size);

		Node* temp = _head->next;
		uint i = 0;

		while(temp != NULL)
		{
			if(i == pos)
			{
				return temp->element;
			}

			temp = temp->next;
			i++;
		}

		assert(false);
	}
}

#endif




















