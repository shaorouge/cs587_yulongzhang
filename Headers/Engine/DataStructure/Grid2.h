
#ifndef _GRID2_H_
#define _GRID2_H_
#include "../Math/Vector2.h"
#include "../Iterator/Iterator.h"
#include <assert.h>

namespace Engine
{
	typedef unsigned int uint;

	template <typename T>
	class GridIterator;

	template <typename T>
	class Grid2
	{
	private:
		T* _array;
		T _defaultData;

		uint _numberOfBinX;
		uint _numberOfBinY;

		float _binSizeX;
		float _binSizeY;

		Vector2 _minCorner;

		friend class GridIterator<T>;

	public:
		Grid2();
		/**
		 * Initialize a Gird2 object by specifying the number of bins of each coordinates
		 * and using the origin as the minPoint and a specified vector as the maxCorner
		 * to specify the size of the Grid2
		 */
		Grid2(uint xBin, uint yBin, float binSizeX, float binSizeY, T defaultData);

		/**
		 * Initialize a Gird2 object by specifying the number of bins of each coordinates
		 * and using two specified vectors as the minPoint
		 * the maxCorner to specify the size of the Grid2
		 */
		Grid2(uint xBin, uint yBin, const Vector2& minCorner, float binSizeX, float binSizeY, T defaultData);

		~Grid2();

		/**
		 * Reset
		 */
		void Reset(uint xBin, uint yBin, float binSizeX, float binSizeY, T defaultData);

		/**
		 * Reset
		 */
		void Reset(uint xBin, uint yBin, const Vector2& minCorner, float binSizeX, float binSizeY, T defaultData);

		/**
		 * Clear the _array
		 */
		void Clear();

		/**
		 * Remove the data in a specified location through a Vector2 object
		 */
		void Remove(const Vector2& v);

		/**
		 * Remove the data in a specified location through coordinates
		 */
		void Remove(int x, int y);

		/**
		 * Store data in a specified location through a Vector2 object
		 */
		void SetData(Vector2& v, const T data);
		/**
		 * Store data in a specified location through coordinates
		 */
		void SetData(int x, int y, const T data);

		/**
		 * Store the data in a specified location through index
		 */
		void SetData(int index, const T data);

		/**
		 * Get the data in a specified location through a Vector2 object
		 */
		T GetData(Vector2& v);

		/**
		 * Get the data in a specified location through coordinates
		 */
		T GetData(int x, int y);

		/**
		 * Get the data in a specified location through index
		 */
		T GetData(int index);

		/**
		 * Get the index through a Vector2 object
		 */
		int GetIndex(const Vector2& v);

		/**
		 * Get the cell  through a Vector2 object
		 */
		Vector2 GetCell(Vector2 v);

		/**
		 * Get the cell through index
		 */
		Vector2 GetCell(uint index);

		/**
		 * Return the size of this Grid
		 */
		uint Size();

		Vector2 GetMaxCorner();

		Iterator<T>* CreateIterator();

		void Destory();

		inline int GetBinX()
		{
			return _numberOfBinX;
		}

		inline int GetBinY()
		{
			return _numberOfBinY;
		}

		inline Vector2 GetMinCorner()
		{
			return _minCorner;
		}

		inline void SetMinCorner(const Vector2& minCorner)
		{
			_minCorner = minCorner;
		}

		inline Vector2 GetBinSize()
		{
			return Vector2(_binSizeX, _binSizeY);
		}

		inline void SetBinSize(float binSizeX, float binSizeY)
		{
			assert(binSizeX > 0.0 && binSizeY > 0.0);

			_binSizeX = binSizeX;
			_binSizeY = binSizeY;
		}
	};

	template <typename T>
	Grid2<T>::Grid2() : _array(NULL),
						_numberOfBinX(0), _numberOfBinY(0),
						_binSizeX(0), _binSizeY(0),
						_minCorner(Vector2::ZERO)
	{

	}

	template <typename T>
	Grid2<T>::Grid2(uint xBin, uint yBin, float binSizeX, float binSizeY, T defaultData)
	{
		Reset(xBin, yBin, Vector2::ZERO, binSizeX, binSizeY, defaultData);
	}

	template <typename T>
	Grid2<T>::Grid2(uint xBin, uint yBin, const Vector2& minCorner, float binSizeX, float binSizeY, T defaultData)
	{
		Reset(xBin, yBin, minCorner, binSizeX, binSizeY, defaultData);
	}

	template <typename T>
	Grid2<T>::~Grid2()
	{
		Destory();
	}

	template <typename T>
	void Grid2<T>::Reset(uint xBin, uint yBin, float binSizeX, float binSizeY, T defaultData)
	{
		Reset(xBin, yBin, Vector2::ZERO, binSizeX, binSizeY, defaultData);
	}

	template <typename T>
	void Grid2<T>::Reset(uint xBin, uint yBin, const Vector2& minCorner, float binSizeX, float binSizeY, T defaultData)
	{
		assert(binSizeX > 0.0 && binSizeY > 0.0);

		_numberOfBinX = xBin;
		_numberOfBinY = yBin;
		_binSizeX = binSizeX;
		_binSizeY = binSizeY;
		_minCorner = minCorner;
		_defaultData = defaultData;

		uint arraySize = _numberOfBinX * _numberOfBinY;

		Destory();

		_array = new T[arraySize];

		for(uint i = 0; i < arraySize; i++)
		{
			_array[i] = _defaultData;
		}
	}

	template <typename T>
	void Grid2<T>::Clear()
	{
		uint arraySize = _numberOfBinX * _numberOfBinY;

		for(uint i = 0; i < arraySize; i++)
		{
			_array[i] = _defaultData;
		}
	}

	template <typename T>
	void Grid2<T>::SetData(Vector2& v, const T data)
	{
		_array[GetIndex(v)] = data;
	}

	template <typename T>
	void Grid2<T>::SetData(int x, int y, const T data)
	{
		_array[_numberOfBinX * y+ x] = data;
	}

	template <typename T>
	void Grid2<T>::SetData(int index, const T data)
	{
		_array[index] = data;
	}

	template <typename T>
	T Grid2<T>::GetData(Vector2& v)
	{
		return _array[GetIndex(v)];
	}

	template <typename T>
	T Grid2<T>::GetData(int x, int y)
	{
		return _array[_numberOfBinX * y + x];
	}

	template <typename T>
	T Grid2<T>::GetData(int index)
	{
		return _array[index];
	}

	template <typename T>
	int Grid2<T>::GetIndex(const Vector2& v)
	{
		assert(v.x >= _minCorner.x && v.y >= _minCorner.y);

		//Calculate the number of bins in each coordinates and cast into int
		int x = static_cast<int> ((v.x - _minCorner.x) / _binSizeX);
		int y = static_cast<int> ((v.y - _minCorner.y) / _binSizeY);

		return _numberOfBinX * y + x;
	}

	template <typename T>
	Vector2 Grid2<T>::GetCell(Vector2 v)
	{
		int x = static_cast<int> ((v.x - _minCorner.x) / _binSizeX);
		int y = static_cast<int> ((v.y - _minCorner.y) / _binSizeY);

		return Vector2(x, y);
	}

	template <typename T>
	Vector2 Grid2<T>::GetCell(uint index)
	{
		assert(index < Size());

		int y = index / _numberOfBinX;
		int x = index - y * _numberOfBinX;

		return Vector2(x, y);
	}

	template <typename  T>
	Vector2 Grid2<T>::GetMaxCorner()
	{
		float x = _minCorner.x + _numberOfBinX * _binSizeX;
		float y = _minCorner.y + _numberOfBinY * _binSizeY;

		return Vector2(x, y);
	}

	template <typename T>
	uint Grid2<T>::Size()
	{
		return _numberOfBinX * _numberOfBinY;
	}

	template <typename T>
	void Grid2<T>::Destory()
	{
		if(_array != NULL)
		{
			delete [] _array;
		}
	}
}

#endif























