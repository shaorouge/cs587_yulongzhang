
#ifndef _DYNAMICARRAY_H_
#define _DYNAMICARRAY_H_
#include <assert.h>
#include <string.h>
#include "../Iterator/Iterator.h"

namespace Engine
{
	typedef unsigned int uint;

	template <typename T>
	class ArrayIterator;

	template <typename T>
	class DynamicArray
	{
	private:
		uint _size;
		uint _capacity;
		T* _array;

		friend class ArrayIterator<T>;

		// --- Private Properties --- //

		void Reserve();

	public:

		// --- Constructors and Destructor --- //

		DynamicArray();
		DynamicArray(uint size, T object);
		~DynamicArray();

		// --- Capacities --- //
		bool IsEmpty();

		// --- Modifiers ---//

		void Add(T object);
		void InsertAt(uint ind, T object);
		bool Remove(T object);
		void RemoveAt(uint ind);
		void Clear();

		// --- Element access --- //

		T At(uint ind);
		bool Contains(T object);
		T& operator[](uint ind);

		// --- Instance duplication --- //

		DynamicArray<T>& operator=(DynamicArray<T>& oldArray);

		const uint Size();
		Iterator<T>* CreateIterator();
	};

	/**
	 * Expand or shink the array capacity
	 */
	template <typename T>
	void DynamicArray<T>::Reserve()
	{
		_capacity = _size * 2;

		T* newArray = new T[_capacity];

		for(uint i = 0; i < _size; i++)
		{
			newArray[i] = _array[i];
		}

		delete [] _array;
		_array = newArray;
	}

	/**
	 * Constructor with no input.
	 */
	template <typename T>
	DynamicArray<T>::DynamicArray()
	{
		_size = 0;
		_capacity = 10;
		_array = new T[_capacity];
	}

	/**
	 * Constructor with initial size and default object from the user
	 */
	template <typename T>
	DynamicArray<T>::DynamicArray(uint size, T object)
	{
		_capacity = size * 2;
		_size = size;
		_array = new T[_capacity];

		for(uint i = 0; i < _size; i++)
			_array[i] = object;
	}

	/**
	 * Destructor
	 */
	template <typename T>
	DynamicArray<T>::~DynamicArray()
	{
		delete [] _array;
	}

	/**
	 * Return true if the size of _array is 0
	 * else return false
	 *
	 * @return boolean depends on _size
	 */
	template <typename T>
	bool DynamicArray<T>::IsEmpty()
	{
		return _size == 0;
	}

	/**
	 * Insert a new object at the index _size and increase _size by 1
	 */
	template <typename T>
	void DynamicArray<T>::Add(T object)
	{
		if(_size + 1 == _capacity)
			Reserve();

		_array[_size] = object;
		_size++;
	}

	/**
	 * Insert a new object at index ind and increase _size by 1
	 */
	template <typename T>
	void DynamicArray<T>::InsertAt(uint ind, T object)
	{
		assert(ind < _size);

		if(_size + 1 == _capacity)
			Reserve();

		for(uint i = _size - 1; i >= ind; i--)
		{
			_array[i + 1] = _array[i];
		}

		_array[ind] = object;
		_size++;
	}

	/**
	 * Remove the object from user if the object can be found in _array
	 */
	template <typename T>
	bool DynamicArray<T>::Remove(T object)
	{
		for(uint i = 0; i < _size; i++)
		{
			if(_array[i] == object)
			{
				RemoveAt(i);
				return true;
			}
		}

		return false;
	}

	/**
	 * Remove the object at index ind
	 */
	template <typename T>
	void DynamicArray<T>::RemoveAt(uint ind)
	{
		assert(ind < _size);

		if(_size - 1 == _capacity / 4)
			Reserve();

		_size--;

		for(uint i = ind; i < _size; i++)
		{
			_array[i] = _array[i + 1];
		}
	}

	/**
	 * Remove all objects in _array
	 */
	template <typename T>
	void DynamicArray<T>::Clear()
	{
		delete [] _array;

		_size = 0;
		_capacity = 10;
		_array = new T[_capacity];
	}

	template <typename T>
	T DynamicArray<T>::At(uint ind)
	{
		return (*this)[ind];
	}

	/**
	 * Returns true if object is found in _array
	 */
	template <typename T>
	bool DynamicArray<T>::Contains(T object)
	{
		for(uint i = 0; i < _size; i++)
		{
			if(_array[i] == object)
				return true;
		}

		return false;
	}

	/**
	 * Overloaded operator []
	 */
	template <typename T>
	T& DynamicArray<T>::operator[](uint ind)
	{
		assert(ind < _size && ind >= 0);

		return _array[ind];
	}

	/**
	 * Overloaded operator =
	 */
	template <typename T>
	DynamicArray<T>& DynamicArray<T>::operator=(DynamicArray<T>& oldArray)
	{
		delete [] _array;

		_capacity = oldArray._capacity;
		_size = oldArray._size;
		_array = new T[_capacity];

		for(uint i = 0; i < _size; i++)
		{
			_array[i] = oldArray._array[i];
		}

		return *this;
	}

	/**
	 * Return the size of _array
	 */
	template <typename T>
	const uint DynamicArray<T>::Size()
	{
		return _size;
	}
}

#endif
