
#ifndef _ITICKABLE_H_
#define _ITICKABLE_H_

namespace Engine
{
	class ITickable
	{
	protected:
		float _tick;

	public:
		virtual ~ITickable(){};
		virtual void Tick(const float deltaTime) = 0;
	};
}

#endif
