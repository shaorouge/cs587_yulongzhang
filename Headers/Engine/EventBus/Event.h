
#ifndef _EVENT_H_
#define _EVENT_H_
using namespace std;

namespace Engine
{
	enum EventType
	{
		STATE_TRANSFER
	};

	const string EventTypeStr[] =
	{
		"state_transfer"
	};

	class Event
	{
	protected:
		string _type;
		string _name;

	public:
		const string GetName() const
		{
			return _name;
		}
		const string GetEventType() const
		{
			return _type;
		}
	};
}

#endif
