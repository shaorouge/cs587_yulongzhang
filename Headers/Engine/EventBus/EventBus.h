
#ifndef _EVENTBUS_H_
#define _EVENTBUS_H_

#include "../DataStructure/DynamicArray.h"
#include "../DataStructure/HashMap.h"
#include "../DataStructure/Queue.h"
#include "../Tick/ITickable.h"
#include "IEventBusListener.h"
#include "Event.h"

namespace Engine
{
	class EventBus
	{
		typedef DynamicArray<IEventBusListener*> IEventBusListenerList;
		typedef HashMap<IEventBusListenerList> IEventBusListenerMap;
		typedef Queue<Event*> EventQueue;
	private:
		IEventBusListenerMap _eventBusListenersMap;
		EventQueue _eventQueue;

	public:
		virtual ~EventBus(){};

		bool AddListenerList(string eventType);

		/**
		 * add  event listener to Listenerlist
		 */
		bool AddListener(IEventBusListener* listener, string eventType);

		/**
		 * add  a listener object to spcifiy eventType
		 */
		bool RemoveListener(IEventBusListener* listener, string eventType);

		/**
		 * remove  a spcified eventType from a listener object
		 */
		bool RemoveListenerList(string eventType);

		/**
		 * Trigger an event
		 */
		bool Dispatch(Event* event);

		/**
		 * add an event to the event queue
		 */
		void QueueEvent(Event* event);

		/**
		 * update all events in the eventquenue
		 */
		void Tick(float deltaTime);

		static EventBus* Instance();
	};
}

#endif
