
#ifndef _EVENTLISTENER_H_
#define _EVENTLISTENER_H_
#include "Event.h"

namespace Engine
{
	class IEventBusListener
	{
	public:
		virtual ~IEventBusListener(){};
		virtual void OnEvent(Event* event) = 0;
	};
}

#endif
