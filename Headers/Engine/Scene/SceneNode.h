
#ifndef _SCENENODE_H_
#define _SCENENODE_H_
#include "../Math/Vector2.h"

namespace Engine
{
	class QuadTree;

	struct AABB
	{
		Vector2 position;
		Vector2 bound;				//The size of the bounding box is

		AABB();
		AABB(const Vector2& pos, const Vector2& bound);

		bool Intersects(const AABB& other);
		bool Contains(const AABB& other);

		AABB& operator=(const AABB& other);
	};

	class SceneNode
	{
		friend class QuadTree;

	protected:
		AABB _aabb;

	public:
		SceneNode();
		SceneNode(const Vector2& pos, const Vector2& bound);
		~SceneNode();

		void Destory();

		inline AABB& GetAABB()
		{
			return _aabb;
		}

		inline void SetAABB(const Vector2& pos, const Vector2& bound)
		{
			_aabb.position = pos;
			_aabb.bound = bound;
		}
	};
}

#endif
