
#ifndef _SCENEMANAGER_H_
#define _SCENEMANAGER_H_
#include "../Actor/Actor.h"
#include "../Tick/ITickable.h"
#include "../Iterator/HashMapIterator.h"
#include "../Iterator/ArrayIterator.h"
#include "../Iterator/HeapIterator.h"
#include "../Camera/Camera.h"
#include "Layer.h"
#include <unistd.h>

namespace Engine
{
	class SceneManager : public ITickable
	{
	protected:
		DynamicArray<ITickable*> _tickables;
		Heap<Layer*> _layers;
		Camera* _camera;

	public:
		SceneManager();
		~SceneManager();
		void AddTickable(ITickable* tickable);
		void RemoveTickable(ITickable* tickable);
		void Tick(const float deltaTime);

		void AddLayer(Layer* layer);
		Iterator<Layer*>* GetLayerItr();

		inline Camera* GetCamera()
		{
			return _camera;
		}

		inline void SetCamera(Camera* camera)
		{
			_camera = camera;
		}
	};
}

#endif
