#ifndef _LAYER_H_
#define _LAYER_H_
#include "../Math/Vector2.h"
#include "../Iterator/ArrayIterator.h"
#include "../Render/IDrawable.h"
using namespace std;

namespace Engine
{
	class Layer
	{
	protected:
		int _order;
		Vector2 _position;
		string _id;
		DynamicArray<IDrawable*> _drawables;

	public:
		virtual ~Layer(){}
		virtual Iterator<IDrawable*>* CreateIterator() = 0;

		inline int GetOrder()
		{
			return _order;
		}

		inline void SetOrder(const int order)
		{
			_order = order;
		}

		inline string GetId()
		{
			return _id;
		}

		inline void SetId(const string& id)
		{
			_id = id;
		}

		inline Vector2 GetPosition()
		{
			return _position;
		}

		inline void SetPosition(const Vector2& position)
		{
			_position = position;
		}
	};
}

#endif
