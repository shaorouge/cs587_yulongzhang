
#ifndef _COLLLITIONMANAGER_H_
#define _COLLLITIONMANAGER_H_
#include "ICollidable.h"
#include "../Actor/Actor.h"
#include "../DataStructure/DynamicArray.h"

namespace Engine
{
	class CollisionManager
	{
	private:
		static CollisionManager* _instance;
		DynamicArray<ICollidable*> _collidables;

		CollisionManager();

	public:
		~CollisionManager();
		void CollisionDetection(Actor* otherActor);

		void AddCollidable(ICollidable* collidable);
		void RemoveCollidable(ICollidable* collidable);

		static CollisionManager* Instance();
	};
}

#endif
