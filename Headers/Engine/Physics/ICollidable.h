
#ifndef _ICOLLIDABLE_H_
#define _ICOLLIDABLE_H_

namespace Engine
{
	class Actor;

	class ICollidable
	{
	protected:
		Actor* _actor;

	public:
		virtual ~ICollidable(){}
		virtual void CollisionHandling() = 0;
		virtual void CollideWith(Actor* actor) = 0;
		virtual ICollidable* Clone() = 0;

		inline Engine::Actor* GetActor()
		{
			return _actor;
		}

		void SetActor(Engine::Actor* actor)
		{
			_actor = actor;
		}
	};
}

#endif
