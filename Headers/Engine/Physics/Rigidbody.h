
#ifndef _RIGIDBODY_H_
#define _RIGIDBODY_H_
#include <assert.h>
#include "../Scene/SceneNode.h"

namespace Engine
{
	/**
	 * A rigidbody is the simplest object that can be simulate in the physics system.
	 */
	class Rigidbody
	{
	private:
		float _damping;			//Holds the amounts of damping applied to linear motion
		float _inverseMass;		//Holds the inverse of the mass of the Rigidbody

		SceneNode* _sceneNode;

		Vector2 _velocity; 		//Holds the linear velocity
		Vector2 _acceleration; 	//Holds the acceleration of the Rigidbody
		Vector2 _forceAccum;	//Holds the accumulated force to be applied at the next simulation iteration only
								//This value is zeroed at each integration step

	public:
		Rigidbody();
		Rigidbody(SceneNode* sceneNode);
		Rigidbody(float damping, float inverseMass, SceneNode* sceneNode, const Vector2& velocity, const Vector2& acceleration);

		/**
		 * Integrates the rigidbody forward in time by the given amount
		 */
		void Integrate(float deltaTime);

		/**
		 * Clears the forces appled to this rigidbody
		 * Will be called automatically after each integration step
		 */
		void ClearAccumulator();

		/**
		 * Adds the given force to this rigidbody, to be applied at the next iteration only
		 */
		void AddForce(const Vector2& force);

		inline float GetDamping() const
		{
			return _damping;
		}

		inline void SetDamping(const float damping)
		{
			_damping = damping;
		}

		inline float GetInverseMass() const
		{
			return _inverseMass;
		}

		inline float GetMass() const
		{
			assert(_inverseMass == 0);

			return 1 / _inverseMass;
		}

		inline void SetInverseMass(const float inverseMass)
		{
			_inverseMass = inverseMass;
		}

		inline SceneNode* GetPosition() const
		{
			return _sceneNode;
		}

		inline void SetSceneNode(SceneNode* sceneNode)
		{
			_sceneNode = sceneNode;
		}

		inline const Vector2 GetVelocity() const
		{
			return _velocity;
		}

		inline void SetVelocity(const Vector2& velocity)
		{
			_velocity = velocity;
		}

		/**
		 * Change the velocity only in the horizontal direction
		 */
		inline void SetVelocityX(const float vx)
		{
			_velocity.x = vx;
		}

		/**
		 * Change the velocity only in the vertical direction
		 */
		inline void SetVelocityY(const float vy)
		{
			_velocity.y = vy;
		}

		inline const Vector2 GetAcceleration() const
		{
			return _acceleration;
		}

		inline void SetAcceleratioin(const Vector2& acceleration)
		{
			_acceleration = acceleration;
		}
	};
}

#endif




















