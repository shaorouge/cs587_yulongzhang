
#ifndef _FORCEREGISTRY_H_
#define _FORCEREGISTRY_H_
#include "../Tick/ITickable.h"
#include "../Iterator/ArrayIterator.h"
#include "IForceGenerator.h"

namespace Engine
{
	/**
	 * Holds all the force generators and the Rigidbodys they apply to
	 */
	class ForceRegistry : public ITickable
	{
	protected:
		/**
		 * Keeps track of one force generator and the Rigidbody it applies to
		 */
		struct ForceRegistration
		{
			Rigidbody* rigidbody;
			IForceGenerator* fg;
			ForceRegistration(Rigidbody* r, IForceGenerator* f) : rigidbody(r), fg(f){}
		};

		DynamicArray<ForceRegistration*> _registrations;

	public:
		~ForceRegistry();
		/**
		 * Registers the given force generator to apply to the given Rigidbody
		 */
		void Add(Rigidbody* rigidbody, IForceGenerator* fg);

		/**
		 * Removes the given registered pair from the registry
		 */
		void Remove(Rigidbody* rigidbody, IForceGenerator* fg);

		/**
		 * Cut the connections between all the force generators and the their Rigidbodys
		 */
		void Clear();

		/**
		 * Calls all the force generators to update the forces of their corresponding Rigidbodys
		 */
		void Tick(const float deltaTime);
	};
}

#endif
