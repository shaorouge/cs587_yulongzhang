
#ifndef _FORCEGENERATOR_H_
#define _FORCEGENERATOR_H_
#include "Rigidbody.h"

namespace Engine
{
	/**
	 * A force generator can be asked to add a force to one or more Rigidbodys
	 */
	class IForceGenerator
	{
	public:
		virtual ~IForceGenerator(){}

		/**
		 * Overload this in implementations of the interface to calculate and update
		 * the force applied to the given Rigidbody
		 */
		virtual void UpdateForce(Rigidbody* Rigidbody, const float deltaTime) = 0;

		void Destory()
		{
			delete this;
		}
	};
}

#endif
