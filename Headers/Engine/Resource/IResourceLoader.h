
#ifndef _IRESOURCELOADER_H_
#define _IRESOURCELOADER_H_
#include "IResource.h"

namespace Engine
{
	typedef enum
	{
		GRAPHICS,
		AUDIO

	}ResourceLoaderType;

	const string resourceLoaderStr[] =
	{
		"graphics",
		"audio"
	};

	class IResourceLoader
	{
	protected:
		ResourceLoaderType _type;

	public:
		virtual ~IResourceLoader(){}

		/*
		 * load resources from JSON file
		 */
		virtual IResource* LoadFromJSON(JSONValue* data) = 0;

		inline string GetLoaderTypeStr()
		{
			return resourceLoaderStr[_type];
		}

		inline ResourceLoaderType GetLoaderTypeEnum()
		{
			return _type;
		}

		inline void SetLoaderType(ResourceLoaderType type)
		{
			_type = type;
		}
	};
}

#endif
