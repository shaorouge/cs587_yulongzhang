#ifndef _IRESOURCE_H_
#define _IRESOURCE_H_
#include "../Json/JSONValue.h"
#include <iostream>
using namespace std;

namespace Engine
{
	typedef enum
	{
		RESOURCE_GRAPHICS,
		RESOURCE_AUDIO
	}ResourceType;

	const string resourceTypeStr[] =
	{
		"graphics",
		"audio"
	};

	class IResource
	{
	protected:
		ResourceType _type;
		string _id;
		string _filePath;

	public:
		virtual ~IResource(){};
		virtual void LoadFromJSON(JSONValue* data) = 0;
		virtual void Unload() = 0;

		inline ResourceType GetResourceType()
		{
			return _type;
		}

		inline string GetResourceTypeStr()
		{
			return resourceTypeStr[_type];
		}

		inline void SetResourceType(ResourceType type)
		{
			_type = type;
		}

		inline string GetId()
		{
			return _id;
		}

		inline void SetId(const string& id)
		{
			_id = id;
		}

		inline string GetFilePath()
		{
			return _filePath;
		}

		inline void SetFilePath(const string& filePath)
		{
			_filePath = filePath;
		}

		inline void Destory()
		{
			delete this;
		}
	};
}
#endif
