
#ifndef _RENDERRESOURCE_H_
#define _RENDERRESOURCE_H_
#include <SDL2/SDL.h>
#include <SDL/SDL_image.h>
#include "IResource.h"

namespace Engine
{
	class RenderResource : public IResource
	{
	private:
		int _sourceWidth;
		int _sourceHeight;
		SDL_Texture* _texture;

	public:
		RenderResource();
		RenderResource(int width, int height, SDL_Texture* texture);
		~RenderResource();
		void LoadFromJSON(JSONValue* data);
		void Unload();

		inline int GetSourceWidth()
		{
			return _sourceWidth;
		}

		inline int GetSourceHeight()
		{
			return _sourceHeight;
		}

		inline SDL_Texture* GetTexture()
		{
			return _texture;
		}

		inline void SetTexture(SDL_Texture* texture)
		{
			_texture = texture;
		}
	};
}

#endif
