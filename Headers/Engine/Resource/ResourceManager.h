
#ifndef _RESOURCEMANAGER_H_
#define _RESOURCEMANAGER_H_
#include <assert.h>
#include "../Iterator/HashMapIterator.h"
#include "IResource.h"
#include "IResourceLoader.h"

namespace Engine
{
	class ResourceManager
	{
	private:
		static ResourceManager* _instance;
		HashMap<IResourceLoader*> _loadersMap;  //a hash map that stores resource loaders
		HashMap<IResource*> _resourcesMap;      //a hash map that stores all the resources in the game

		ResourceManager();

	public:
		~ResourceManager();

		/**
		 * finds resource by its unique file name, returns NULL if nothing is found
		 */
		IResource* FindResource(const string& id);

		/**
		 * clear all resources
		 */
		void ClearResources();

		/**
		 * load resources from JSON file
		 */
		bool LoadFromJSON(JSONValue* data);

		/**
		 * put a new resource loader into the loaders' hash map
		 */
		void AddResourceLoader(IResourceLoader* resourceLoader);

		/**
		 * returns how many resources in the resource hash map
		 */
		uint GetResourceCount();

		static ResourceManager* Instance();

		void Destory();
	};
}

#endif
