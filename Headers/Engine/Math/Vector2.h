
#ifndef _VECTOR2_H_
#define _VECTOR2_H_
#include <math.h>
#include <iostream>

namespace Engine
{
	class Vector2
	{
	public:
		float x;
		float y;

		const static Vector2 ZERO;
		const static Vector2 UP;
		const static Vector2 DOWN;
		const static Vector2 LEFT;
		const static Vector2 RIGHT;

		Vector2() : x(0.0), y(0.0){}

		Vector2(float nx, float ny) : x(nx), y(ny){}

		~Vector2(){};

		inline float GetX() { return x; }
		inline float GetY() { return y; }

		inline void SetX(float nx) { x = nx; }
		inline void SetY(float ny) { y = ny; }

		inline float Length()
		{
			return sqrt(x * x + y * y);
		}

		inline void Normalize()
		{
			float len = Length();

			if(len > 0)
			{
				x /= len;
				y /= len;
			}
		}

		inline Vector2 Dot(const Vector2& v2)
		{
			return Vector2(x * v2.x, y * v2.y);
		}

		inline Vector2 operator+(const Vector2& v2) const
		{
			return Vector2(x + v2.x, y + v2.y);
		}

		inline friend void operator+=(Vector2& v1, const Vector2& v2)
		{
			v1.x += v2.x;
			v1.y += v2.y;
		}

		inline Vector2 operator*(float scalar) const
		{
			return Vector2(x * scalar, y * scalar);
		}

		inline void operator*=(float scalar)
		{
			x *= scalar;
			y *= scalar;
		}

		inline Vector2 operator-(const Vector2& v2) const
		{
			return Vector2(x - v2.x, y - v2.y);
		}

		inline friend void operator-=(Vector2& v1, const Vector2& v2)
		{
			v1.x -= v2.x;
			v1.y -= v2.y;
		}

		inline Vector2 operator/(float scalar) const
		{
			return Vector2(x / scalar, y / scalar);
		}

		inline void operator/=(float scalar)
		{
			x /= scalar;
			y /= scalar;
		}

		inline Vector2& operator=(const Vector2& v)
		{
			x = v.x;
			y = v.y;

			return *this;
		}
	};
}

#endif
