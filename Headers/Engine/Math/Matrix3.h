
#ifndef _MATRIX3_H_
#define _MATRIX3_H_
#define PI 3.14159267
#define DEGREE_TO_RADIANS(THETA) (PI * THETA / 180)
#include "Vector3.h"

namespace Engine
{
	class Matrix3
	{
	private:
		Vector3 _matrix[3];

	public:
		Matrix3();
		Matrix3(float value);
		Matrix3(const Vector3& a, const Vector3& b, const Vector3& c);
		Matrix3(const Matrix3& m);

		Vector3& operator[](int i);
		const Vector3& operator [] (int i) const;
		Matrix3 operator+(const Matrix3& m) const;
		Matrix3 operator-(const Matrix3& m) const;
		Matrix3 operator*(const float s) const;
		Matrix3 operator/(const float s) const;
		Matrix3 operator*(const Matrix3& m) const;
		Vector3 operator*(const Vector3& v) const;

		static Matrix3 RotateZ(const float theta)
		{
			float angle = DEGREE_TO_RADIANS(theta);

			Matrix3 m;

			m[0][0] = m[1][1] = cos(angle);
			m[1][0] = sin(angle);
			m[0][1] = -m[1][0];

			return m;
		}

		static Matrix3 Translate(const float x, const float y)
		{
			Matrix3 m;

			m[0][2] = x;
			m[1][2] = y;

			return m;
		}

		static Matrix3 Scale(const float x, const float y)
		{
			Matrix3 m;

			m[0][0] = x;
			m[1][1] = y;

			return m;
		}

		friend Matrix3 operator*(const float s, const Matrix3& m)
		{
			return m * s;
		}
	};
}

#endif
