
#ifndef _VECTOR3_H_
#define _VECTOR3_H_

#include<iostream>
#include<math.h>
#include <assert.h>

namespace Engine
{
	class Vector3
	{
	public:

		// -----------Public Properties ------//

		float x;   // The frist element of a Vector3D object
		float y;   // The second element of a Vector3D object
		float z;   // The third element of a Vector3D object

		// --- ----Predefined const vectors --- ---//

		static  const Vector3  ZERO;    //The ZERO    define as a Vector3D object with coordinate(0.0.0)
		static  const Vector3  UP;      //The UP      define as a Vector3D object with coordinate(1.0.0)
		static  const Vector3  DOWN;    //The DOWN    define as a Vector3D object with coordinate(-1.0.0)
		static  const Vector3  RIGHT;   //The RIGHT   define as a Vector3D object with coordinate(0.1.0)
		static  const Vector3  LEFT;    //The LEFT    define as a Vector3D object with coordinate(0.-1.0)
		static  const Vector3  FORWARD; //The FORWARD define as a Vector3D object with coordinate(0.0.1)
		static  const Vector3  BACK;    //The FORWARD define as a Vector3D object with coordinate(0.0.-1)


		// --- The constructors  and distructors ---//

		Vector3();                                     //The defult constructor ,initializes a new instance of a Vector3D with elements(0,0,0)
		Vector3(float value1,
				float value2,                    // The constructor ,initializes a new instance of a Vector3D with  element (value1,value2 , value3)
				float value3);
		~Vector3();                                    // The distructor

		// -----Public static Methods--------- //

		static Vector3 Cross(const Vector3 &,
							  const Vector3 &);       //Calculates the cross product of two Vector3D objects.//

		static float Dot(const Vector3 &,
						 const Vector3 &);             //Calculates the dot productof two Vector3D objects.//


		// ----------Public  Methods----------- //
		void  Normalize();                              //Normalizes the current Vector3D object.
		float Length()const;                            //Calculates   the current Vector3D object's length.
		float LengthSquared() const;                    //Calculates  the current  Vector3D object's LengthSquared.

		void Set(const float,                           // Sets the current  Vector3D object's value
				 const float,
				 const float);

		// ----------- Overload Operator ---------//

		Vector3 operator-() const;                     //Negates  the current Vector3D object

		Vector3 operator*(const float) const;

		Vector3 operator/(const float) const;         //Divides the current Vector3D object by the specified scalar

		Vector3& operator=(const Vector3&);        //Assgins all the values from target Vector3Dobject to the current Vector3D object.*/

		Vector3  operator+(const Vector3&) const;   //Adds two Vector3D objects

		Vector3  operator-(const Vector3&) const;   //Subtracts a Vector3D object from a Vector3D object

		bool operator==(const Vector3&) const;    //Compares two Vector3D objects for equality.

		float& operator[](int i);

		const float operator[](int i) const;
	};
}

#endif
