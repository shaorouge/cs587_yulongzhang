
#ifndef _CAMERA_H_
#define _CAMERA_H_
#include "../Math/Vector2.h"
#include "../Math/Matrix3.h"

namespace Engine
{
	class Camera
	{
	private:
		Vector2 _position;
		float _zoom;
		float _rotation;
		int _width;
		int _height;
		int _borderX;
		int _borderY;

		void Adjust(float& pos, int length, float newPos, int border);

	public:
		Camera(Vector2 position, int width, int height, int borderX, int borderY);
		~Camera();

		void SetCameraWindow(Vector2 position, int width, int height, int borderX, int borderY);
		void LookAt(Vector2 postion);
		void Move(Vector2 displacement);
		void Zoom(float zoom);
		void Rotate(float theta);
		Matrix3 GetViewMatrix();
		bool Cull(Vector2 position, int objWidth, int objHeight);

		inline Vector2 GetPosition()
		{
		    return _position;
		}
	};
}

#endif
