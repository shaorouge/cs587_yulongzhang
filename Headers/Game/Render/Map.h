
#ifndef _MAP_H_
#define _MAP_H_
#include "../../Engine/Render/IDrawable.h"
#include "../../Engine/DataStructure/Grid2.h"
#include "../../Engine/DataStructure/DynamicArray.h"

namespace Game
{
	class Map : public Engine::IDrawable
	{
	private:
		SDL_Rect _destRect;
		SDL_Rect _srcRect;

		Engine::Grid2<int>* _grid;
		Engine::DynamicArray<Engine::RenderResource*> _resources;

	public:
		Map(Engine::Grid2<int>* grid, int sourceWidth, int sourceHeight);
		~Map();

		void AddResource(Engine::RenderResource* resource);
		void RemoveResource(Engine::RenderResource* resource);
		void Draw(SDL_Renderer* renderer);
		void ModifyPosition(float x, float y);
		IDrawable* Clone();
	};
}

#endif
