
#ifndef _SPRITE_H_
#define _SPRITE_H_
#include "../../Engine/Render/IDrawable.h"
#include "../../Engine/Iterator/HashMapIterator.h"
#include "../Actor/Action.h"

namespace Game
{
	enum Flip
	{
		NONE,
		HORIZONTAL,
		VERTICAL
	};

	class Sprite : public Engine::IDrawable
	{
	private:
		struct Frame
		{
			uint row;		//Indicate which row in the sprite sheet is in use
			uint start;		//The start column of this sprite sheet
			uint end;		//The last column of this sprite sheet
			uint cur;		//The current column of this sprite sheet

			Frame(uint r, uint s, uint e) : row(r), start(s), end(e), cur(0){}

			Frame* Clone()
			{
				return new Frame(row, start, end);
			}

			void Destory()
			{
				delete this;
			}
		};

		Frame** _frameArray;				//Store all the frame according to the action they represent

		SDL_Rect _destRect;
		SDL_Rect _srcRect;
		Flip _flip;

	public:
		Sprite(Engine::IResource* renderResource, int width, int height);
		~Sprite();
		void Draw(SDL_Renderer* renderer);
		void ModifyPosition(float x, float y);

		void SetAction(const ActionEnum action, uint row, uint start, uint end);
		void SetFrame(const ActionEnum action, const Flip flip);
		void SetFrame(const ActionEnum action);

		void RemoveAction(const ActionEnum action);

		Sprite* Clone();

		inline void SetFlip(const Flip flip)
		{
			_flip = flip;
		}

		inline int GetWidth()
		{
			return _destRect.w;
		}

		inline void SetWidth(int width)
		{
			_destRect.w = width;
		}

		inline int GetHeight()
		{
			return _destRect.h;
		}

		inline void SetHeight(int height)
		{
			_destRect.h = height;
		}

		inline Engine::Vector2 GetSize()
		{
			return Engine::Vector2(_destRect.w, _destRect.h);
		}

		inline void MoveFrame(int row, int column)
		{
			_srcRect.x = column * _renderResource->GetSourceWidth();
			_srcRect.y = row * _renderResource->GetSourceHeight();
		}
	};
}

#endif
