
#ifndef LEVEL_H_
#define LEVEL_H_
#include "../../Engine/Render/RenderManager.h"
#include "../../Engine/Resource/ResourceManager.h"
#include "../../Engine/IO/InputManager.h"
#include "../../Engine/Json/FileReader.h"
#include "../../Engine/Json/JSONReader.h"
#include "../../Engine/Physics/CollisionManager.h"
#include "../../Engine/EventBus/EventBus.h"
#include "../../Engine/Tick/ITickable.h"
#include "../Factories/ActorFactory.h"
#include "../Scene/GameSceneManager.h"
#include "../Scene/TileLayer.h"
#include "../Scene/ObjectLayer.h"
#include "../Controller/PlayerController.h"

namespace Game
{
	class Level : Engine::ITickable
	{
	private:
		GameSceneManager* _sceneManager;
		Engine::InputManager* _inputManager;
		PlayerController* _playerCtrl;

		void InitResourceManager(Engine::JSONValue* resourceData);
		void InitActorFactory(Engine::JSONValue* actorData);
		void InitCamera();
		void InitTileLayer(Engine::JSONValue* tileLayerData);
		void InitObjectLayer(Engine::JSONValue* objectLayerData);
		void InitForceGenerator(Engine::JSONValue* forceData);
		void InitPlayer(Engine::JSONValue* playerData);

	public:
		Level();
		~Level();

		void InitLevel(GameSceneManager* sceneManager, Engine::InputManager* inputManager, Engine::JSONValue* levelData);
		void Tick(const float deltaTime);

		inline PlayerController* GetPlayerCtrl()
		{
			return _playerCtrl;
		}
	};
}

#endif
