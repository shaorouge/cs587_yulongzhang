
#ifndef _STATE_H_
#define _STATE_H_
#include "../../Engine/Tick/ITickable.h"
#include "../../Engine/Actor/Actor.h"

namespace Game
{
	class State : public Engine::ITickable
	{
	protected:
		string _name;

	public:
		virtual ~State(){}
		virtual void Enter() = 0;
		virtual void Exit() = 0;

		inline string GetStateName()
		{
			return _name;
		};
	};
}

#endif
