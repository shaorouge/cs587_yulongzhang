
#ifndef _ISTATETRANSITIONLISTENER_H_
#define _ISTATETRANSITIONLISTENER_H_
#include "State.h"

namespace Game
{
	class IStateTransitionListener
	{
	public:
		virtual ~IStateTransitionListener(){}
		virtual void OnStateTransition(State* fromState, string toState) = 0;
	};
}

#endif
