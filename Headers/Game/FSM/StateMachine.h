
#ifndef _STATEMACHINE_H_
#define _STATEMACHINE_H_
#include "../../Engine/Tick/ITickable.h"
#include "../../Engine/DataStructure/HashMap.h"
#include "../../Engine/DataStructure/DynamicArray.h"
#include "State.h"

namespace Game
{
	class StateMachine
	{
	protected:
		Engine::HashMap<State*> _stateMap;
		Engine::DynamicArray<State*> _currentStates;

	public:
		StateMachine();

		void SetState(State* state);
		void RemoveState(string stateName);
		void SwitchState(State* fromState, string toState);

	};
}

#endif
