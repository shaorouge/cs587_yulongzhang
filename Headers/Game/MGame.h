
#ifndef _GAME_H_
#define _GAME_H_
#include "../Engine/Render/RenderManager.h"
#include "Level/Level.h"

namespace Game
{
	class MGame
	{
	private:
		float _deltaTime;

		Level _level;
		GameSceneManager _sceneManager;
		Engine::RenderManager _renderManager;
		Engine::InputManager _inputManager;
		Engine::JSONReader _reader;

		Engine::JSONValue* ParseFile(const string& filePath);

	public:
		void Init();
		void Run();
		void Quit();
	};
}



#endif
