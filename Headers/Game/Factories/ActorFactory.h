
#ifndef _ACTORFACTORY_H_
#define _ACTORFACTORY_H_
#include "../../Engine/Iterator/HashMapIterator.h"
#include "../../Engine/Resource/ResourceManager.h"
#include "../../Engine/Json/JSONValue.h"
#include "../Actor/GameActor.h"
#include "../Render/Sprite.h"

namespace Game
{
	/**
	 * ActorFactory produces the basic Actor for PlayerController and AIController
	 */
	class ActorFactory
	{
	private:
		static ActorFactory* _instance;
		Engine::HashMap<GameActor*> _actorMap;
		Engine::HashMap<Sprite*> _spriteMap;

		ActorFactory();
		void RegisterSprite(Engine::JSONValue* data);
		void LoadSpriteData(Engine::JSONValue* data);
		void RegisterActor(Engine::JSONValue* data);

	public:
		~ActorFactory();

		void LoadFromJSON(Engine::JSONValue* data);
		GameActor* CreateActor(const string& prototypeId, const string& newId, float posX, float posY);
		void Clear();
		static ActorFactory* Instance();
		void Destory();
	};
}

#endif
