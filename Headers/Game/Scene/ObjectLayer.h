#ifndef _OBJECTLAYER_H_
#define _OBJECTLAYER_H_
#include "../../Engine/Scene/Layer.h"
#include "GameSceneManager.h"

namespace Game
{
	/**
	 * ObjectLayer is the layer that is used to store all the Player Actor or other Enemy Actors.
	 */
	class ObjectLayer : public Engine::Layer
	{
	private:
		GameSceneManager* _sceneManager;

	public:
		ObjectLayer();
		ObjectLayer(GameSceneManager* sceneManager);
		~ObjectLayer();

		void LoadFromJSON(Engine::JSONValue* data);
		Engine::Iterator<Engine::IDrawable*>* CreateIterator();
		void RemoveActor(string actorId);

		inline void SetSceneManager(GameSceneManager* sceneManager)
		{
			_sceneManager = sceneManager;
		}
	};
}

#endif
