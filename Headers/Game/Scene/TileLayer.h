#ifndef _TILELAYER_H_
#define _TILELAYER_H_
#include "../../Engine/DataStructure/Grid2.h"
#include "../../Engine/Scene/Layer.h"
#include "GameSceneManager.h"

namespace Game
{
	/**
	 * TileLayer is the layer that is used to store all the tilesets.
	 */
	class TileLayer : public Engine::Layer
	{
	private:
		GameSceneManager* _sceneManager;
		int _row;
		int _column;
		float _tileWidth;
		float _tileHeight;
		float _sourceWidth;
		float _sourceHeight;
		Engine::JSONValue* _data;

		void InitProperties();
		void InitGrid();
		void InitMap();
		void InitResourceArray();

	public:
		TileLayer();
		TileLayer(GameSceneManager* sceneManager);
		~TileLayer();

		void LoadFromJSON(Engine::JSONValue* data);
		Engine::Iterator<Engine::IDrawable*>* CreateIterator();

		inline void SetSceneManager(GameSceneManager* sceneManager)
		{
			_sceneManager = sceneManager;
		}
	};
}

#endif
