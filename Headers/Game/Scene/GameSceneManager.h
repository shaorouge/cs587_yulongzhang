
#ifndef _GAMESCENEMANAGER_H_
#define _GAMESCENEMANAGER_H_
#include "../../Engine/Scene/SceneManager.h"
#include "../../Engine/Physics/IForceGenerator.h"
#include "../../Engine/Physics/ForceRegistry.h"
#include "../../Engine/Iterator/GridIterator.h"
#include "../Actor/GameActor.h"

namespace Game
{
	class TileLayer;
	class ObjectLayer;

	class GameSceneManager : public Engine::SceneManager
	{
	private:
		Engine::HashMap<GameActor*> _actorMap;
		Engine::HashMap<Engine::IForceGenerator*> _forceGeneratorMap;
		Engine::ForceRegistry _forceRegistry;
		Engine::Grid2<int> _grid;
		Engine::SceneNode* _focusNode; 		//A scene node that the camera should be focus at

		friend class TileLayer;
		friend class ObjectLayer;

		/**
		 * After all the ticks, every SceneNode's position is changed,
		 * Adjust function can use the camera to rearrange all the drawables' positions on the screen
		 */
		void Adjust(Engine::Actor* actor);

	public:
		~GameSceneManager();

		/**
		 * Register a rigidbody and gravity generator to force registry
		 */
		void Register(Engine::Rigidbody* rigidbody, const string& generatorId);

		/**
		 * Remove the rigidbody from force registry
		 */
		void RemoveRegister(Engine::Rigidbody* rigidbody, const string& generatorId);

		/**
		 * Add new generator to hash map
		 */
		void AddGenerator(Engine::IForceGenerator* generator, const string& generatorId);

		void AddActor(GameActor* actor);

		void RemoveActor(GameActor* actor);

		GameActor* FindActor(const string& actorId);

		/**
		 * Clear all the actors, force generators, and so on
		 */
		void Clear();

		Engine::Iterator<GameActor*>* GetActorItr();

		void Tick(const float deltaTime);

		bool IsActorOnGround(GameActor* actor);

		inline Engine::Grid2<int>* GetGrid()
		{
			return &_grid;
		}

		inline Engine::SceneNode* GetFocusNode()
		{
			return _focusNode;
		}

		inline void SetFocusNode(Engine::SceneNode* focusNode)
		{
			_focusNode = focusNode;
		}
	};
}

#endif
