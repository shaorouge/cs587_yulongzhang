
#ifndef _ICOMMAND_H_
#define _ICOMMAND_H_
#include <iostream>
#include "../../Engine/IO/KeyEvent.h"
using namespace std;

namespace Game
{
	class ICommand
	{
	protected:
		Engine::KeyCode _key;
		string _commandName;

	public:
		virtual ~ICommand(){}
		virtual void Run() = 0;
		virtual void Stop() = 0;

		inline string GetCommandName() const
		{
			return _commandName;
		}

		inline void SetCommandName(const string& commandName)
		{
			_commandName = commandName;
		}

		inline const Engine::KeyCode GetKeyCode()
		{
			return _key;
		}

		inline void SetKeyCode(const Engine::KeyCode key)
		{
			_key = key;
		}

		inline void Destory()
		{
			delete this;
		}
	};
}

#endif
