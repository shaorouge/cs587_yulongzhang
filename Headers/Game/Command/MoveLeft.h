
#ifndef _MOVELEFT_H_
#define _MOVELEFT_H_
#include "ICommand.h"
#include "../../Engine/Math/Vector2.h"

namespace Game
{
	class MoveLeft : public ICommand
	{
	private:
		GameActor* _actor;
		int _velocity;

	public:
		MoveLeft(Engine::KeyCode key, GameActor* actor, int velocity) : _actor(actor), _velocity(velocity)
		{
			_key = key;
		}

		void Run()
		{
			_actor->GetRigidbody()->SetVelocityX(-_velocity);
			_actor->SetAction(Move);
		}

		void Stop()
		{
			_actor->GetRigidbody()->SetVelocityX(0);
			_actor->SetAction(Default);
		}
	};
}

#endif
