
#ifndef _JUMP_H_
#define _JUMP_H_
#include "ICommand.h"
#include "../../Engine/Math/Vector2.h"
#include "../Scene/GameSceneManager.h"
#include "../Actor/GameActor.h"

namespace Game
{
	class Hop : public ICommand
	{
	private:
		GameActor* _actor;
		int _velocity;
		GameSceneManager* _sceneManager;

	public:
		Hop(Engine::KeyCode key, GameActor* actor, int velocity, GameSceneManager* sceneManager) :
																						_actor(actor),
																						_velocity(velocity),
																						_sceneManager(sceneManager)
		{
			_key = key;
		}

		void Run()
		{
			if(_sceneManager->IsActorOnGround(_actor))
			{
				_actor->GetRigidbody()->SetVelocityY(-_velocity);
			}
		}

		void Stop()
		{

		}
	};
}

#endif
