
#ifndef _MOVERIGHT_H_
#define _MOVERIGHT_H_
#include "ICommand.h"
#include "../../Engine/Math/Vector2.h"
namespace Game
{
	class MoveRight : public ICommand
	{
	private:
		GameActor* _actor;
		int _velocity;

	public:
		MoveRight(Engine::KeyCode key, GameActor* actor, int velocity) : _actor(actor), _velocity(velocity)
		{
			_key = key;
		}

		void Run()
		{
			_actor->GetRigidbody()->SetVelocityX(_velocity);
			_actor->SetAction(Move);
		}

		void Stop()
		{
			_actor->GetRigidbody()->SetVelocityX(0);
			_actor->SetAction(Default);
		}
	};
}

#endif
