
#ifndef _EXIT_H_
#define _EXIT_H_
#include "ICommand.h"

namespace Game
{
	class Escape : public ICommand
	{
	private:

	public:
		Escape(Engine::KeyCode key)
		{
			_key = key;
		}

		void Run()
		{
			exit(0);
		}

		void Stop()
		{

		}
	};
}

#endif
