
#ifndef _DRAGFORCEGENERATOR_H_
#define _DRAGFORCEGENERATOR_H_
#include "IForceGenerator.h"

namespace Engine
{
	/**
	 * A force generator that applies a drag force
	 * One instance can be used for multiple Rigidbodys
	 * Formula of drag force:
	 * 		f = -p * (k1 * |v| + k2 * |v| ^ 2)
	 * 		p is the normalized velocity of the Rigidbody
	 */
	class DragForceGenerator : public ForceGenerator
	{
	private:
		float _k1; 			//Holds the velocity drag coefficient
		float _k2;			//Holds the velocity squared drag coefficient

	public:
		/**
		 * Creates the generator with the given coefficients
		 */
		DragForceGenerator(float k1, float k2);

		/**
		 * Applies the drag force to the given Rigidbody
		 */
		void UpdateForce(Rigidbody* rigidbody, float deltaTime);
	};

	DragForceGenerator::DragForceGenerator(float k1, float k2) : _k1(k1), _k2(k2)
	{

	}

	void DragForceGenerator::UpdateForce(Rigidbody* rigidbody, float deltaTime)
	{
		Vector2 velocity = rigidbody->GetVelocity();

		float magnitude = velocity.Length();
		velocity.Normalize();

		Vector2 dragForce = -(_k1 * magnitude + _k2 * magnitude * magnitude) * velocity;

		rigidbody->AddForce(dragForce);
	}
}

#endif
