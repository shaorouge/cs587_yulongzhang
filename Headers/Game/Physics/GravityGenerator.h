
#ifndef _GRAVITYGENERATOR_H_
#define _GRAVITYGENERATOR_H_
#include "../../Engine/Physics/IForceGenerator.h"

namespace Engine
{
	/**
	 * A force generator that applies a gravitational force
	 * One instance can be used for multiple Rigidbodys
	 */
	class GravityGenerator : public IForceGenerator
	{
	private:
		Vector2 _gravity; 		//Holds the acceleratioin due to gravity

	public:
		/**
		 * Creates the generator with the given acceleration
		 */
		GravityGenerator(const Vector2& gravity);

		/**
		 * Applies the gravitational force to the given Rigidbody
		 */
		void UpdateForce(Rigidbody* Rigidbody, float deltaTime);
	};
}

#endif
