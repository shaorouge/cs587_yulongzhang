
#ifndef _TILECOLLIDER_H_
#define _TILECOLLIDER_H_
#include "../../Engine/Physics/ICollidable.h"
#include "../../Engine/DataStructure/Grid2.h"

namespace Game
{
	class TileCollider : public Engine::ICollidable
	{
	private:
		Engine::Grid2<int>* _grid;

		/**
		 * According to the collider the actor interact, change its velocity and position
		 */
		void Modify(Engine::Actor* otherActor, const Engine::Vector2& tilePos);

	public:
		TileCollider(Engine::Grid2<int>* grid);
		~TileCollider();

		void CollisionHandling();
		void CollideWith(Engine::Actor* otherActor);
		Engine::ICollidable* Clone();
	};
}

#endif
