
#ifndef _OBJECTCOLLIDER_H_
#define _OBJECTCOLLIDER_H_
#include "../../Engine/Physics/ICollidable.h"

namespace Game
{
	class ObjectCollider : public Engine::ICollidable
	{
	public:
		ObjectCollider();
		~ObjectCollider();

		void CollisionHandling();
		void CollideWith(Engine::Actor* actor);
		Engine::ICollidable* Clone();
	};
}


#endif
