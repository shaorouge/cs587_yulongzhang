
#ifndef _PLAYERCONTROLLER_H_
#define _PLAYERCONTROLLER_H_
#include "../../Engine/IO/IInputListener.h"
#include "../../Engine/Iterator/ArrayIterator.h"
#include "../../Engine/Tick/ITickable.h"
#include "../Actor/GameActor.h"
#include "../Command/ICommand.h"
#include "../Render/Sprite.h"

namespace Game
{
	class PlayerController : public Engine::IInputListener, public Engine::ITickable
	{
	private:
		GameActor* _actor;
		Engine::DynamicArray<ICommand*> _keyCommandArray;
		Sprite* _sprite;

		void ModifySprite();

	public:
		 PlayerController(GameActor* actor);
		 ~PlayerController();

		 /**
		  * Add a Command class to player controller
		  */
		 void SetKeyCommand(ICommand* command);

		 /**
		  * When a key is pressed, execute the Run() method in the resposible Command class
		  */
		 bool KeyPressed(const Engine::KeyEvent& keyEvent);

		 /**
		  * When a key is released, execute the Stop() method in the resposible Command class
		  */
		 bool KeyReleased(const Engine::KeyEvent& keyEvent);

		 void Tick(const float deltaTime);

		 inline GameActor* GetGameActor() const
		 {
			 return _actor;
		 }

		 inline void SetGameActor(GameActor* const actor)
		 {
			 _actor = actor;
		 }
	};
}

#endif
