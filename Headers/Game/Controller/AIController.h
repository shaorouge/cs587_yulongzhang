
#ifndef _AICONTROLLER_H_
#define _AICONTROLLER_H_
#include "../../Engine/Tick/ITickable.h"
#include "../../Engine/Actor/Actor.h"
#include "../FSM/StateMachine.h"
#include "../FSM/IStateTransitionListener.h"

namespace Game
{
	class AIController : public Engine::ITickable, public StateMachine, public IStateTransitionListener
	{
	private:
		 Engine::Actor* _actor;

	public:
		AIController(Engine::Actor* actor, State* initialState);

		void Tick(float deltaTime);
		void OnStateTransition(State* fromState, string toState);
	};
}

#endif
