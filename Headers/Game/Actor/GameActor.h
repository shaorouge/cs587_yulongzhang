
#ifndef _GAMEACTOR_H_
#define _GAMEACTOR_H_
#include "../../Engine/Actor/Actor.h"
#include "../Render/Sprite.h"
#include "Action.h"

namespace Game
{
	class GameActor : public Engine::Actor
	{
	private:
		ActionEnum _action;

	public:
		GameActor(const string& id, Engine::SceneNode* sceneNode,
				  Engine::IDrawable* drawable,
				  Engine::Rigidbody* rigidbody,
				  Engine::ICollidable* collidable,
				  int layer);

		~GameActor();

		GameActor* Clone(const string& newId);

		void Destory();

		inline ActionEnum GetAction()
		{
			return _action;
		}

		inline void SetAction(ActionEnum action)
		{
			_action = action;
		}
	};
}

#endif
