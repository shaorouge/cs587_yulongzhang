
#ifndef _ACTION_H_
#define _ACTION_H_

namespace Game
{
	enum ActionEnum
	{
		Default,
		Attack,
		Move,
		Jump
	};
}


#endif
