
#include "../../../Headers/Game/Render/Map.h"
using namespace Engine;
using namespace Game;

Map::Map(Grid2<int>* grid, int sourceWidth, int sourceHeight) : _grid(grid)
{
	_srcRect.x = 0;
	_srcRect.y = 0;
	_srcRect.w = sourceWidth;
	_srcRect.h = sourceHeight;

	_destRect.x = 0;
	_destRect.y = 0;
	_destRect.w = _grid->GetBinSize().x;
	_destRect.h = _grid->GetBinSize().y;
}

Map::~Map()
{

}

void Map::AddResource(RenderResource* resource)
{
	_resources.Add(resource);
}

void Map::RemoveResource(RenderResource* resource)
{
	for(uint i = 0; i < _resources.Size(); i++)
	{
		if(resource == _resources[i])
		{
			_resources[i] = NULL;

			return;
		}
	}
}

void Map::Draw(SDL_Renderer* renderer)
{
	for(uint i = 0; i < _grid->Size(); i++)
	{
		int index = _grid->GetData(i) - 1;

		if(index >= 0 && _resources[index] != NULL)
		{
			//Retrieve the position that the drawable should be shown in the screen
			Vector2 pos = _grid->GetMinCorner() + _grid->GetCell(i).Dot(_grid->GetBinSize());
			_destRect.x = pos.x;
			_destRect.y = pos.y;

			SDL_RenderCopyEx(renderer, _resources[index]->GetTexture(), &_srcRect, &_destRect, 0, 0, SDL_FLIP_NONE);
		}
	}
}

void Map::ModifyPosition(float x, float y)
{
	Vector2 v = Vector2(x, y);

	_grid->SetMinCorner(v);
}

IDrawable* Map::Clone()
{
	return NULL;
}
