
#include "../../../Headers/Game/Render/Sprite.h"
using namespace Engine;
using namespace Game;

Sprite::Sprite(Engine::IResource* renderResource, int width, int height) : _flip(NONE)
{
	_renderResource = dynamic_cast<RenderResource*>(renderResource);

	_srcRect.x = 0;
	_srcRect.y = 0;
	_srcRect.w = _renderResource->GetSourceWidth();
	_srcRect.h = _renderResource->GetSourceHeight();

	_destRect.x = 0;
	_destRect.y = 0;
	_destRect.w = width;
	_destRect.h = height;

	_frameArray = new Frame*[Jump + 1];
}

Sprite::~Sprite()
{
	for(uint i = Default; i <= Jump; i++)
	{
		_frameArray[i]->Destory();
	}

	delete _frameArray;
}

void Sprite::Draw(SDL_Renderer* renderer)
{
	SDL_RendererFlip SDLFlip;

	switch(_flip)
	{
		case HORIZONTAL: SDLFlip = SDL_FLIP_HORIZONTAL; break;
		case VERTICAL: SDLFlip = SDL_FLIP_VERTICAL; break;
		default: SDLFlip = SDL_FLIP_NONE; break;
	}

	SDL_RenderCopyEx(renderer, _renderResource->GetTexture(), &_srcRect, &_destRect, 0, 0, SDLFlip);
}

void Sprite::ModifyPosition(float x, float y)
{
	_destRect.x = x;
	_destRect.y = y;
}

void Sprite::SetAction(const ActionEnum action, uint row, uint start, uint end)
{
	_frameArray[action] =  new Frame(row, start, end);
}

void Sprite::SetFrame(const ActionEnum action, const Flip flip)
{
	_flip = flip;

	SetFrame(action);
}

void Sprite::SetFrame(const ActionEnum action)
{
	Frame* frame = _frameArray[action];

	assert(frame != NULL);

	if(frame->cur >= frame->end)
	{
		frame->cur = frame->start;
	}
	else
	{
		frame->cur++;
	}

	MoveFrame(frame->row, frame->cur);
}

void Sprite::RemoveAction(const ActionEnum action)
{
	_frameArray[action]->Destory();
	_frameArray = NULL;
}

Sprite* Sprite::Clone()
{
	Sprite* newSprite = new Sprite(_renderResource, _destRect.w, _destRect.h);

	for(uint i = Default; i <= Jump; i++)
	{
		if(_frameArray[i] != NULL)
		{
			newSprite->_frameArray[i] = _frameArray[i]->Clone();
		}
	}


	return newSprite;
}























