
#include "../../../Headers/Game/Level/Level.h"
#include "../../../Headers/Game/Command/MoveLeft.h"
#include "../../../Headers/Game/Command/MoveRight.h"
#include "../../../Headers/Game/Command/Hop.h"
#include "../../../Headers/Game/Command/Escape.h"
#include "../../../Headers/Game/Physics/GravityGenerator.h"
using namespace Engine;
using namespace Game;

Level::Level() : _sceneManager(NULL), _inputManager(NULL), _playerCtrl(NULL)
{

}

Level::~Level()
{

}

void Level::InitResourceManager(JSONValue* resourceData)
{
	ResourceManager::Instance()->LoadFromJSON(resourceData);
}

void Level::InitActorFactory(JSONValue* actorData)
{
	ActorFactory::Instance()->LoadFromJSON(actorData);
}

void Level::InitCamera()
{
	Camera* camera = new Camera(Vector2(0.0f, 0.0f), 800, 600, 5400, 1800);
	_sceneManager->SetCamera(camera);
}

void Level::InitTileLayer(Engine::JSONValue* tileLayerData)
{
	//Initialize tile layer
	TileLayer* tileLayer = new TileLayer(_sceneManager);
	tileLayer->LoadFromJSON(tileLayerData);
	_sceneManager->AddLayer(tileLayer);
}

void Level::InitObjectLayer(Engine::JSONValue* objectLayerData)
{
	//Initialize object layer
	ObjectLayer* objectLayer = new ObjectLayer(_sceneManager);
	objectLayer->LoadFromJSON(objectLayerData);
	_sceneManager->AddLayer(objectLayer);
}

void Level::InitForceGenerator(Engine::JSONValue* forceData)
{
	string id = (*forceData)["id"]->AsString();
	float x = (*forceData)["x"]->AsNumber();
	float y = (*forceData)["y"]->AsNumber();

	_sceneManager->AddGenerator(new GravityGenerator(Vector2(x, y)), id);
}

void Level::InitPlayer(Engine::JSONValue* playerData)
{
	string forceId = (*playerData)["force_id"]->AsString();
	GameActor* playerActor = dynamic_cast<GameActor*>(_sceneManager->FindActor((*playerData)["actor_id"]->AsString()));
	_sceneManager->Register(playerActor->GetRigidbody(), forceId);
	_playerCtrl = new PlayerController(playerActor);

	int velocity = (*playerData)["velocity"]->AsNumber();
	int hopVelocity = (*playerData)["hop_velocity"]->AsNumber();
	string moveLeft = (*playerData)["move_left"]->AsString();
	string moveRight = (*playerData)["move_right"]->AsString();
	string hop = (*playerData)["hop"]->AsString();
	string escape = (*playerData)["escape"]->AsString();

	_playerCtrl->SetKeyCommand(new MoveLeft(_inputManager->GetKeyboard()->GetKeyCode(moveLeft), _playerCtrl->GetGameActor(), velocity));
	_playerCtrl->SetKeyCommand(new MoveRight(_inputManager->GetKeyboard()->GetKeyCode(moveRight), _playerCtrl->GetGameActor(), velocity));
	_playerCtrl->SetKeyCommand(new Hop(_inputManager->GetKeyboard()->GetKeyCode(hop), _playerCtrl->GetGameActor(), hopVelocity, _sceneManager));
	_playerCtrl->SetKeyCommand(new Escape(_inputManager->GetKeyboard()->GetKeyCode(escape)));

	_inputManager->AddListener(_playerCtrl);
	_sceneManager->AddTickable(_inputManager);
	_sceneManager->AddTickable(_playerCtrl);
}

void Level::InitLevel(GameSceneManager* sceneManager, InputManager* inputManager, Engine::JSONValue* levelData)
{
	assert(inputManager != NULL);
	assert(sceneManager != NULL);
	assert(levelData != NULL);

	//Clear before put anything in
	ResourceManager::Instance()->ClearResources();
	ActorFactory::Instance()->Clear();

	_inputManager = inputManager;
	_sceneManager = sceneManager;

	InitResourceManager((*levelData)["resources"]);
	InitActorFactory((*levelData)["objects"]);
	InitCamera();
	InitTileLayer((*levelData)["tile_layer"]);
	InitObjectLayer((*levelData)["object_layer"]);
	InitForceGenerator((*levelData)["force"]);
	InitPlayer((*levelData)["player"]);

	_sceneManager->AddTickable(this);

	levelData->Destory();
}

void Level::Tick(const float deltaTime)
{

}
