
#include "../../../Headers/Game/Actor/GameActor.h"
using namespace Engine;
using namespace Game;

GameActor::GameActor(const string& id, Engine::SceneNode* sceneNode,
				  	 IDrawable* drawable,
				  	 Engine::Rigidbody* rigidbody,
				  	 Engine::ICollidable* collidable,
				  	 int layer) : Actor(id, sceneNode, drawable, rigidbody, collidable, layer),
				  			      _action(Default)
{

}

GameActor::~GameActor()
{

}

GameActor* GameActor::Clone(const string& newId)
{
	SceneNode* sceneNode = new SceneNode(Vector2::ZERO, _sceneNode->GetAABB().bound);
	Rigidbody* rigidbody = new Rigidbody(sceneNode);

	return new GameActor(newId, sceneNode, _drawable->Clone(), rigidbody, _collidable->Clone(), _layer);
}


void GameActor::Destory()
{
	delete this;
}
