
#include "../../../Headers/Game/Controller/AIController.h"
using namespace Engine;
using namespace Game;

AIController::AIController(Actor* actor, State* initialState) : _actor(actor)
{
    SetState(initialState);
    _currentStates.Add(initialState);
}

void AIController::Tick(const float deltaTime)
{
    for(uint i = 0; i < _currentStates.Size(); i++)
    {
        _currentStates[i]->Tick(deltaTime);
    }
}

void AIController::OnStateTransition(State* fromState, string toState)
{
    SwitchState(fromState, toState);
}
