
#include "../../../Headers/Game/Controller/PlayerController.h"
#include "../../../Headers/Game/Render/Sprite.h"
using namespace Engine;
using namespace Game;

void PlayerController::ModifySprite()
{
	float vx = _actor->GetRigidbody()->GetVelocity().x;
	float vy = _actor->GetRigidbody()->GetVelocity().y;

	if(vx > 0)
	{
		_sprite->SetFlip(HORIZONTAL);
	}
	else if(vx < 0)
	{
		_sprite->SetFlip(NONE);
	}

	switch(_actor->GetAction())
	{
	case Attack:
		_sprite->SetFrame(Attack);
		break;

	case Jump:
	case Move:
	case Default:
		if(vy == 0)
		{
			_sprite->SetFrame(_actor->GetAction());
		}
		else
		{
			_sprite->SetFrame(Jump);
		}
		break;
	}
}

PlayerController::PlayerController(GameActor* actor) : _actor(actor), _keyCommandArray(256, NULL)
{
	_sprite = dynamic_cast<Sprite*>(_actor->GetDrawable());
}

PlayerController::~PlayerController()
{
	Iterator<ICommand*>* itr = _keyCommandArray.CreateIterator();

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		itr->Current()->Destory();
	}

	itr->Destory();
}

void PlayerController::SetKeyCommand(ICommand* command)
{
	if(command != NULL)
	{
		_keyCommandArray[command->GetKeyCode()] = command;
	}
}

bool PlayerController::KeyPressed(const KeyEvent& keyEvent)
{
	if(_keyCommandArray[keyEvent.GetKeyCode()] != NULL)
	{
		_keyCommandArray[keyEvent.GetKeyCode()]->Run();

		return true;
	}

	return false;
}

bool PlayerController::KeyReleased(const KeyEvent& keyEvent)
{
	if(_keyCommandArray[keyEvent.GetKeyCode()] != NULL)
	{
		_keyCommandArray[keyEvent.GetKeyCode()]->Stop();

		return true;
	}

	return false;
}

void PlayerController::Tick(const float deltaTime)
{
	_actor->GetRigidbody()->Integrate(deltaTime);
	_actor->GetColliable()->CollisionHandling();

	ModifySprite();
}











