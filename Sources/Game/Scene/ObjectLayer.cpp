
#include "../../../Headers/Game/Factories/ActorFactory.h"
#include "../../../Headers/Game/Scene/ObjectLayer.h"
#include "../../../Headers/Engine/Physics/CollisionManager.h"
using namespace Engine;
using namespace Game;

ObjectLayer::ObjectLayer() : _sceneManager(NULL)
{

}

ObjectLayer::ObjectLayer(GameSceneManager* sceneManager) : _sceneManager(sceneManager)
{

}

ObjectLayer::~ObjectLayer()
{

}

void ObjectLayer::LoadFromJSON(JSONValue* data)
{

	_id = (*data)["id"]->AsString();
	_order = (*data)["order"]->AsNumber();
	_position = Vector2((*data)["pos_x"]->AsNumber(), (*data)["pos_y"]->AsNumber());

	Iterator<JSONValue*>* itr = (*data)["array"]->ArrayIterator();

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		string prototypeId = (*itr->Current())["prototype_id"]->AsString();
		string actorId = (*itr->Current())["actor_id"]->AsString();
		float posX = (*itr->Current())["pos_x"]->AsNumber();
		float posY = (*itr->Current())["pos_y"]->AsNumber();

		GameActor* newActor = ActorFactory::Instance()->CreateActor(prototypeId, actorId, posX, posY);
		_sceneManager->AddActor(newActor);
		_drawables.Add(newActor->GetDrawable());
		CollisionManager::Instance()->AddCollidable(newActor->GetColliable());
	}

	itr->Destory();

	_sceneManager->SetFocusNode(_sceneManager->FindActor("player")->GetSceneNode());
}

Iterator<IDrawable*>* ObjectLayer::CreateIterator()
{
	return _drawables.CreateIterator();
}

void ObjectLayer::RemoveActor(string actorId)
{
	if(_sceneManager->_actorMap.Exists(actorId))
	{
		_sceneManager->_actorMap.Remove(actorId);
	}
}
