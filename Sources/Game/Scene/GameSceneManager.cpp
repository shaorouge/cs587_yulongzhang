
#include "../../../Headers/Game/Scene/GameSceneManager.h"
using namespace Engine;
using namespace Game;

GameSceneManager::~GameSceneManager()
{
	Iterator<IForceGenerator*>* forceItr = _forceGeneratorMap.CreateIterator();

	for(forceItr->First(); !forceItr->IsDone(); forceItr->Next())
	{
		delete forceItr->Current();
	}

	forceItr->Destory();

	Iterator<GameActor*>* actorItr = _actorMap.CreateIterator();

	for(actorItr->First(); !actorItr->IsDone(); actorItr->Next())
	{
		delete actorItr->Current();
	}

	actorItr->Destory();
}

void GameSceneManager::Adjust(Actor* actor)
{
	_camera->LookAt(_focusNode->GetAABB().position);

	Vector3 viewPos = _camera->GetViewMatrix() * Vector3(actor->GetSceneNode()->GetAABB().position.x,
														 actor->GetSceneNode()->GetAABB().position.y,
														 1.0);

	actor->GetDrawable()->ModifyPosition(viewPos.x, viewPos.y);
}

void GameSceneManager::Register(Rigidbody* rigidbody, const string& generatorId)
{
	if(_forceGeneratorMap.Exists(generatorId))
	{
		_forceRegistry.Add(rigidbody, _forceGeneratorMap.Get(generatorId));
	}
}

void GameSceneManager::RemoveRegister(Rigidbody* rigidbody, const string& generatorId)
{
	if(_forceGeneratorMap.Exists(generatorId))
	{
		_forceRegistry.Remove(rigidbody, _forceGeneratorMap.Get(generatorId));
	}
}

void GameSceneManager::AddGenerator(IForceGenerator* generator, const string& generatorId)
{
	_forceGeneratorMap.Set(generatorId, generator);
}

void GameSceneManager::Tick(const float deltaTime)
{
	usleep(deltaTime * 1000000);

	_grid.SetMinCorner(Vector2::ZERO);
	_forceRegistry.Tick(deltaTime);

	for(uint i = 0; i < _tickables.Size(); i++)
	{
		_tickables[i]->Tick(deltaTime);
	}

	Iterator<GameActor*>* mapItr = _actorMap.CreateIterator();

	for(mapItr->First(); !mapItr->IsDone(); mapItr->Next())
	{
		Adjust(mapItr->Current());
	}

	delete mapItr;
}

void GameSceneManager::AddActor(GameActor* actor)
{
    _actorMap.Set(actor->GetId(), actor);
}

void GameSceneManager::RemoveActor(GameActor* actor)
{
	_actorMap.Remove(actor->GetId());
	actor->Destory();
}

GameActor* GameSceneManager::FindActor(const string& actorId)
{
    return _actorMap.Get(actorId);
}

void GameSceneManager::Clear()
{
	_forceRegistry.Clear();

	Iterator<GameActor*>* actorItr = _actorMap.CreateIterator();

	for(actorItr->First(); !actorItr->IsDone(); actorItr->Next())
	{
		actorItr->Current()->Destory();
	}

	Iterator<IForceGenerator*>* forceItr = _forceGeneratorMap.CreateIterator();

	for(forceItr->First(); !forceItr->IsDone(); forceItr->Next())
	{
		forceItr->Current()->Destory();
	}

	_focusNode->Destory();

	actorItr->Destory();
	forceItr->Destory();
}

Iterator<GameActor*>* GameSceneManager::GetActorItr()
{
	return _actorMap.CreateIterator();
}

bool GameSceneManager::IsActorOnGround(GameActor* actor)
{
	Vector2 actorPos = actor->GetSceneNode()->GetAABB().position;
	Vector2 actorBound = actor->GetSceneNode()->GetAABB().bound;
	Vector2 tileBound = _grid.GetBinSize();

	uint topLeftIndex = _grid.GetIndex(actorPos);
	uint bottomRightIndex = _grid.GetIndex(actorPos + actorBound);
	uint columns = (bottomRightIndex - topLeftIndex) % _grid.GetBinX() + 1;
	uint bottomLeftIndex = bottomRightIndex - columns + 1;

	//If the actor is on the ground
	uint i = bottomLeftIndex;
	for(; i < bottomRightIndex; i++)
	{
		if(_grid.GetData(i) > 0)
		{
			return true;
		}
	}

	if(_grid.GetData(bottomRightIndex) > 0 &&
	   (_grid.GetCell(bottomRightIndex).Dot(_grid.GetBinSize())).x < actorPos.x + actorBound.x)
	{
		return true;
	}

	return false;
}

