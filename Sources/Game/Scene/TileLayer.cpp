
#include "../../../Headers/Game/Scene/TileLayer.h"
#include "../../../Headers/Game/Render/Map.h"
#include "../../../Headers/Game/Physics/TileCollider.h"
#include "../../../Headers/Engine/Resource/ResourceManager.h"
#include "../../../Headers/Engine/Physics/CollisionManager.h"
using namespace Engine;
using namespace Game;

TileLayer::TileLayer() : _sceneManager(NULL), _row(0), _column(0), _tileWidth(0.0), _tileHeight(0.0), _sourceWidth(0.0), _sourceHeight(0.0), _data(NULL)
{

}

TileLayer::TileLayer(GameSceneManager* sceneManager) : _sceneManager(sceneManager), _row(0), _column(0), _tileWidth(0.0), _tileHeight(0.0), _sourceWidth(0.0), _sourceHeight(0.0), _data(NULL)
{

}

TileLayer::~TileLayer()
{

}

void TileLayer::InitProperties()
{
	_id = (*_data)["id"]->AsString();
	_order = (*_data)["order"]->AsNumber();
	_position = Vector2((*_data)["pos_x"]->AsNumber(), (*_data)["pos_y"]->AsNumber());
}

void TileLayer::InitGrid()
{
	_row = (*_data)["row"]->AsNumber();
	_column = (*_data)["column"]->AsNumber();
	_tileWidth = (*_data)["tile_width"]->AsNumber();
	_tileHeight = (*_data)["tile_height"]->AsNumber();
	_sourceWidth = (*_data)["source_height"]->AsNumber();
	_sourceHeight = (*_data)["source_height"]->AsNumber();

	_sceneManager->GetGrid()->Reset(_column, _row, _position, _tileWidth, _tileHeight, 0);
}

void TileLayer::InitResourceArray()
{
	Iterator<JSONValue*>* itr = (*_data)["map_data"]->ArrayIterator();
	int i = 0;

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		_sceneManager->GetGrid()->SetData(i, itr->Current()->AsNumber());
		i++;
	}
}

void TileLayer::InitMap()
{
	Iterator<JSONValue*>* itr = (*_data)["tilesets"]->ArrayIterator();
	Map* map = new Map(_sceneManager->GetGrid(), _sourceWidth, _sourceHeight);

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		string resourceId = itr->Current()->AsString();
		RenderResource* resource = dynamic_cast<RenderResource*>(ResourceManager::Instance()->FindResource(resourceId));
		map->AddResource(resource);
	}

	string actorId = (*_data)["actor_id"]->AsString();
	Vector2 gridSize = Vector2(_row * _tileHeight, _column * _tileWidth);
	SceneNode* sceneNode = new SceneNode(_position, gridSize);
	TileCollider* tileCollider = new TileCollider(_sceneManager->GetGrid());

	_sceneManager->AddActor(new GameActor(actorId, sceneNode, map, NULL, tileCollider, _order));
	_drawables.Add(map);
	CollisionManager::Instance()->AddCollidable(tileCollider);
}

void TileLayer::LoadFromJSON(JSONValue* data)
{
	_data = data;

	//Initialize the properties of the tile layer
	InitProperties();
	//Initialize the grid
	InitGrid();
	//Initialize the map
	InitResourceArray();
	//Initialize the resource array in Map
	InitMap();
}

Iterator<IDrawable*>* TileLayer::CreateIterator()
{
	return _drawables.CreateIterator();
}
