
#include "../../../Headers/Game/Physics/ObjectCollider.h"
#include "../../../Headers/Engine/Physics/CollisionManager.h"
using namespace Engine;
using namespace Game;

ObjectCollider::ObjectCollider()
{

}

ObjectCollider::~ObjectCollider()
{

}

void ObjectCollider::CollisionHandling()
{
	CollisionManager::Instance()->CollisionDetection(_actor);
}

void ObjectCollider::CollideWith(Actor* otherActor)
{
	/*
	if(otherActor != _actor && _actor->GetSceneNode()->GetAABB().Intersects(otherActor->GetSceneNode()->GetAABB()))
	{
		return _actor;
	}

	return NULL;
	*/
}

ICollidable* ObjectCollider::Clone()
{
	return new ObjectCollider();
}
