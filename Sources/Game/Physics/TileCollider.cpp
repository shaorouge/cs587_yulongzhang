
#include "../../../Headers/Game/Physics/TileCollider.h"
#include "../../../Headers/Engine/Physics/CollisionManager.h"
using namespace Engine;
using namespace Game;

TileCollider::TileCollider(Grid2<int>* grid) : _grid(grid)
{

}

TileCollider::~TileCollider()
{

}

void TileCollider::CollisionHandling()
{

}

void TileCollider::Modify(Actor* otherActor, const Vector2& tilePos)
{
	AABB& aabb = otherActor->GetSceneNode()->GetAABB();
	float x;
	float y;

	if(aabb.position.x > tilePos.x)
	{
		x = tilePos.x + _grid->GetBinSize().x - aabb.position.x;
	}
	else
	{
		x = aabb.position.x + aabb.bound.x - tilePos.x;
	}

	if(aabb.position.y > tilePos.y)
	{
		y = tilePos.y + _grid->GetBinSize().y - aabb.position.y;
	}
	else
	{
		y = aabb.position.y + aabb.bound.y - tilePos.y;
	}

	if(x > y)
	{
		otherActor->GetRigidbody()->SetVelocityY(0.0);
		aabb.position.y = (aabb.position.y < tilePos.y) ? tilePos.y - aabb.bound.y : tilePos.y + _grid->GetBinSize().y;
	}
	else
	{
		otherActor->GetRigidbody()->SetVelocityX(0.0);
		aabb.position.x = (aabb.position.x < tilePos.x) ? tilePos.x - aabb.bound.x : tilePos.x + _grid->GetBinSize().x;
	}
}

void TileCollider::CollideWith(Actor* otherActor)
{
	if(otherActor == NULL)
	{
		return;
	}

	Vector2 actorPos = otherActor->GetSceneNode()->GetAABB().position;

	uint topLeftIndex = _grid->GetIndex(actorPos);
	uint bottomRightIndex = _grid->GetIndex(actorPos + otherActor->GetSceneNode()->GetAABB().bound);

	uint rows = (bottomRightIndex - topLeftIndex) / _grid->GetBinX() + 1;
	uint columns = (bottomRightIndex - topLeftIndex) % _grid->GetBinX() + 1;
	uint bottomRow = topLeftIndex + _grid->GetBinX() * rows;

	for(uint i = topLeftIndex; i < bottomRow; i += _grid->GetBinX())
	{
		for(uint j = i; j < i + columns; j++)
		{
			if(_grid->GetData(j) == 0)
			{
				continue;
			}

			Modify(otherActor, _grid->GetCell(j).Dot(_grid->GetBinSize()));
		}
	}
}

ICollidable* TileCollider::Clone()
{
	return NULL;
}
