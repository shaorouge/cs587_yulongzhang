
#include "../../../Headers/Game/FSM/StateMachine.h"
using namespace Engine;
using namespace Game;
StateMachine::StateMachine()
{

}

void StateMachine::SetState(State* state)
{
    _stateMap.Set(state->GetStateName(), state);
}

void StateMachine::RemoveState(string stateName)
{
    _currentStates.Remove(_stateMap.Get(stateName));
}

void StateMachine::SwitchState(State* fromState, string toState)
{
    if(fromState != NULL)
    {
        for(uint i = 0; i < _currentStates.Size(); i++)
        {
            if(_currentStates[i] == fromState)
            {
                _currentStates[i]->Exit();
                _currentStates[i] = _stateMap.Get(toState);
                return;
            }
        }
    }

    State* newState = _stateMap.Get(toState);
    newState->Enter();
    _currentStates.Add(newState);
}




