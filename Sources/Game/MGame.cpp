
#include "../../Headers/Game/MGame.h"
using namespace Engine;
using namespace Game;

JSONValue* MGame::ParseFile(const string& filePath)
{
	FileReader::Instance()->SetInfileName(filePath);

	return _reader.Parse(FileReader::Instance()->ReadInfile());
}

void MGame::Init()
{
	JSONValue* initData = ParseFile("/home/shaorouge/git/cs587_yulongzhang/Resource/Json/init.json");

	_renderManager.Init(ParseFile((*initData)["render"]->AsString()));
	ResourceManager::Instance()->AddResourceLoader(&_renderManager);
	_level.InitLevel(&_sceneManager, &_inputManager, ParseFile((*initData)["level"]->AsString()));
	_renderManager.SetSceneManager(&_sceneManager);

	initData->Destory();

	_deltaTime = 1.0 / 60;
}

void MGame::Run()
{
	while(true)
	{
		_sceneManager.Tick(_deltaTime);
		_renderManager.Render();
	}
}

void MGame::Quit()
{
	delete CollisionManager::Instance();
	delete ResourceManager::Instance();
	delete ActorFactory::Instance();
	delete EventBus::Instance();
}
