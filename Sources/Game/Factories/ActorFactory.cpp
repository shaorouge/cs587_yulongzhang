
#include "../../../Headers/Game/Factories/ActorFactory.h"
#include "../../../Headers/Game/Physics/ObjectCollider.h"
using namespace Engine;
using namespace Game;

ActorFactory* ActorFactory::_instance = NULL;

ActorFactory::ActorFactory()
{

}

ActorFactory::~ActorFactory()
{
	Clear();
}

void ActorFactory::RegisterSprite(JSONValue* data)
{
	Iterator<JSONValue*>* itr = (*data)["sprites"]->ArrayIterator();

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		LoadSpriteData(itr->Current());
	}

	itr->Destory();
}

void ActorFactory::LoadSpriteData(JSONValue* data)
{
	string spriteId = (*data)["sprite_id"]->AsString();
	string resourceId = (*data)["resource_id"]->AsString();
	uint width = (*data)["width"]->AsNumber();
	uint height = (*data)["height"]->AsNumber();
	Sprite* newSprite = new Sprite(ResourceManager::Instance()->FindResource(resourceId), width, height);

	Iterator<JSONValue*>* itr = (*data)["actions"]->ArrayIterator();

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		ActionEnum action = static_cast<ActionEnum>((*itr->Current())["enum"]->AsNumber());
		uint row = (*itr->Current())["row"]->AsNumber();
		uint start = (*itr->Current())["start"]->AsNumber();
		uint end = (*itr->Current())["end"]->AsNumber();

		newSprite->SetAction(action, row, start, end);
	}

	_spriteMap.Set(spriteId, newSprite);

	itr->Destory();
}

void ActorFactory::RegisterActor(JSONValue* data)
{
	Iterator<JSONValue*>* itr = (*data)["actors"]->ArrayIterator();

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		string prototypeId = (*itr->Current())["prototype_id"]->AsString();
		string spriteId = (*itr->Current())["sprite_id"]->AsString();
		int layer = (*itr->Current())["layer"]->AsNumber();

		Sprite* sprite = _spriteMap.Get(spriteId);
		SceneNode* sceneNode = new SceneNode(Vector2::ZERO, sprite->GetSize());
		GameActor* prototypeActor = new GameActor(prototypeId, sceneNode, sprite, new Rigidbody(sceneNode), new ObjectCollider(), layer);

		_actorMap.Set(prototypeId, prototypeActor);
	}

	itr->Destory();
}

void ActorFactory::LoadFromJSON(JSONValue* data)
{
	RegisterSprite(data);
	RegisterActor(data);
}

GameActor* ActorFactory::CreateActor(const string& prototypeId, const string& newId, float posX, float posY)
{
	//Retrieve the prototype actor from the hash map
	GameActor* actor = _actorMap.Get(prototypeId)->Clone(newId);
	actor->GetSceneNode()->GetAABB().position = Vector2(posX, posY);

	return actor;
}

void ActorFactory::Clear()
{
	if(_actorMap.IsEmpty())
	{
		return;
	}

	Iterator<GameActor*>* itr = _actorMap.CreateIterator();

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		itr->Current()->Destory();
	}

	_spriteMap.Clear();
	_actorMap.Clear();

	itr->Destory();
}

ActorFactory* ActorFactory::Instance()
{
	if(_instance == NULL)
	{
		_instance = new ActorFactory();
	}

	return _instance;
}

void ActorFactory::Destory()
{
	delete this;
}

















