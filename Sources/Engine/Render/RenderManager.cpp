
#include "../../../Headers/Engine/Render/RenderManager.h"
using namespace Engine;

RenderManager::RenderManager() : _window(NULL), _renderer(NULL), _sceneManager(NULL)
{
	_type = GRAPHICS;
}

RenderManager::~RenderManager()
{

}

void RenderManager::Render()
{
    SDL_RenderClear(_renderer);  // clear the renderer to the draw

    Iterator<Layer*>* layerItr = _sceneManager->GetLayerItr();

    for(layerItr->First(); !layerItr->IsDone(); layerItr->Next())
    {
        Iterator<IDrawable*>* drawableItr = layerItr->Current()->CreateIterator();

        for(drawableItr->First(); !drawableItr->IsDone(); drawableItr->Next())
        {
            drawableItr->Current()->Draw(_renderer);
        }

        drawableItr->Destory();
    }

    layerItr->Destory();

    SDL_RenderPresent(_renderer);    // draw to the screen
}

void RenderManager::Init(JSONValue* data)
{
    bool fullScreen = (*data)["full_screen"]->AsBool();
    int flags = 0;

    if(fullScreen)
    {
        flags = SDL_WINDOW_FULLSCREEN;
    }

    if(SDL_Init(SDL_INIT_EVERYTHING) >= 0)
    {
        _window = SDL_CreateWindow((*data)["title"]->AsString().c_str(),
                                   (*data)["pos_x"]->AsNumber(),
                                   (*data)["pos_y"]->AsNumber(),
                                   (*data)["width"]->AsNumber(),
                                   (*data)["height"]->AsNumber(),
                                   flags);

        assert(_window != NULL);

        _renderer = SDL_CreateRenderer(_window, -1, SDL_RENDERER_ACCELERATED);

        assert(_renderer != NULL);

        SDL_SetRenderDrawColor(_renderer, 255, 255, 255, 255);
    }
}

IResource* RenderManager::LoadFromJSON(JSONValue* data)
{
    RenderResource* renderResource = new RenderResource();
    renderResource->LoadFromJSON(data);

    SDL_Surface* tempSurface = IMG_Load(renderResource->GetFilePath().c_str());

    assert(tempSurface != NULL);

    SDL_Texture* texture = SDL_CreateTextureFromSurface(_renderer, tempSurface);
    SDL_FreeSurface(tempSurface);

    assert(texture != NULL);

    renderResource->SetTexture(texture);

    return renderResource;
}
















