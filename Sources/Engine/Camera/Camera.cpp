
#include "../../../Headers/Engine/Camera/Camera.h"
using namespace Engine;

Camera::Camera(Vector2 position, int width, int height, int borderX, int borderY)
{
    SetCameraWindow(position, width, height, borderX, borderY);
}

Camera::~Camera()
{

}

void Camera::SetCameraWindow(Vector2 position, int width, int height, int borderX, int borderY)
{
    _position = position;
    _width = width;
    _height = height;
    _borderX = borderX;
    _borderY = borderY;
}

void Camera::Adjust(float& pos, int length, float newPos, int border)
{
	if(newPos < 0)
	{
		pos = 0;
	}
	else if(newPos + length > border)
	{
		pos = border - length;
	}
	else
	{
		pos = newPos;
	}
}

void Camera::LookAt(Vector2 position)
{
    float newPosX = position.x - _width / 2.0;
    float newPosY = position.y - _height / 2.0;

    Adjust(_position.x, _width, newPosX, _borderX);
    Adjust(_position.y, _height, newPosY, _borderY);
}

void Camera::Move(Vector2 displacement)
{
    _position = _position + displacement;
}

void Camera::Zoom(float zoom)
{
    //Zoom
}

void Camera::Rotate(float theta)
{
    //rotate the camera;
}

Matrix3 Camera::GetViewMatrix()
{
    return Matrix3::Translate(-_position.x, -_position.y);
}

bool Camera::Cull(Vector2 position, int objWidth, int objHeight)
{
    if(position.y + objHeight < _position.y || position.y > _position.y + _height ||
       position.x + objWidth < _position.x|| position.x > _position.x + _width)
    {
        return false;
    }

    return true;
}
