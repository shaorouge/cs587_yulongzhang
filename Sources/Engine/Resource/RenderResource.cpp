
#include "../../../Headers/Engine/Resource/RenderResource.h"
using namespace Engine;

RenderResource::RenderResource() : _sourceWidth(0), _sourceHeight(0), _texture(NULL)
{

}

RenderResource::RenderResource(int width, int height, SDL_Texture* texture) : _sourceWidth(width),
																			  _sourceHeight(height),
																			  _texture(texture)
{

}

RenderResource::~RenderResource()
{
    Unload();
}

void RenderResource::LoadFromJSON(JSONValue* data)
{
    _sourceWidth = (*data)["source_width"]->AsNumber();
    _sourceHeight = (*data)["source_height"]->AsNumber();
    _id = (*data)["id"]->AsString();
    _filePath = (*data)["file_path"]->AsString();
}

void RenderResource::Unload()
{
    if(_texture != NULL)
    {
        SDL_DestroyTexture(_texture);
    }
}
