
#include "../../../Headers/Engine/Resource/ResourceManager.h"
using namespace Engine;

ResourceManager* ResourceManager::_instance = NULL;

ResourceManager::ResourceManager()
{

}

ResourceManager::~ResourceManager()
{
	ClearResources();
	_loadersMap.Clear();
}

IResource* ResourceManager::FindResource(const string& id)
{
    if(_resourcesMap.Exists(id))
    {
        return _resourcesMap.Get(id);
    }

    return NULL;
}

void ResourceManager::ClearResources()
{
	if(_resourcesMap.IsEmpty())
	{
		return;
	}

	Iterator<IResource*>* itr = _resourcesMap.CreateIterator();

	for(itr->First(); !itr->IsDone(); itr->Next())
	{
		itr->Current()->Destory();
	}

	_resourcesMap.Clear();
	itr->Destory();
}

bool ResourceManager::LoadFromJSON(JSONValue* data)
{
    assert(data != NULL);

    Iterator<JSONValue*>* itr = (*data)["textures"]->ArrayIterator();

    for(itr->First(); !itr->IsDone(); itr->Next())
    {
        string type = (*itr->Current())["type"]->AsString();

        IResource* resource = _loadersMap.Get(type)->LoadFromJSON(itr->Current());

        assert(resource != NULL);

        _resourcesMap.Set(resource->GetId(), resource);
    }

    return false;
}

void ResourceManager::AddResourceLoader(IResourceLoader *resourceLoader)
{
    _loadersMap.Set(resourceLoader->GetLoaderTypeStr(), resourceLoader);
}

uint ResourceManager::GetResourceCount()
{
    return _resourcesMap.GetAllKeys().Size();
}

ResourceManager* ResourceManager::Instance()
{
	if(_instance == NULL)
	{
		_instance = new ResourceManager();
	}

	return _instance;
}

void ResourceManager::Destory()
{
	delete this;
}

