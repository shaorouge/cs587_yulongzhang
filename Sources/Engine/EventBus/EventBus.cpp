
#include "../../../Headers/Engine/EventBus/EventBus.h"
using namespace Engine;

bool EventBus::AddListenerList(string eventType)
{
    if(!_eventBusListenersMap.Exists(eventType))
    {
        DynamicArray<IEventBusListener*> newEventBusListenerList;
        _eventBusListenersMap.Set(eventType, newEventBusListenerList);
        return true;
    }
    return false;
}

bool EventBus::AddListener(IEventBusListener* listener, string eventType)
{
    IEventBusListenerList& eventListenerList = _eventBusListenersMap[eventType];

    for(uint i = 0; i <  eventListenerList.Size(); i++)
    {
        if(listener == eventListenerList[i])
        {
            return false;
        }
    }

    eventListenerList.Add(listener);

    return true;
}

bool EventBus::RemoveListener(IEventBusListener* listener, string eventType)
{
    IEventBusListenerList& eventListenerList = _eventBusListenersMap[eventType];

    for(uint i = 0; i <  eventListenerList.Size(); i++)
    {
        if(listener == eventListenerList[i])
        {
            eventListenerList.RemoveAt(i);
            return true;
        }
    }

    return false;
}

bool EventBus::RemoveListenerList(string eventType)
{
    IEventBusListenerList& eventListenerList = _eventBusListenersMap[eventType];

    eventListenerList.Clear();

    return true;
}

bool EventBus::Dispatch(Event* event)
{
    bool processed = false;
    IEventBusListenerList& eventListenerList = _eventBusListenersMap[event->GetEventType()];

    for(uint i = 0; i <  eventListenerList.Size(); i++)
    {
        eventListenerList[i]->OnEvent(event);
        processed = true;
    }

    return processed;
}

void EventBus::QueueEvent(Event* event)
{
    if(_eventBusListenersMap.Exists(event->GetEventType()))
    {
        _eventQueue.EnQueue(event);
    }
}

void EventBus::Tick(float deltaTime)
{
    while(!_eventQueue.IsEmpty())
    {
        Event* event = _eventQueue.Front();
        _eventQueue.DeQueue();

        IEventBusListenerList& eventListenerList = _eventBusListenersMap[event->GetEventType()];

        for(uint i = 0; i <  eventListenerList.Size(); i++)
        {
            eventListenerList[i]->OnEvent(event);
        }
    }
}

EventBus* EventBus::Instance()
{
    static EventBus instance;
    return &instance;
}
