
#include "../../../Headers/Engine/Scene/SceneNode.h"
using namespace Engine;

AABB::AABB() : position(Vector2(0.0f, 0.0f)), bound(Vector2(1.0f, 1.0f))
{

}

AABB::AABB(const Vector2& pos, const Vector2& bound) : position(pos), bound(bound)
{

}

bool AABB::Intersects(const AABB& other)
{
	Vector2 thisLowerBound = position + bound;
	Vector2 otherLowerBound = other.position + other.bound;

	if(position.x > otherLowerBound.x ||
	   position.y > otherLowerBound.y ||
	   thisLowerBound.x < other.position.x ||
	   thisLowerBound.y < other.position.y)
	{
		return false;
	}

	return true;
}

bool AABB::Contains(const AABB& other)
{
	Vector2 thisLowerBound = position + bound;
	Vector2 otherLowerBound = other.position + other.bound;

	if(otherLowerBound.x >= thisLowerBound.x &&
	   otherLowerBound.y >= thisLowerBound.y &&
	   other.position.x <= position.x &&
	   other.position.y <= position.y)
	{
		return true;
	}

	return false;
}

AABB& AABB::operator=(const AABB& other)
{
	position = other.position;
	bound = other.bound;

	return *this;
}

SceneNode::SceneNode()
{

}

SceneNode::SceneNode(const Vector2& pos, const Vector2& bound)
{
	SetAABB(pos, bound);
}

SceneNode::~SceneNode()
{

}

void SceneNode::Destory()
{
	delete this;
}












