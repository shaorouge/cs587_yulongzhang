
#include "../../../Headers/Engine/Scene/SceneManager.h"
using namespace Engine;

SceneManager::SceneManager() : _camera(NULL)
{

}

SceneManager::~SceneManager()
{
	Iterator<Layer*>* layerItr = _layers.CreateIterator();

	for(layerItr->First(); !layerItr->IsDone(); layerItr->Next())
	{
		delete layerItr->Current();
	}

	layerItr->Destory();

	Iterator<ITickable*>* tickItr = _tickables.CreateIterator();

	for(tickItr->First(); !tickItr->IsDone(); tickItr->Next())
	{
		delete tickItr->Current();
	}

	tickItr->Destory();

	delete _camera;
}

void SceneManager::AddTickable(ITickable* tickable)
{
    _tickables.Add(tickable);
}

void SceneManager::RemoveTickable(ITickable* tickable)
{
    _tickables.Remove(tickable);
}

void SceneManager::Tick(const float deltaTime)
{
	usleep(deltaTime * 1000000);

	for(uint i = 0; i < _tickables.Size(); i++)
	{
		_tickables[i]->Tick(deltaTime);
	}
}

void SceneManager::AddLayer(Layer* layer)
{
    _layers.Push(layer->GetOrder(), layer);
}

Iterator<Layer*>* SceneManager::GetLayerItr()
{
    return _layers.CreateIterator();
}
