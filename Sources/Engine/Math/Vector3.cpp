
#include "../../../Headers/Engine/Math/Vector3.h"
using namespace Engine;

//---------------------------Overload Operator----------------------------//


/**
 operator+(): Overload operator
 Description:Adds two Vector3D objects
 Parameters:

 vector1:Vector3
 The second Vector3D object.

 Returns:A Vector3 object that is the result of adding the
 current Vector3 object to second Vector3D object.
 */
 Vector3 Vector3::operator+(const Vector3& vector1)const
{
    return Vector3(x + vector1.x, y + vector1.y, z + vector1.z);
}
/**
 operator-(): Overload operator
 Description:Subtracts a Vector3 object from a
 Vector3D object.
 Parameters:

 vector1:Vector3
 The second Vector3 object.

 Returns:A Vector3 object that is the result of
 subtracting the current Vector3D object from
 second Vector3D
 */
 Vector3 Vector3::operator-(const Vector3& vector1)const
{
    return Vector3(x - vector1.x, y - vector1.y, z - vector1.z);
}

/**
 operator-(): Overload operator
 Description: Negates  the current Vector3 object.


 Returns:A Vector3 object that is the result of negating  the
 current Vector3
 */
 Vector3 Vector3::operator-()const
{
    float value1 = -x;
    float value2 = -y;
    float value3 = -z;
    Vector3  vector1(value1,value2,value3);
    return vector1;
}

 Vector3 Vector3::operator*(const float scalar)const
{
    if (scalar == 0)
    {
        return Vector3::ZERO;
    }

    Vector3  vector1(x * scalar, y * scalar ,z * scalar);

    return vector1;
}

/**
 operator/(): Overload operator
 Description: Divides the current Vector3D object by
 the specified scalar.
 Parameters:

 scalar:float
 The scalar to divide vector by

 Returns:A Vector3D object that is the result of dividing c
 urrent vector by scalar.
 */
 Vector3 Vector3::operator/(const float scalar)const
{
    assert(scalar != 0);

    Vector3  vector1(x / scalar, y / scalar ,z / scalar);

    return vector1;
}

/**
 operator=(): Overload operator
 Description: Assgins all the values from target Vector3D
 object to the current Vector3D object.
 Parameters:

 vector1:Vector3D
 The target Vector3D

 Returns:A reference of current Vector3D object.
 */
 Vector3& Vector3::operator=(const Vector3 &vector1)
{
    x = vector1.x;
    y = vector1.y;
    z = vector1.z;

    return *this;
}
/**
 operator==(): Overload operator
 Description:Compares two Vector3 objects for equality.
 Parameters:

 vector1:Vector3
 The second Vector3 structure to compare.

 Returns:True if the x, y, and z components of current
 vector3 object and vector2 are equal; false otherwise.
 */
 bool Vector3::operator==(const Vector3 &vector1)const
{
    return (x == vector1.x && y == vector1.y && z == vector1.z);
}

 float& Vector3::operator[](int i)
{
    switch(i)
    {
        case 0: return x; break;
        case 1: return y; break;
        case 2: return z; break;
    }

    assert(false);
}

const float Vector3::operator[](int i) const
{
    switch(i)
    {
        case 0: return x; break;
        case 1: return y; break;
        case 2: return z; break;
    }

    assert(false);
}

//----------------------Predefined const vectors----------------------//

/**
 ZERO :constant
 [Public static const]
 Vector that defines the (0,0,-1) constant
 */
const  Vector3 Vector3::ZERO = Vector3(0.0f, 0.0f, 0.0f);

/**
 UP    :constant
 [Public static const]
 Vector that defines the (0,1,0) constant
 */
const Vector3 Vector3::UP = Vector3(0.0f, 1.0f, 0.0f);

/**
 DOWN   :constant
 [Public static const]
 Vector that defines the (0,-1,0) constant
 */
const Vector3 Vector3::DOWN = Vector3(0.0f, -1.0f, 0.0f);

/**
 RIGHT  :constant
 [Public static const]
 Vector that defines the (1,0,0) constant
 */
const Vector3 Vector3::RIGHT = Vector3(1.0f, 0.0f, 0.0f);

/**
 LEFT  :constant
 [Public static const]
 Vector that defines the (-1,0,0) constant
 */
const Vector3 Vector3::LEFT = Vector3(-1.0f, 0.0f, 1.0f);

/**
 FORWARD :constant
 [Public static const]
 Vector that defines the (0,0,1) constant
 */

const Vector3 Vector3::FORWARD = Vector3(0.0f, 0.0f, 1.0f);

/**
 BACK    :constant
 [Public static const]
 Vector that defines the (0,0,-1) constant
 */
const Vector3 Vector3::BACK = Vector3(0.0f, 0.0f, -1.0f);

//-----------------------The constructors  and distructors------------------//

/**
 Vector3d(): Constructor
 Districption: The defult constructor ,initializes a new instance
 of a Vector3D with elements (0,0,0)
 */
Vector3::Vector3()
{
    x = 0;
    y = 0;
    z = 0;
}

/**
 Vector3D(): Constructor
 Description: Initializes a new instance
 of a Vector3D with elements(value1,value2,value3)
 Parameters:

 value1:float
 The new Vector3D object's x value

 value2:float
 The new Vector3D object's y value

 value3:float
 The new Vector3D object's z  value
 */


Vector3::Vector3(const float value1,
                 const float value2,
                 const float value3)
{
    x = value1;
    y = value2;
    z = value3;
}

/**
 ~Vector3D(): Distructor
 Description: The Distructor of Vector3D object
 */
Vector3::~Vector3(){};

//-----------------------Public  Static Methods---------------------------------------//


/**
 Dot(): Public static method
 Description: Calculates the dot product with the current
 Vector3D object and a specifed Vector3D object
 Parameters:

 vector1:Vector3D
 The first Vector3D object.
 vector2:Vector3D
 The second Vector3D object.


 Return: A scalar which is the dot product of the current
 Vector3D object and the specified Vector3D object
 */

 float Vector3::Dot(const Vector3 &vector1,
                          const Vector3 &vector2)
{
    return vector1.x * vector2.x + vector1.y * vector2.y + vector1.z * vector2.z;
}

/**
 Cross(): Public static method
 Description: Calculates the cross product of two
 Vector3D objects.
 Parameters:

 vector1:Vector3D
 The first Vector3D object.
 vector2:Vector3D
 The second Vector3D object.

 Returns: A Vector3D object which is the Cross product
 of the two Vector3D object
 */

 Vector3 Vector3::Cross(const Vector3 &vector1,
                              const Vector3 &vector2)
{
    Vector3 instance;
    instance.x = vector1.y * vector2.z - vector1.z * vector2.y;
    instance.y = vector1.z * vector2.x - vector1.x * vector2.z;
    instance.z = vector1.x * vector2.y - vector1.y * vector2.x;

    return instance;
}
/**
 Set(): Public method
 Description:Sets the current  Vector3 object's value
 Parameters:

 value1:float
 The current Vector3 object's x value.

 value2:float
 The current Vector3 object's y value.

 value3:float
 The current Vector3 object's z  value.
 */


 void Vector3::Set(const float value1,
                         const float value2,
                         const float value3)
{
    x = value1;
    y = value2;
    z = value3;
}

/**
 Length(): Public method

 Description: Calculates   the current Vector3
 object's length.

 Returns: A scalar which is the current
 Vector3 object's length.
 */



 float Vector3::Length()const
{
    return sqrt(LengthSquared());

}
/**
 LengthSquared(): Public method

 Description: Calculates   the current Vector3 object's
 LengthSquared.

 Returns: A scalar which is the current
 Vector3 object's LengthSquared.
 */

 float Vector3::LengthSquared() const
{
    float lenSquared = x * x + y * y + z * z;

    return lenSquared;
}
/**
 Normalize(): Public method

 Description: Normalizes the current Vector3D object

 Ret
 */

 void Vector3::Normalize()
{
    float len = Length();// Gets the current Vector3 object's length;
    assert (len!=0); //when the length of the current Vector3 equal to 0

    x = x / len;
    y = y / len;
    z = z / len;
}
