
#include "../../../Headers/Engine/Math/Matrix3.h"
using namespace Engine;

Matrix3::Matrix3()
{
    _matrix[0].x = 1.0;
    _matrix[1].y = 1.0;
    _matrix[2].z = 1.0;
}

Matrix3::Matrix3(float value)
{
    _matrix[0].x = value;
    _matrix[1].y = value;
    _matrix[2].z = value;
}

Matrix3::Matrix3(const Vector3& a, const Vector3& b, const Vector3& c)
{
    _matrix[0] = a;
    _matrix[1] = b;
    _matrix[2] = c;
}

Matrix3::Matrix3(const Matrix3& m)
{
    _matrix[0] = m._matrix[0];
    _matrix[1] = m._matrix[1];
    _matrix[2] = m._matrix[2];
}

Vector3& Matrix3::operator[](int i)
{
    return _matrix[i];
}

const Vector3& Matrix3::operator[](int i) const
{
    return _matrix[i];
}

Matrix3 Matrix3::operator+(const Matrix3& m) const
{
    return Matrix3(_matrix[0] + m[0], _matrix[1] + m[1], _matrix[2] + m[2]);
}

Matrix3 Matrix3::operator-(const Matrix3& m) const
{
    return Matrix3(_matrix[0] - m[0], _matrix[1] - m[1], _matrix[2] - m[2]);
}

Matrix3 Matrix3::operator*(const float s) const
{
    return Matrix3(_matrix[0] * s, _matrix[1] * s, _matrix[2] * s);
}

Matrix3 Matrix3::operator/(const float s) const
{
    assert(s != 0);

    return Matrix3(_matrix[0] / s, _matrix[1] / s, _matrix[2] / s);
}

Matrix3 Matrix3::operator*(const Matrix3& m) const
{
    Matrix3 newMatrix(0.0);

    for(int i = 0; i < 3; i++)
    {
        for(int j = 0; j < 3; j++)
        {
            for(int k = 0; k < 3; k++)
            {
                newMatrix[i][j] += _matrix[i][k] * m[k][j];
            }
        }
    }

    return newMatrix;
}

Vector3 Matrix3::operator*(const Vector3& v) const
{
    return Vector3(_matrix[0][0] *  v.x + _matrix[0][1] * v.y + _matrix[0][2] * v.z,
                   _matrix[1][0] *  v.x + _matrix[1][1] * v.y + _matrix[1][2] * v.z,
                   _matrix[2][0] *  v.x + _matrix[2][1] * v.y + _matrix[2][2] * v.z);
}



