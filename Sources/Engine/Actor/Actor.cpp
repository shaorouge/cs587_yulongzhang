
#include "../../../Headers/Engine/Actor/Actor.h"
using namespace Engine;

Actor::Actor() : _sceneNode(NULL), _drawable(NULL), _rigidbody(NULL), _collidable(NULL), _layer(0)
{

}

Actor::Actor(const string& id, SceneNode* sceneNode, IDrawable* drawable, Rigidbody* rigidbody, ICollidable* collidable, int layer) :
																					_id(id),
																					_sceneNode(sceneNode),
																					_drawable(drawable),
																					_rigidbody(rigidbody),
																					_collidable(collidable),
																					_layer(layer)
{
	if(_collidable != NULL)
	{
		_collidable->SetActor(this);
	}
}

Actor::~Actor()
{
	if(_sceneNode != NULL)
	{
		delete _sceneNode;
	}

	if(_rigidbody != NULL)
	{
		delete _rigidbody;
	}

	if(_drawable != NULL)
	{
		delete _drawable;
	}

	if(_collidable != NULL)
	{
		delete _collidable;
	}
}

