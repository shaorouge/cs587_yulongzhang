
#include <assert.h>
#include "../../../Headers/Engine/Json/JSONReader.h"
using namespace Engine;

JSONReader::JSONReader() : _currentToken(Null), _currentNum(0),_currentStr(""), _begin(0), _end(0), _current(0)
{

}

JSONReader::~JSONReader()
{

}

JSONValue* JSONReader::Parse(const string& document)
{
	_begin = const_cast<char*>(document.c_str());
	_end = const_cast<char*>(document.c_str() + document.size());
	_current = _begin;

	while(!_valueStack.IsEmpty())
	{
		_valueStack.Pop();
	}

	ReadToken();

	_valueStack.Push(new JSONValue());
	ReadValue();

	return _valueStack.Pop();
}

void JSONReader::ReadValue()
{
	switch(_currentToken)
	{
		case ObjectBegin :
			ReadObject();
			break;
		case ArrayBegin :
			ReadArray();
			break;
		case String :
			_valueStack.Peek()->SetValue(ReadString());
			break;
		case Number :
			_valueStack.Peek()->SetValue(ReadNumber());
			break;
		case True :
			_valueStack.Peek()->SetValue(true);
			break;
		case False :
			_valueStack.Peek()->SetValue(false);
			break;
		case Null :
			;
			break;
		default :
			assert(false);
	}
}

void JSONReader::ReadToken()
{
	char c = GetNextChar();

	switch(c)
	{
	case '{' :
		_currentToken = ObjectBegin;
		break;
	case '}' :
		_currentToken = ObjectEnd;
		break;
	case '[' :
		_currentToken = ArrayBegin;
		break;
	case ']' :
		_currentToken = ArrayEnd;
		break;
	case '"' :
		_currentToken = String;
		break;
	case '0':
	case '1':
	case '2':
	case '3':
	case '4':
	case '5':
	case '6':
	case '7':
	case '8':
	case '9':
		_currentToken = Number;
		break;
	case 't':
		assert(Match("rue", 3));
		_currentToken = True;
		break;
	case 'f':
		assert(Match("alse", 4));
		_currentToken = False;
		break;
	case 'n':
		assert(Match("ull", 3));
		_currentToken = Null;
		break;
	case ',':
		_currentToken = ArraySeperator;
		break;
	case ':':
		_currentToken = MemberSeperator;
		break;
	default:
		assert(false);
		break;
	}
}

int JSONReader::ReadNumber()
{
	_current--;
	int num = 0;

	while(*_current >= '0' && *_current <= '9')
	{
		num = num * 10 + (*_current - '0');
		_current++;
	}

	assert(*_current == ',' || *_current == '}' || *_current == ']');

	return num;
}

const string JSONReader::ReadString()
{
	string newStr;

	while(char c = GetNextChar())
	{
		if(c != '"')
		{
			newStr.push_back(c);
		}
		else
		{
			break;
		}
	}

	return newStr;
}

void JSONReader::ReadObject()
{
	if(*(_current + 1) == '}')
	{
		ReadToken();
		return;
	}

	//The current token should be ObjectStart
	while(_currentToken != ObjectEnd)
	{
		//The next token should be String
		ReadToken();

		assert(_currentToken == String);

		//Set up a new JSONValue
		JSONValue* value = _valueStack.Peek()->NewValue(ReadString());

		ReadToken();

		assert(_currentToken == MemberSeperator);

		ReadToken();

		_valueStack.Push(value);
		ReadValue();
		_valueStack.Pop();

		ReadToken();

		assert(_currentToken == ArraySeperator || _currentToken == ObjectEnd);
	}
}

void JSONReader::ReadArray()
{
	if(*(_current + 1) == ']')
	{
		ReadToken();
		return;
	}

	while(_currentToken != ArrayEnd)
	{
		ReadToken();

		assert(_currentToken != ObjectEnd && _currentToken != ArraySeperator && _currentToken != MemberSeperator);

		JSONValue* value = _valueStack.Peek()->AddValue();	//Add new value to array

		_valueStack.Push(value);
		ReadValue();
		_valueStack.Pop();

		ReadToken();

		assert(_currentToken == ArraySeperator || _currentToken == ArrayEnd);
	}
}

char JSONReader::GetNextChar()
{
	if(_current == _end)
	{
		return 0;
	}

	return *_current++;
}

bool JSONReader::Match(const string& matchStr, uint length)
{
	assert(length == matchStr.length());

	for(uint i = 0; i < length; i++)
	{
		if(*(_current + i) != matchStr[i])
		{
			return false;
		}
	}

	_current += length;

	return true;
}














