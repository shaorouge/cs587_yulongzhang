
#include "../../../Headers/Engine/Json/JSONValue.h"
#include <iostream>
using namespace std;
using namespace Engine;

JSONValue::JSONValue() : _type(Null)
{

}

JSONValue::~JSONValue()
{
	if(_type == Object)
	{
		Iterator<JSONValue*>* itr = _valueHolder.map->CreateIterator();

		for(itr->First(); !itr->IsDone(); itr->Next())
		{
			itr->Current()->Destory();
		}

		itr->Destory();

		delete _valueHolder.map;
	}
	else if(_type == Array)
	{
		Iterator<JSONValue*>* itr = _valueHolder.array->CreateIterator();

		for(itr->First(); !itr->IsDone(); itr->Next())
		{
			itr->Current()->Destory();
		}

		itr->Destory();

		delete _valueHolder.array;
	}
	else if(_type == String)
	{
		delete _valueHolder.str;
	}
}

int JSONValue::AsNumber()
{
	assert(_type == Number);

	return _valueHolder.num;
}

string JSONValue::AsString()
{
	assert(_type == String);

	return _valueHolder.str;
}

bool JSONValue::AsBool()
{
	assert(_type == Boolean);

	return _valueHolder.boolean;
}

JSONValue* JSONValue::operator[](int index)
{
	assert(_type == Array && index >= 0);

	return _valueHolder.array->At(index);
}

JSONValue* JSONValue::operator[](const string& key)
{
	assert(_type == Object);

	return _valueHolder.map->Get(key);
}

void JSONValue::SetValue(const string& value)
{
	_type = String;
	char* newStr = new char[value.length()];

	for(uint i = 0; i < value.length(); i++)
	{
		newStr[i] = value[i];
	}

	newStr[value.length()] = 0;
	_valueHolder.str = newStr;
}

void JSONValue::SetValue(const int value)
{
	_type = Number;
	_valueHolder.num = value;
}

void JSONValue::SetValue(const bool value)
{
	_type = Boolean;
	_valueHolder.boolean = value;
}

JSONValue* JSONValue::NewValue(const string& value)
{
	JSONValue* newValue = new JSONValue();

	if(_type == Null)
	{
		_type = Object;
		_valueHolder.map = new HashMap<JSONValue*>();
	}

	_valueHolder.map->Set(value, newValue);

	return newValue;
}

JSONValue* JSONValue::AddValue()
{
	JSONValue* newValue = new JSONValue();

	if(_type == Null)
	{
		_type = Array;
		_valueHolder.array = new DynamicArray<JSONValue*>();
	}

	_valueHolder.array->Add(newValue);

	return newValue;
}

Iterator<JSONValue*>* JSONValue::ArrayIterator()
{
	return _valueHolder.array->CreateIterator();
}

void JSONValue::Destory()
{
	delete this;
}


















