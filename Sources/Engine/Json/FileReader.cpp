
#include "../../../Headers/Engine/Json/FileReader.h"
using namespace Engine;

FileReader::FileReader():_infileName(),_outfileName()
{

}

FileReader* FileReader::Instance()
{
    static FileReader instance;
    return &instance;
}

string FileReader::ReadInfile()
{
    ifstream in_stream;
    in_stream.open(_infileName.c_str());

    if(in_stream.fail())
    {
        cout << "Fail to open " << _infileName << endl;
        exit(1);
    }

    string tmp;
    string str;

    while(!in_stream.eof())
    {
        in_stream >> tmp;
        str += tmp;
    }

    in_stream.close();

    return str;
}

void FileReader::WriteOutfile(string str)
{
    ofstream out_stream;
    out_stream.open(_outfileName.c_str(), ios::app);

    if(out_stream.fail())
    {
        cout << "Fail to open " << _outfileName << endl;
        exit(1);
    }

    out_stream << str << endl;
    out_stream.close();
}

void FileReader::ClearOutfile()
{
    ofstream out_stream;
    out_stream.open(_outfileName.c_str());

    if(out_stream.fail())
    {
        cout << "Fail to open " << _outfileName << endl;
        exit(1);
    }

    out_stream.clear();
    out_stream.close();
}
