
#include "../../../Headers/Engine/Physics/CollisionManager.h"
using namespace Engine;

CollisionManager* CollisionManager::_instance = NULL;

CollisionManager::CollisionManager()
{

}

CollisionManager::~CollisionManager()
{

}

void CollisionManager::CollisionDetection(Actor* otherActor)
{
	for(uint i = 0; i < _collidables.Size(); i++)
	{
		_collidables[i]->CollideWith(otherActor);
	}
}

void CollisionManager::AddCollidable(ICollidable* collidable)
{
	_collidables.Add(collidable);
}

void CollisionManager::RemoveCollidable(ICollidable* collidable)
{
	for(uint i = 0; i < _collidables.Size(); i++)
	{
		if(collidable == _collidables[i])
		{
			_collidables.RemoveAt(i);

			return;
		}
	}
}

CollisionManager* CollisionManager::Instance()
{
	if(_instance == NULL)
	{
		_instance = new CollisionManager();
	}

	return _instance;
}
