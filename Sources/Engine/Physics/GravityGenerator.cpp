
#include "../../../Headers/Game/Physics/GravityGenerator.h"
using namespace Engine;

GravityGenerator::GravityGenerator(const Vector2& gravity) : _gravity(gravity)
{

}

void GravityGenerator::UpdateForce(Rigidbody* rigidbody, const float deltaTime)
{
	//If a rigidbody has infinite mass, return
	if(rigidbody->GetInverseMass() == 0)
	{
		return;
	}

	rigidbody->AddForce(_gravity / rigidbody->GetInverseMass());
}
