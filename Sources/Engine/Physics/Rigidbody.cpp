
#include "../../../Headers/Engine/Physics/Rigidbody.h"
#include "../../../Headers/Engine/Scene/SceneManager.h"
using namespace Engine;

Rigidbody::Rigidbody() : _damping(0),
						 _inverseMass(1.0f),
						 _sceneNode(NULL),
						 _velocity(Vector2::ZERO),
						 _acceleration(Vector2::ZERO)
{

}

Rigidbody::Rigidbody(SceneNode* sceneNode) : _damping(0),
		 	 	 	 	 	 	 	 	 	  _inverseMass(1.0f),
		 	 	 	 	 	 	 	 	 	  _sceneNode(sceneNode),
		 	 	 	 	 	 	 	 	 	  _velocity(Vector2::ZERO),
		 	 	 	 	 	 	 	 	 	  _acceleration(Vector2::ZERO)
{

}

Rigidbody::Rigidbody(float damping, float inverseMass, SceneNode* sceneNode, const Vector2& velocity, const Vector2& acceleration) :
																		_damping(damping),
																		_inverseMass(inverseMass),
																		_sceneNode(sceneNode),
																		_velocity(velocity),
																		_acceleration(acceleration)
{

}

void Rigidbody::Integrate(float deltaTime)
{
	assert(deltaTime > 0.0);

	//Update linear position
	_sceneNode->GetAABB().position += _velocity * deltaTime;// + acceleration * deltaTime * deltaTime * 0.5;

	//Work out the acceleration from the force
	Vector2 resultingAcc = _acceleration + _forceAccum * _inverseMass;

	//Update linear velocity from the acceleration
	_velocity += resultingAcc * deltaTime;

	//Clear the forces
	ClearAccumulator();
}

void Rigidbody::ClearAccumulator()
{
	_forceAccum = Vector2::ZERO;
}

void Rigidbody::AddForce(const Vector2& force)
{
	_forceAccum += force;
}






















