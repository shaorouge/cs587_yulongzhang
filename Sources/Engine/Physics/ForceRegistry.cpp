
#include "../../../Headers/Engine/Physics/ForceRegistry.h"
using namespace Engine;

ForceRegistry::~ForceRegistry()
{
	Clear();
}

void ForceRegistry::Add(Rigidbody* rigidbody, IForceGenerator* fg)
{
	_registrations.Add(new ForceRegistration(rigidbody, fg));
}

void ForceRegistry::Remove(Rigidbody* rigidbody, IForceGenerator* fg)
{
	for(uint i = 0; i < _registrations.Size(); i++)
	{
		if(rigidbody == _registrations[i]->rigidbody &&
		   fg == _registrations[i]->fg)
		{
			delete _registrations[i];
			_registrations.RemoveAt(i);
		}
	}
}

void ForceRegistry::Clear()
{
	Iterator<ForceRegistration*>* iterator = _registrations.CreateIterator();

	for(iterator->First(); !iterator->IsDone(); iterator->Next())
	{
		delete iterator->Current();
	}

	iterator->Destory();
}

void ForceRegistry::Tick(const float deltaTime)
{
	for(Iterator<ForceRegistration*>* iterator = _registrations.CreateIterator();
		!iterator->IsDone(); iterator->Next())
	{
		iterator->Current()->fg->UpdateForce(iterator->Current()->rigidbody, deltaTime);
	}
}

