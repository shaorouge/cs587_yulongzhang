
#include "../../../Headers/Engine/IO/Keyboard.h"
using namespace Engine;

Keyboard::Keyboard() : _keyListener(NULL), _buffered(true), _numEvents(10)
{
	Init();
}

Keyboard::Keyboard(bool buffered) : _keyListener(NULL), _buffered(buffered), _numEvents(1)
{
	Init();
}

Keyboard::Keyboard(bool buffered, int numEvents) : _keyListener(NULL), _buffered(buffered), _numEvents(numEvents)
{
	Init();
}

Keyboard::~Keyboard()
{

}

void Keyboard::CopyKeyStates(unsigned char keys[256]) const
{
	for(int i = 0; i < 256; i++)
	{
		keys[i] = _keysState[i];
	}
}

void Keyboard::Capture()
{
	SDL_Event events[_numEvents];
	SDL_PumpEvents();
	int count = SDL_PeepEvents(events, _numEvents, SDL_GETEVENT, SDL_KEYDOWN, SDL_KEYUP);

	for(int i = 0; i < count; i++)
	{
		KeyCode kc = _keyMap[string(SDL_GetKeyName(events[i].key.keysym.sym))];
		_keysState[kc] = events[i].key.state;

		if(_buffered && _keyListener)
		{
			if(events[i].key.state == SDL_PRESSED)
			{
				if(!_keyListener->keyPressed(KeyEvent(kc)))
					break;

			}
			else
			{
				if(!_keyListener->keyReleased(KeyEvent(kc)))
					break;
			}
		}
	}

}

string Keyboard::GetAsString(KeyCode kc)
{
	switch(kc)
	{
		case KC_UNASSIGNED: return string("UNASSIGNED");
		case KC_ESCAPE: return string(SDL_GetKeyName(SDLK_ESCAPE));
		case KC_1: return string(SDL_GetKeyName(SDLK_1));
		case KC_2: return string(SDL_GetKeyName(SDLK_2));
		case KC_3: return string(SDL_GetKeyName(SDLK_3));
		case KC_4: return string(SDL_GetKeyName(SDLK_4));
		case KC_5: return string(SDL_GetKeyName(SDLK_5));
		case KC_6: return string(SDL_GetKeyName(SDLK_6));
		case KC_7: return string(SDL_GetKeyName(SDLK_7));
		case KC_8: return string(SDL_GetKeyName(SDLK_8));
		case KC_9: return string(SDL_GetKeyName(SDLK_9));
		case KC_0: return string(SDL_GetKeyName(SDLK_0));
		case KC_MINUS: return string(SDL_GetKeyName(SDLK_MINUS));
		case KC_EQUALS: return string(SDL_GetKeyName(SDLK_EQUALS));
		case KC_BACK: return string(SDL_GetKeyName(SDLK_BACKSPACE));
		case KC_TAB: return string(SDL_GetKeyName(SDLK_TAB));
		case KC_Q: return string(SDL_GetKeyName(SDLK_q));
		case KC_W: return string(SDL_GetKeyName(SDLK_w));
		case KC_E: return string(SDL_GetKeyName(SDLK_e));
		case KC_R: return string(SDL_GetKeyName(SDLK_r));
		case KC_T: return string(SDL_GetKeyName(SDLK_t));
		case KC_Y: return string(SDL_GetKeyName(SDLK_y));
		case KC_U: return string(SDL_GetKeyName(SDLK_u));
		case KC_I: return string(SDL_GetKeyName(SDLK_i));
		case KC_O: return string(SDL_GetKeyName(SDLK_o));
		case KC_P: return string(SDL_GetKeyName(SDLK_p));
		case KC_LBRACKET: return string("[");
		case KC_RBRACKET: return string("]");
		case KC_RETURN: return string(SDL_GetKeyName(SDLK_RETURN));
		case KC_LCONTROL: return string(SDL_GetKeyName(SDLK_LCTRL));
		case KC_A: return string(SDL_GetKeyName(SDLK_a));
		case KC_S: return string(SDL_GetKeyName(SDLK_s));
		case KC_D: return string(SDL_GetKeyName(SDLK_d));
		case KC_F: return string(SDL_GetKeyName(SDLK_f));
		case KC_G: return string(SDL_GetKeyName(SDLK_g));
		case KC_H: return string(SDL_GetKeyName(SDLK_h));
		case KC_J: return string(SDL_GetKeyName(SDLK_j));
		case KC_K: return string(SDL_GetKeyName(SDLK_k));
		case KC_L: return string(SDL_GetKeyName(SDLK_l));
		case KC_SEMICOLON: return string(SDL_GetKeyName(SDLK_SEMICOLON));
		case KC_APOSTROPHE: return string(SDL_GetKeyName(SDLK_QUOTE));
		case KC_GRAVE: return string(SDL_GetKeyName(SDLK_BACKQUOTE));
		case KC_LSHIFT: return string(SDL_GetKeyName(SDLK_LSHIFT));
		case KC_BACKSLASH: return string(SDL_GetKeyName(SDLK_BACKSLASH));
		case KC_Z: return string(SDL_GetKeyName(SDLK_z));
		case KC_X: return string(SDL_GetKeyName(SDLK_x));
		case KC_C: return string(SDL_GetKeyName(SDLK_c));
		case KC_V: return string(SDL_GetKeyName(SDLK_v));
		case KC_B: return string(SDL_GetKeyName(SDLK_b));
		case KC_N: return string(SDL_GetKeyName(SDLK_n));
		case KC_M: return string(SDL_GetKeyName(SDLK_m));
		case KC_COMMA: return string(SDL_GetKeyName(SDLK_COMMA));
		case KC_PERIOD: return string(SDL_GetKeyName(SDLK_PERIOD));
		case KC_SLASH: return string(SDL_GetKeyName(SDLK_SLASH));
		case KC_RSHIFT: return string(SDL_GetKeyName(SDLK_RSHIFT));
		case KC_MULTIPLY: return string(SDL_GetKeyName(SDLK_KP_MULTIPLY));
		case KC_LMENU: return string(SDL_GetKeyName(SDLK_LALT));
		case KC_SPACE: return string(SDL_GetKeyName(SDLK_SPACE));
		case KC_CAPITAL: return string(SDL_GetKeyName(SDLK_CAPSLOCK));
		case KC_F1: return string(SDL_GetKeyName(SDLK_F1));
		case KC_F2: return string(SDL_GetKeyName(SDLK_F2));
		case KC_F3: return string(SDL_GetKeyName(SDLK_F3));
		case KC_F4: return string(SDL_GetKeyName(SDLK_F4));
		case KC_F5: return string(SDL_GetKeyName(SDLK_F5));
		case KC_F6: return string(SDL_GetKeyName(SDLK_F6));
		case KC_F7: return string(SDL_GetKeyName(SDLK_F7));
		case KC_F8: return string(SDL_GetKeyName(SDLK_F8));
		case KC_F9: return string(SDL_GetKeyName(SDLK_F9));
		case KC_F10: return string(SDL_GetKeyName(SDLK_F10));
		case KC_NUMLOCK: return string(SDL_GetKeyName(SDLK_NUMLOCKCLEAR));
		case KC_SCROLL: return string(SDL_GetKeyName(SDLK_SCROLLLOCK));
		case KC_NUMPAD7: return string(SDL_GetKeyName(SDLK_KP_7));
		case KC_NUMPAD8: return string(SDL_GetKeyName(SDLK_KP_8));
		case KC_NUMPAD9: return string(SDL_GetKeyName(SDLK_KP_9));
		case KC_SUBTRACT: return string(SDL_GetKeyName(SDLK_KP_MINUS));
		case KC_NUMPAD4: return string(SDL_GetKeyName(SDLK_KP_4));
		case KC_NUMPAD5: return string(SDL_GetKeyName(SDLK_KP_5));
		case KC_NUMPAD6: return string(SDL_GetKeyName(SDLK_KP_6));
		case KC_ADD: return string(SDL_GetKeyName(SDLK_KP_PLUS));
		case KC_NUMPAD1: return string(SDL_GetKeyName(SDLK_KP_1));
		case KC_NUMPAD2: return string(SDL_GetKeyName(SDLK_KP_2));
		case KC_NUMPAD3: return string(SDL_GetKeyName(SDLK_KP_3));
		case KC_NUMPAD0: return string(SDL_GetKeyName(SDLK_KP_0));
		case KC_DECIMAL: return string(SDL_GetKeyName(SDLK_KP_PERIOD));
		case KC_OEM_102: return string("OEM_102");
		case KC_F11: return string(SDL_GetKeyName(SDLK_F11));
		case KC_F12: return string(SDL_GetKeyName(SDLK_F12));
		case KC_F13: return string(SDL_GetKeyName(SDLK_F13));
		case KC_F14: return string(SDL_GetKeyName(SDLK_F14));
		case KC_F15: return string(SDL_GetKeyName(SDLK_F15));
		case KC_KANA: return string("Kana");
		case KC_ABNT_C1: return string("ABNT_C1");
		case KC_CONVERT: return string("CONVERT");
		case KC_NOCONVERT: return string("NOCONVERT");
		case KC_YEN: return string("YEN");
		case KC_ABNT_C2: return string("ABNT_C2");
		case KC_NUMPADEQUALS: return string(SDL_GetKeyName(SDLK_KP_EQUALS));
		case KC_PREVTRACK: return string("KC_PREVTRACK");
		case KC_AT: return string("KC_AT");
		case KC_COLON: return string(SDL_GetKeyName(SDLK_COLON));
		case KC_UNDERLINE: return string("KC_UNDERLINE");
		case KC_KANJI: return string("KC_KANJI");
		case KC_STOP: return string("KC_STOP");
		case KC_AX: return string("KC_AX");
		case KC_UNLABELED: return string("KC_UNLABELED");
		case KC_NEXTTRACK: return string("KC_NEXTTRACK");
		case KC_NUMPADENTER: return string("KC_NUMPADENTER");
		case KC_RCONTROL: return string("KC_RCONTROL");
		case KC_MUTE: return string("KC_MUTE");
		case KC_CALCULATOR: return string("KC_CALCULATOR");
		case KC_PLAYPAUSE: return string("KC_PLAYPAUSE");
		case KC_MEDIASTOP: return string("KC_MEDIASTOP");
		case KC_VOLUMEDOWN: return string("KC_VOLUMEDOWN");
		case KC_VOLUMEUP: return string("KC_VOLUMEUP");
		case KC_WEBHOME: return string("KC_WEBHOME");
		case KC_NUMPADCOMMA: return string("KC_NUMPADCOMMA");
		case KC_DIVIDE: return string(SDL_GetKeyName(SDLK_KP_DIVIDE));
		case KC_SYSRQ: return string(SDL_GetKeyName(SDLK_SYSREQ));
		case KC_RMENU: return string(SDL_GetKeyName(SDLK_RALT));
		case KC_PAUSE: return string("Pause");
		case KC_HOME: return string(SDL_GetKeyName(SDLK_HOME));
		case KC_UP: return string(SDL_GetKeyName(SDLK_UP));
		case KC_PGUP: return string(SDL_GetKeyName(SDLK_PAGEUP));
		case KC_LEFT: return string(SDL_GetKeyName(SDLK_LEFT));
		case KC_RIGHT: return string(SDL_GetKeyName(SDLK_RIGHT));
		case KC_END:  return string(SDL_GetKeyName(SDLK_END));
		case KC_DOWN: return string(SDL_GetKeyName(SDLK_DOWN));
		case KC_PGDOWN: return string(SDL_GetKeyName(SDLK_PAGEDOWN));
		case KC_INSERT: return string(SDL_GetKeyName(SDLK_INSERT));
		case KC_DELETE: return string(SDL_GetKeyName(SDLK_DELETE));
		case KC_LWIN: return string("LWIN");
		case KC_RWIN:  return string("RWIN");
		case KC_APPS: return string("KC_APPS");
		case KC_POWER: return string("KC_POWER");
		case KC_SLEEP: return string("KC_SLEEP");
		case KC_WAKE: return string("KC_WAKE");
		case KC_WEBSEARCH: return string("KC_WEBSEARCH");
		case KC_WEBFAVORITES: return string("KC_WEBFAVORITES");
		case KC_WEBREFRESH: return string("KC_WEBREFRESH");
		case KC_WEBSTOP: return string("KC_WEBSTOP");
		case KC_WEBFORWARD: return string("KC_WEBFORWARD");
		case KC_WEBBACK: return string("KC_WEBBACK");
		case KC_MYCOMPUTER: return string("KC_MYCOMPUTER");
		case KC_MAIL: return string("KC_MAIL");
		case KC_MEDIASELECT: return string("KC_MEDIASELECT");
	};

	return string("Unknown");
}

void Keyboard::Keyboard::Init()
{
	_keyMap.Set(GetAsString(KC_ESCAPE), KC_ESCAPE);
	_keyMap.Set(GetAsString(KC_1), KC_1);
	_keyMap.Set(GetAsString(KC_2), KC_2);
	_keyMap.Set(GetAsString(KC_3), KC_3);
	_keyMap.Set(GetAsString(KC_4), KC_4);
	_keyMap.Set(GetAsString(KC_5), KC_5);
	_keyMap.Set(GetAsString(KC_6), KC_6);
	_keyMap.Set(GetAsString(KC_7), KC_7);
	_keyMap.Set(GetAsString(KC_8), KC_8);
	_keyMap.Set(GetAsString(KC_9), KC_9);
	_keyMap.Set(GetAsString(KC_0), KC_0);
	_keyMap.Set(GetAsString(KC_MINUS), KC_MINUS);
	_keyMap.Set(GetAsString(KC_EQUALS), KC_EQUALS);
	_keyMap.Set(GetAsString(KC_BACK), KC_BACK);
	_keyMap.Set(GetAsString(KC_TAB), KC_TAB);
	_keyMap.Set(GetAsString(KC_Q), KC_Q);
	_keyMap.Set(GetAsString(KC_W), KC_W);
	_keyMap.Set(GetAsString(KC_E), KC_E);
	_keyMap.Set(GetAsString(KC_R), KC_R);
	_keyMap.Set(GetAsString(KC_T), KC_T);
	_keyMap.Set(GetAsString(KC_Y), KC_Y);
	_keyMap.Set(GetAsString(KC_U), KC_U);
	_keyMap.Set(GetAsString(KC_I), KC_I);
	_keyMap.Set(GetAsString(KC_O), KC_O);
	_keyMap.Set(GetAsString(KC_P), KC_P);
	_keyMap.Set(GetAsString(KC_RETURN), KC_RETURN);
	_keyMap.Set(GetAsString(KC_LCONTROL), KC_LCONTROL);
	_keyMap.Set(GetAsString(KC_A), KC_A);
	_keyMap.Set(GetAsString(KC_S), KC_S);
	_keyMap.Set(GetAsString(KC_D), KC_D);
	_keyMap.Set(GetAsString(KC_F), KC_F);
	_keyMap.Set(GetAsString(KC_G), KC_G);
	_keyMap.Set(GetAsString(KC_H), KC_H);
	_keyMap.Set(GetAsString(KC_J), KC_J);
	_keyMap.Set(GetAsString(KC_K), KC_K);
	_keyMap.Set(GetAsString(KC_L), KC_L);
	_keyMap.Set(GetAsString(KC_SEMICOLON), KC_SEMICOLON);
	_keyMap.Set(GetAsString(KC_COLON), KC_COLON);
	_keyMap.Set(GetAsString(KC_APOSTROPHE), KC_APOSTROPHE);
	_keyMap.Set(GetAsString(KC_GRAVE), KC_GRAVE);
	_keyMap.Set(GetAsString(KC_LSHIFT), KC_LSHIFT);
	_keyMap.Set(GetAsString(KC_BACKSLASH), KC_BACKSLASH);
	_keyMap.Set(GetAsString(KC_SLASH), KC_SLASH);
	_keyMap.Set(GetAsString(KC_Z), KC_Z);
	_keyMap.Set(GetAsString(KC_X), KC_X);
	_keyMap.Set(GetAsString(KC_C), KC_C);
	_keyMap.Set(GetAsString(KC_V), KC_V);
	_keyMap.Set(GetAsString(KC_B), KC_B);
	_keyMap.Set(GetAsString(KC_N), KC_N);
	_keyMap.Set(GetAsString(KC_M), KC_M);
	_keyMap.Set(GetAsString(KC_COMMA), KC_COMMA);
	_keyMap.Set(GetAsString(KC_PERIOD), KC_PERIOD);
	_keyMap.Set(GetAsString(KC_RSHIFT), KC_RSHIFT);
	_keyMap.Set(GetAsString(KC_MULTIPLY), KC_MULTIPLY);
	_keyMap.Set(GetAsString(KC_LMENU), KC_LMENU);
	_keyMap.Set(GetAsString(KC_SPACE), KC_SPACE);
	_keyMap.Set(GetAsString(KC_CAPITAL), KC_CAPITAL);
	_keyMap.Set(GetAsString(KC_F1), KC_F1);
	_keyMap.Set(GetAsString(KC_F2), KC_F2);
	_keyMap.Set(GetAsString(KC_F3), KC_F3);
	_keyMap.Set(GetAsString(KC_F4), KC_F4);
	_keyMap.Set(GetAsString(KC_F5), KC_F5);
	_keyMap.Set(GetAsString(KC_F6), KC_F6);
	_keyMap.Set(GetAsString(KC_F7), KC_F7);
	_keyMap.Set(GetAsString(KC_F8), KC_F8);
	_keyMap.Set(GetAsString(KC_F9), KC_F9);
	_keyMap.Set(GetAsString(KC_F10), KC_F10);
	_keyMap.Set(GetAsString(KC_NUMLOCK), KC_NUMLOCK);
	_keyMap.Set(GetAsString(KC_SCROLL), KC_SCROLL);
	_keyMap.Set(GetAsString(KC_NUMPAD7), KC_NUMPAD7);
	_keyMap.Set(GetAsString(KC_NUMPAD8), KC_NUMPAD8);
	_keyMap.Set(GetAsString(KC_NUMPAD9), KC_NUMPAD9);
	_keyMap.Set(GetAsString(KC_SUBTRACT), KC_SUBTRACT);
	_keyMap.Set(GetAsString(KC_NUMPAD4), KC_NUMPAD4);
	_keyMap.Set(GetAsString(KC_NUMPAD5), KC_NUMPAD5);
	_keyMap.Set(GetAsString(KC_NUMPAD6), KC_NUMPAD6);
	_keyMap.Set(GetAsString(KC_ADD), KC_ADD);
	_keyMap.Set(GetAsString(KC_NUMPAD1), KC_NUMPAD1);
	_keyMap.Set(GetAsString(KC_NUMPAD2), KC_NUMPAD2);
	_keyMap.Set(GetAsString(KC_NUMPAD3), KC_NUMPAD3);
	_keyMap.Set(GetAsString(KC_NUMPAD0), KC_NUMPAD0);
	_keyMap.Set(GetAsString(KC_DECIMAL), KC_DECIMAL);
	_keyMap.Set(GetAsString(KC_F11), KC_F11);
	_keyMap.Set(GetAsString(KC_F12), KC_F12);
	_keyMap.Set(GetAsString(KC_F13), KC_F13);
	_keyMap.Set(GetAsString(KC_F14), KC_F14);
	_keyMap.Set(GetAsString(KC_F15), KC_F15);
	_keyMap.Set(GetAsString(KC_NUMPADEQUALS), KC_NUMPADEQUALS);
	_keyMap.Set(GetAsString(KC_DIVIDE), KC_DIVIDE);
	_keyMap.Set(GetAsString(KC_SYSRQ), KC_SYSRQ);
	_keyMap.Set(GetAsString(KC_RMENU), KC_RMENU);
	_keyMap.Set(GetAsString(KC_HOME), KC_HOME);
	_keyMap.Set(GetAsString(KC_UP), KC_UP);
	_keyMap.Set(GetAsString(KC_PGUP), KC_PGUP);
	_keyMap.Set(GetAsString(KC_LEFT), KC_LEFT);
	_keyMap.Set(GetAsString(KC_RIGHT), KC_RIGHT);
	_keyMap.Set(GetAsString(KC_END), KC_END);
	_keyMap.Set(GetAsString(KC_DOWN), KC_DOWN);
	_keyMap.Set(GetAsString(KC_PGDOWN), KC_PGDOWN);
	_keyMap.Set(GetAsString(KC_INSERT), KC_INSERT);
	_keyMap.Set(GetAsString(KC_DELETE), KC_DELETE);
}
