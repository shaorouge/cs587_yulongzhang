
#include "../../../Headers/Engine/IO/InputManager.h"
using namespace Engine;

InputManager* InputManager::_instance = NULL;

InputManager::InputManager() : _keyboard(new Keyboard())
{
	_keyboard->SetEventCallBack(this);
	_keyboard->Init();
}

InputManager::~InputManager()
{
	delete _keyboard;
}

bool InputManager::keyPressed(const KeyEvent& keyEvent)
{
	for(Iterator<IInputListener*>* inputItr = _listeners.CreateIterator(); !inputItr->IsDone(); inputItr->Next())
	{
		inputItr->Current()->KeyPressed(keyEvent);
	}

	return true;
}

bool InputManager::keyReleased(const KeyEvent& keyEvent)
{
	for(Iterator<IInputListener*>* inputItr = _listeners.CreateIterator(); !inputItr->IsDone(); inputItr->Next())
	{
		inputItr->Current()->KeyReleased(keyEvent);
	}

	return true;
}

void InputManager::Tick(const float deltaTime)
{
	if(_keyboard)
	{
		_keyboard->Capture();
	}
}

void InputManager::AddListener(IInputListener* listener)
{
	_listeners.Add(listener);
}

InputManager* InputManager::Instance()
{
	if(_instance == NULL)
	{
		_instance = new InputManager();
	}

	return _instance;
}

void InputManager::Destory()
{
	delete this;
}
