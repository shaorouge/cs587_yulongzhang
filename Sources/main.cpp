/*
#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestFixture.h>
#include <cppunit/TestSuite.h>
#include <cppunit/TestAssert.h>
#include <cppunit/extensions/HelperMacros.h>
*/

#include "../Headers/Game/MGame.h"

int main(int argc, char* argv[])
{
	/*
	CppUnit::TextUi::TestRunner runner;
	CppUnit::TestFactoryRegistry& registry = CppUnit::TestFactoryRegistry::getRegistry();
	runner.addTest(registry.makeTest());
	runner.run();
	*/
	Game::MGame game;

	game.Init();
	game.Run();
	game.Quit();

	return 0;
}
